# Get started with a build env with Rust nightly
FROM rustlang/rust:nightly-bookworm as builder

# Install cargo-binstall, which makes it easier to install other
# cargo extensions like cargo-leptos
RUN wget -q https://github.com/cargo-bins/cargo-binstall/releases/latest/download/cargo-binstall-x86_64-unknown-linux-musl.tgz
RUN tar -xvf cargo-binstall-x86_64-unknown-linux-musl.tgz
RUN cp cargo-binstall /usr/local/cargo/bin

# Install cargo-leptos
RUN cargo binstall cargo-leptos --version 0.2.21 -y

# Add the WASM target
RUN rustup target add wasm32-unknown-unknown

# Install npm
RUN apt-get update -y &&\
    apt-get install -y --no-install-recommends npm

# Make an /app dir, which everything will eventually live in
RUN mkdir -p /app
WORKDIR /app
RUN git clone --recurse-submodules https://codeberg.org/grafcube/wordforge.git /app

# Build the app
ENV SQLX_OFFLINE=true
RUN cargo leptos build --release -vv

FROM debian:trixie-slim as runtime
WORKDIR /app
RUN apt-get update -y \
    && apt-get install -y --no-install-recommends openssl ca-certificates pandoc \
    && apt-get autoremove -y \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/*

# Copy the server binary to the /app directory
COPY --from=builder /app/target/release/wordforge-server /app/

# /target/site contains our JS/WASM/CSS, etc.
COPY --from=builder /app/target/site /app/site

# Set any required env variables and
ENV SITE_ADDR="0.0.0.0:8080,[::]:8080"
ENV LEPTOS_SITE_ROOT="site"
EXPOSE 8080

VOLUME [ "/store" ]
ENV FILE_STORE="/store"

# Set DOMAIN, DATABASE_URL and MEDIA_ADDR before running.

# Run the server
CMD ["/app/wordforge-server"]
