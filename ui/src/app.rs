use crate::{
    components::Panel,
    i18n::{use_i18n, I18nContextProvider},
    routes::{
        auth::{Auth, SetupAdmin},
        novel::{CreateBook, NovelPage},
        reader::ReaderView,
        settings::{Account, Admin, Developer, SettingsView},
        user::UserView,
        writer::WriterView,
    },
};
use codee::string::JsonSerdeCodec;
use html::Dialog;
use leptos::*;
use leptos_i18n::t;
use leptos_icons::*;
use leptos_meta::*;
use leptos_router::*;
use leptos_use::use_cookie;
use serde::{Deserialize, Serialize};
use wasm_bindgen::JsCast;

pub type ValidationResult = Result<UserInfo, ValidationError>;

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct UserInfo {
    pub id: String,
    pub name: Option<String>,
    pub username: String,
}

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct ClientPrefs {
    pub theme_id: Option<String>,
    pub theme_name: String,
}

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub enum ValidationError {
    Unauthorized(String),
    Error(String),
}

#[server]
async fn does_admin_exist() -> Result<bool, ServerFnError> {
    use activitypub_federation::config::Data;
    use leptos_axum::extract;
    use wordforge_api::{account::admin_exists, DataHandle};

    let data: Data<DataHandle> = extract().await?;
    admin_exists(&data.pool).await.map_err(ServerFnError::new)
}

#[component]
pub fn App() -> impl IntoView {
    let admin_exists_res = create_blocking_resource(|| (), move |()| does_admin_exist());

    let (is_routing, set_is_routing) = create_signal(false);
    let valid = create_blocking_resource(|| (), move |()| user_validate());
    let validate = Callback::new(move |()| {
        valid
            .get()
            .map(|resp| resp.unwrap_or_else(|e| Err(ValidationError::Error(e.to_string()))))
    });

    provide_context(valid);
    provide_context(validate);
    provide_meta_context();

    view! {
        <Stylesheet id="leptos" href="/pkg/wordforge.css" />
        <Link rel="icon" href="/favicon.svg" />
        <I18nContextProvider>
            <Router set_is_routing>
                <RoutingProgress
                    is_routing
                    class="absolute top-0 w-screen h-1 bg-transparent [&::-moz-progress-bar]:rounded-r [&::-webkit-progress-bar]:rounded-r [&::-webkit-progress-value]:rounded-r [&::-webkit-progress-bar]:bg-transparent [&::-webkit-progress-value]:bg-primary [&::-moz-progress-bar]:bg-primary"
                />

                <Overlay admin_exists_res>
                    <Routes>
                        <Route
                            path="/"
                            view=move || {
                                view! {
                                    <Suspense fallback=move || {
                                        view! {
                                            <span class="loading loading-spinner loading-lg m-auto"></span>
                                        }
                                    }>
                                        {move || {
                                            admin_exists_res()
                                                .map(|admin_exists| {
                                                    let admin_exists = admin_exists.unwrap();
                                                    view! {
                                                        <Show
                                                            when=move || admin_exists
                                                            fallback=move || {
                                                                view! { <SetupAdmin admin_exists_res /> }
                                                            }
                                                        >

                                                            <Home />
                                                        </Show>
                                                    }
                                                })
                                        }}

                                    </Suspense>
                                }
                            }
                        />

                        <Route
                            path="/auth"
                            view=move || {
                                view! {
                                    <Suspense fallback=move || {
                                        view! {
                                            <span class="loading loading-spinner loading-lg m-auto"></span>
                                        }
                                    }>
                                        <Show
                                            when=move || !matches!(validate(()), Some(Ok(_)))
                                            fallback=move || {
                                                view! {
                                                    <Redirect
                                                        path="/"
                                                        options=NavigateOptions {
                                                            replace: true,
                                                            ..Default::default()
                                                        }
                                                    />
                                                }
                                            }
                                        >

                                            <Outlet />
                                        </Show>
                                    </Suspense>
                                }
                            }
                        >

                            <Route path="" view=Auth />
                        </Route>

                        <Route
                            path="/create"
                            view=move || {
                                view! {
                                    <Suspense fallback=move || {
                                        view! {
                                            <span class="loading loading-spinner loading-lg m-auto"></span>
                                        }
                                    }>
                                        <Show
                                            when=move || matches!(validate(()), Some(Ok(_)))
                                            fallback=move || {
                                                view! {
                                                    <Redirect
                                                        path="/auth"
                                                        options=NavigateOptions {
                                                            replace: true,
                                                            ..Default::default()
                                                        }
                                                    />
                                                }
                                            }
                                        >

                                            <Outlet />
                                        </Show>
                                    </Suspense>
                                }
                            }
                        >
                            <Route path="" view=CreateBook />
                        </Route>

                        <Route path="/novel/:uuid" view=NovelPage />
                        <Route path="/novel/:uuid/:preview" view=NovelPage />

                        <Route path="/user/:name" view=UserView />

                        <Route path="/chapter/:uuid/:seq" view=ReaderView />
                        <Route path="/chapter/:uuid/:seq/:npreview" view=ReaderView />
                        <Route path="/chapter/:uuid/:seq/:npreview/:cpreview" view=ReaderView />
                        <Route
                            path="/edit/:uuid/:seq"
                            view=move || {
                                view! {
                                    <Suspense fallback=move || {
                                        view! {
                                            <span class="loading loading-spinner loading-lg m-auto"></span>
                                        }
                                    }>
                                        <Show
                                            when=move || matches!(validate(()), Some(Ok(_)))
                                            fallback=move || {
                                                view! {
                                                    <Redirect
                                                        path="/auth"
                                                        options=NavigateOptions {
                                                            replace: true,
                                                            ..Default::default()
                                                        }
                                                    />
                                                }
                                            }
                                        >

                                            <Outlet />
                                        </Show>
                                    </Suspense>
                                }
                            }
                        >
                            <Route path="" view=WriterView />
                        </Route>

                        <Route
                            path="/settings"
                            view=move || {
                                view! {
                                    <Suspense fallback=move || {
                                        view! {
                                            <span class="loading loading-spinner loading-lg m-auto"></span>
                                        }
                                    }>
                                        <Show
                                            when=move || matches!(validate(()), Some(Ok(_)))
                                            fallback=move || {
                                                view! {
                                                    <Redirect
                                                        path="/auth"
                                                        options=NavigateOptions {
                                                            replace: true,
                                                            ..Default::default()
                                                        }
                                                    />
                                                }
                                            }
                                        >

                                            <Outlet />
                                        </Show>
                                    </Suspense>
                                }
                            }
                        >

                            <Route
                                path=""
                                view=move || {
                                    view! {
                                        <Redirect
                                            path="account"
                                            options=NavigateOptions {
                                                replace: true,
                                                ..Default::default()
                                            }
                                        />
                                    }
                                }
                            />

                            <Route path="" view=SettingsView>
                                <Route path="account" view=Account />
                                <Route path="developer" view=Developer />
                                <Route path="admin" view=Admin />
                            </Route>
                        </Route>
                    </Routes>
                </Overlay>
            </Router>
        </I18nContextProvider>
    }
}

#[component]
fn Overlay(
    admin_exists_res: Resource<(), Result<bool, ServerFnError>>,
    children: Children,
) -> impl IntoView {
    let valid = use_context::<Resource<(), Result<ValidationResult, ServerFnError>>>().unwrap();
    let loc = use_location();
    let redirect_path = Memo::new(move |_| {
        format!(
            "{}{}{}",
            loc.pathname.get(),
            loc.search.get(),
            loc.hash.get()
        )
    });
    let logout = create_action(|()| logout());
    let logout_res = logout.value();

    let show_setup = move || admin_exists_res().map(|v| !v.unwrap()).unwrap_or_default();

    let (_, set_user_info) = use_cookie::<UserInfo, JsonSerdeCodec>("user_info");

    Effect::new(move |_| match logout_res() {
        None => (),
        Some(Err(e)) => logging::error!("{}", e.to_string()),
        Some(Ok(_)) => {
            set_user_info(None);
            valid.refetch();
            use_navigate()("/", NavigateOptions::default());
        }
    });

    view! {
        <Body class="w-full h-full overflow-x-clip overflow-y-auto md:px-4" />
        <div class:hidden=show_setup>
            <Topbar logout redirect_path />
        </div>
        <div class="mb-12 md:mb-0 mx-auto max-w-[100rem]">{children()}</div>
        <div class:hidden=show_setup>
            <BottomBar logout redirect_path />
        </div>
    }
}

#[component]
fn ThemePreview(id: Option<String>, name: String) -> impl IntoView {
    let i18n = use_i18n();
    let id = StoredValue::new(id);
    let name = StoredValue::new(name);
    let (_, set_client_prefs) = use_cookie::<ClientPrefs, JsonSerdeCodec>("client_prefs");

    view! {
        <button
            class="border-base-content/20 hover:border-base-content/40 max-h-24 overflow-hidden rounded-lg border outline outline-2 outline-offset-2 outline-base-content"
            data-theme=id
            on:click=move |_| {
                update!(
                    |set_client_prefs| {
                    if let Some(ref mut client_prefs) = set_client_prefs {
                        client_prefs.theme_id = id();
                        client_prefs.theme_name = name();
                    } else {
                        *set_client_prefs = Some(ClientPrefs { theme_id: id(), theme_name: name() });
                    }
                }
                );
            }
        >
            <div
                data-theme=id
                class="bg-base-100 text-base-content w-full cursor-pointer font-sans"
            >
                <div class="grid grid-cols-5 grid-rows-3">
                    <div class="bg-base-200 col-start-1 row-span-2 row-start-1"></div>
                    <div class="bg-base-300 col-start-1 row-start-3"></div>
                    <div class="bg-base-100 col-span-4 col-start-2 row-span-3 row-start-1 flex flex-col gap-1 p-2">
                        <div class="font-bold">{name}</div>
                        <div class="flex flex-wrap gap-1">
                            <div class="bg-primary flex aspect-square w-5 items-center justify-center rounded lg:w-6">
                                <div class="text-primary-content text-sm font-bold">
                                    {t!(i18n, common.theme_preview_character)}
                                </div>
                            </div>
                            <div class="bg-secondary flex aspect-square w-5 items-center justify-center rounded lg:w-6">
                                <div class="text-secondary-content text-sm font-bold">
                                    {t!(i18n, common.theme_preview_character)}
                                </div>
                            </div>
                            <div class="bg-accent flex aspect-square w-5 items-center justify-center rounded lg:w-6">
                                <div class="text-accent-content text-sm font-bold">
                                    {t!(i18n, common.theme_preview_character)}
                                </div>
                            </div>
                            <div class="bg-neutral flex aspect-square w-5 items-center justify-center rounded lg:w-6">
                                <div class="text-neutral-content text-sm font-bold">
                                    {t!(i18n, common.theme_preview_character)}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </button>
    }
}

#[component]
fn ThemeSwitcher(class: &'static str) -> impl IntoView {
    let (client_prefs, set_client_prefs) =
        use_cookie::<ClientPrefs, JsonSerdeCodec>("client_prefs");
    let themes = Resource::local(
        || (),
        move |()| async {
            let theme_list = window().fetch_with_str("/_themes");
            let theme_list = wasm_bindgen_futures::JsFuture::from(theme_list)
                .await
                .map_err(|e| logging::error!("Failed to fetch stylesheet: {:?}", e))
                .ok()?;
            let theme_list: web_sys::Response = theme_list.unchecked_into();

            if theme_list.ok() {
                let theme_list = wasm_bindgen_futures::JsFuture::from(theme_list.text().ok()?)
                    .await
                    .ok()?
                    .as_string()?
                    .lines()
                    .flat_map(|v| {
                        let (id, name) = v.split_once(':')?;
                        Some((id.into(), name.into()))
                    })
                    .collect::<Vec<(Box<str>, Box<str>)>>();
                return Some(theme_list);
            }
            None
        },
    );

    Effect::new(move |_| {
        if client_prefs().is_none() {
            set_client_prefs(Some(ClientPrefs {
                theme_id: None,
                theme_name: "Auto".to_string(),
            }));
        }
    });

    let node_ref: NodeRef<Dialog> = NodeRef::new();

    view! {
        <Html attr:data-theme=move || client_prefs().map(|v| v.theme_id) />
        <button
            on:click=move |_| {
                if let Some(node_ref) = node_ref() {
                    node_ref.show_modal().unwrap()
                }
            }
            class=class
        >
            <Icon icon=icondata::TbSunMoon class="w-6 h-6 my-auto" />
            <span class="my-auto">{move || client_prefs().map(|v| v.theme_name)}</span>
        </button>
        <dialog node_ref=node_ref class="modal modal-bottom sm:modal-middle">
            <div class="modal-box lg:max-w-3xl">
                <form method="dialog" class="flex flex-row w-full justify-between">
                    <h3 class="my-auto text-xl text-neutral-content">"Select theme"</h3>
                    <button class="my-auto p-1 rounded-full transition ease-in-out duration-[400ms] hover:bg-base-300">
                        <Icon icon=icondata::CgClose class="w-8 h-8 my-auto" />
                    </button>
                </form>
                <div class="rounded-box w-full grid grid-cols-2 gap-4 sm:grid-cols-3 md:grid-cols-4 lg:grid-cols-5">
                    <ThemePreview id=None name="Auto".to_string() />
                    {move || {
                        themes()
                            .flatten()
                            .map(|themes| {
                                themes
                                    .iter()
                                    .map(|(id, name)| {
                                        view! {
                                            <ThemePreview
                                                id=Some(id.to_string())
                                                name=name.to_string()
                                            />
                                        }
                                    })
                                    .collect_view()
                            })
                    }}
                </div>
            </div>
            <form method="dialog" class="modal-backdrop">
                <button class="cursor-default" />
            </form>
        </dialog>
    }
}

#[component]
fn Topbar(
    logout: Action<(), Result<(), ServerFnError>>,
    redirect_path: Memo<String>,
) -> impl IntoView {
    let i18n = use_i18n();
    let validate = expect_context::<Callback<(), Option<ValidationResult>>>();
    let panel = RwSignal::new(false);
    let (user_info, set_user_info) = use_cookie::<UserInfo, JsonSerdeCodec>("user_info");

    view! {
        <div class="md:flex flex-none flex-row w-full gap-1 mt-3 mb-4 mx-auto rounded-md justify-start whitespace-nowrap text-md hidden max-w-[100rem] join bg-primary">
            <A href="/" class="flex flex-row gap-2 btn btn-primary join-item">
                <Icon icon=icondata::CgHome class="w-6 h-6 my-auto" />
                <span class="my-auto">{t!(i18n, common.nav_home)}</span>
            </A>
            <A href="/explore/local" class="flex flex-row gap-2 btn btn-primary join-item">
                <Icon icon=icondata::LuComponent class="w-6 h-6 my-auto" />
                <span class="my-auto">{t!(i18n, common.nav_local)}</span>
            </A>
            <A href="/explore" class="flex flex-row gap-2 btn btn-primary join-item">
                <Icon icon=icondata::LuGlobe2 class="w-6 h-6 my-auto" />
                <span class="my-auto">{t!(i18n, common.nav_global)}</span>
            </A>
            <span class="w-full mx-auto"></span>
            <Panel
                class="dropdown dropdown-bottom dropdown-end min-w-[12rem] btn btn-primary join-item"
                summary_class="flex flex-row gap-2 h-full"
                state=panel
                summary=move || {
                    view! {
                        <span
                            class="flex-none w-6 h-6 my-auto rounded-full bg-pink-500"
                            class:hidden=move || user_info().is_none()
                        ></span>
                        <span class="my-auto max-w-[10rem] text-left whitespace-nowrap overflow-hidden overflow-ellipsis">
                            {move || {
                                user_info()
                                    .map(|user| {
                                        user.name.clone().unwrap_or_else(|| user.username.clone())
                                    })
                                    .unwrap_or_else(|| "Welcome!".to_string())
                            }}
                        </span>

                        {
                            view! {
                                <Icon
                                    icon=if panel() {
                                        icondata::CgChevronUp
                                    } else {
                                        icondata::CgChevronDown
                                    }

                                    class="my-auto ml-auto w-8 h-8"
                                />
                            }
                        }
                    }
                }
            >
                <ul class="mt-1 z-50 rounded-md min-w-[12rem] shadow dropdown-content join join-vertical bg-primary">
                    <li>
                        <ThemeSwitcher class="flex flex-row gap-2 w-full items-start justify-start btn btn-primary join-item" />
                    </li>
                    <Suspense>
                        {move || {
                            validate(())
                                .map(|v| match v {
                                    Ok(user) => {
                                        set_user_info(Some(user.clone()));
                                        view! {
                                            <li>
                                                <A
                                                    href="/create"
                                                    class="flex flex-row gap-2 w-full items-start justify-start btn btn-primary join-item"
                                                >
                                                    <Icon
                                                        icon=icondata::BiPencilRegular
                                                        class="w-6 h-6 my-auto"
                                                    />
                                                    <span class="my-auto">
                                                        {t!(i18n, common.create_new_book)}
                                                    </span>
                                                </A>
                                            </li>
                                            <li>
                                                <A
                                                    class="flex flex-row gap-2 w-full items-start justify-start btn btn-primary join-item"
                                                    href=format!("/user/{}", user.username)
                                                >
                                                    <Icon icon=icondata::BsPerson class="w-6 h-6 my-auto" />
                                                    <span class="my-auto">{t!(i18n, common.profile)}</span>
                                                </A>
                                            </li>
                                            <li>
                                                <A
                                                    class="flex flex-row gap-2 w-full items-start justify-start btn btn-primary join-item"
                                                    href="/settings"
                                                >
                                                    <Icon icon=icondata::TbSettings2 class="w-6 h-6 my-auto" />
                                                    <span class="my-auto">{t!(i18n, common.settings)}</span>
                                                </A>
                                            </li>
                                            <li>
                                                <button
                                                    class="flex flex-row gap-2 w-full items-start justify-start btn btn-primary join-item"
                                                    on:click=move |_| {
                                                        logout.dispatch(());
                                                    }
                                                >
                                                    <Icon icon=icondata::CgLogOut class="w-6 h-6 my-auto" />
                                                    <span class="my-auto">{t!(i18n, common.logout)}</span>
                                                </button>
                                            </li>
                                        }
                                            .into_view()
                                    }
                                    Err(ValidationError::Unauthorized(_)) => {
                                        set_user_info(None);
                                        view! {
                                            <li>
                                                <A
                                                    href=format!("/auth?redirect_to={}", redirect_path())
                                                    class="flex flex-row gap-2 w-full items-start justify-start btn btn-primary join-item"
                                                >
                                                    <Icon icon=icondata::BsPersonPlus class="w-6 h-6 my-auto" />
                                                    <span class="my-auto">{t!(i18n, common.sign_in_up)}</span>
                                                </A>
                                            </li>
                                        }
                                            .into_view()
                                    }
                                    Err(ValidationError::Error(e)) => {
                                        logging::error!(
                                            "ValidationError::Error@app::Sidebar: {}", e
                                        );
                                        view! {
                                            <span class="h-full pl-1 pr-2 py-1 my-auto text-center">
                                                "Something went wrong"
                                            </span>
                                        }
                                            .into_view()
                                    }
                                })
                        }}
                    </Suspense>
                </ul>
            </Panel>
        </div>
    }
}

#[component]
fn BottomBar(
    logout: Action<(), Result<(), ServerFnError>>,
    redirect_path: Memo<String>,
) -> impl IntoView {
    let i18n = use_i18n();
    let validate = use_context::<Callback<(), Option<ValidationResult>>>().unwrap();
    let panel = RwSignal::new(false);
    let (user_info, set_user_info) = use_cookie::<UserInfo, JsonSerdeCodec>("user_info");

    let loc = use_location();

    Effect::new(move |_| {
        loc.pathname.track();
        panel.set(false);
    });

    view! {
        <div class="fixed bottom-0">
            <Show when=panel>
                <div class="p-2 rounded-t-xl w-full bg-base-200 light:bg-primary">
                    <ThemeSwitcher class="relative flex flex-row text-left w-full p-3 gap-3 hover:btn-primary" />
                    <div class:hidden=move || user_info().is_none()>
                        <A
                            class="relative flex flex-row text-left w-full p-3 gap-3 hover:btn-primary"
                            href=move || {
                                user_info()
                                    .map(|v| format!("/user/{}", v.username))
                                    .unwrap_or_default()
                            }
                        >
                            <Icon icon=icondata::BsPerson class="w-6 h-6 my-auto" />
                            <span class="my-auto">{t!(i18n, common.profile)}</span>
                        </A>
                    </div>
                    <div class:hidden=move || user_info().is_none()>
                        <A
                            class="relative flex flex-row text-left w-full p-3 gap-3 hover:btn-primary"
                            href="/settings"
                        >
                            <Icon icon=icondata::TbSettings2 class="w-6 h-6 my-auto" />
                            <span class="my-auto">{t!(i18n, common.settings)}</span>
                        </A>
                    </div>
                    <button
                        class="relative flex flex-row text-left w-full p-3 gap-3 hover:btn-primary"
                        class:hidden=move || user_info().is_none()
                        on:click=move |_| {
                            logout.dispatch(());
                            panel.set(false);
                        }
                    >

                        <Icon icon=icondata::CgLogOut class="w-6 h-6 my-auto" />
                        <span class="my-auto">{t!(i18n, common.logout)}</span>
                    </button>
                    <div class:hidden=move || user_info().is_some()>
                        <A
                            href=format!("/auth?redirect_to={}", redirect_path())
                            class="relative flex flex-row text-left w-full p-3 gap-3 hover:btn-primary"
                        >
                            <Icon icon=icondata::BsPersonPlus class="w-6 h-6 my-auto" />
                            <span class="my-auto">{t!(i18n, common.sign_in_up)}</span>
                        </A>
                    </div>
                </div>
            </Show>
            <div class="flex flex-row max-h-12 justify-stretch z-40 overflow-hidden w-screen md:hidden bg-base-200 light:bg-primary">
                <A href="/" class="w-full p-1 hover:btn-primary">
                    <Icon icon=icondata::CgHome class="w-10 h-10 m-auto" />
                </A>
                <A href="/explore" class="w-full p-1 hover:btn-primary">
                    <Icon icon=icondata::LuGlobe2 class="w-10 h-10 m-auto" />
                </A>
                <button class="w-full p-1 hover:btn-primary">
                    <Icon icon=icondata::CgSearch class="w-10 h-10 m-auto" />
                </button>
                <Suspense>
                    {move || {
                        validate(())
                            .map(|v| match v {
                                Ok(user) => {
                                    set_user_info(Some(user.clone()));
                                    view! {
                                        <A href="/create" class="w-full p-1 hover:btn-primary">
                                            <Icon
                                                icon=icondata::BiPencilRegular
                                                class="w-10 h-10 m-auto"
                                            />
                                        </A>
                                        <button
                                            on:click=move |_| panel.set(!panel())
                                            class="w-full p-1 hover:btn-primary"
                                        >
                                            <span class="block w-8 h-8 m-auto rounded-full bg-pink-500"></span>
                                        </button>
                                    }
                                        .into_view()
                                }
                                Err(ValidationError::Unauthorized(_)) => {
                                    set_user_info(None);
                                    view! {
                                        <button
                                            on:click=move |_| panel.set(!panel())
                                            class="w-full p-1 hover:btn-primary"
                                        >
                                            <Icon
                                                icon=icondata::BsPersonPlus
                                                class="w-10 h-10 m-auto"
                                            />
                                        </button>
                                    }
                                        .into_view()
                                }
                                Err(ValidationError::Error(e)) => {
                                    logging::error!("ValidationError::Error@app::BottomBar: {}", e);
                                    view! {
                                        <Icon
                                            icon=icondata::LuCircleSlash
                                            class="w-10 h-10 m-auto"
                                        />
                                    }
                                        .into_view()
                                }
                            })
                    }}

                </Suspense>
            </div>
        </div>
    }
}

#[component]
fn Home() -> impl IntoView {
    let users = Resource::once(homepage_user_list);
    let user_list = move || {
        users().map(|v| {
            v.map(|v| {
                v.into_iter()
            .map(|user| view! {
                <A
                    href=user.href
                    class="cursor-pointer flex flex-col justify-start items-start p-2 rounded-xl border-solid border-accent border-2 h-40 w-full md:w-80"
                >
                    <div class="font-bold text-lg line-clamp-1 whitespace-nowrap text-ellipsis">
                        {user.name}
                    </div>
                    <span class="mt-2 text-neutral-content">{user.novel_count} " novels"</span>
                    <p class="text-neutral-content line-clamp-2 whitespace-nowrap text-ellipsis">
                        {user.summary}
                    </p>
                    <span class="mt-2 italic text-neutral-content">"Joined " {user.published}</span>
                </A>
            }).collect_view()
            })
        })
    };

    view! {
        <Title text="Wordforge: Federated creative writing" />
        <div role="alert" class="md:mx-auto w-fit alert alert-warning m-2 p-2 mb-4 gap-2">
            <Icon icon=icondata::TiWarning class="w-8 h-8 mt-1 mb-auto" />
            <div class="prose-sm md:prose prose-h1:mb-3 prose-h1:text-warning-content prose-p:leading-snug prose-p:my-2 prose-p:text-warning-content">
                <h1>"Warning!"</h1>
                <p>
                    "This is highly unstable software. What you see here will change a lot"
                    " in future versions."
                </p>
                <p>
                    "Since feeds and recommendations have not yet been implemented, "
                    "this page will show you a list of users on this instance. "
                </p>
                <A
                    href="https://codeberg.org/grafcube/wordforge"
                    class="flex flex-row gap-1 justify-start bg-base-100 text-base-content btn btn-outline mx-auto max-w-sm"
                >
                    <Icon icon=icondata::AiHeartFilled class="w-10 h-10" />
                    <span>
                        "Want to help improve Wordforge? The project is open source! Click here to get involved."
                    </span>
                </A>
            </div>
        </div>
        <Suspense fallback=move || {
            view! { <span class="loading loading-spinner loading-lg m-auto"></span> }
        }>
            <ErrorBoundary fallback=move |e| {
                view! {
                    <div class="w-full px-4 py-2 my-1 rounded-md bg-error text-error-content">
                        {e()
                            .into_iter()
                            .map(|(_, e)| view! { <p>{e.to_string()}</p> })
                            .collect_view()}
                    </div>
                }
            }>
                <div class="flex flex-row justify-around flex-wrap gap-4 w-full px-2 md:mx-auto">
                    {user_list}
                </div>
            </ErrorBoundary>
        </Suspense>
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct UserItem {
    pub href: String,
    pub name: String,
    pub novel_count: u32,
    pub published: String,
    pub summary: String,
}

#[server]
async fn homepage_user_list() -> Result<Vec<UserItem>, ServerFnError> {
    use activitypub_federation::config::Data;
    use chrono_humanize::HumanTime;
    use itertools::Itertools;
    use leptos_axum::extract;
    use rand::prelude::*;
    use sqlx::query;
    use url::Url;
    use wordforge_api::DataHandle;

    let data: Data<DataHandle> = extract().await?;

    let mut users = query!(
        r#"
        select
            u.apub_id,
            u.preferred_username,
            u.name,
            u.published,
            u.summary,
            count(a.id) as novel_count
        from
            users u
        left join
            author_roles a
        on
            u.apub_id = a.author
        group by
            u.apub_id,
            u.preferred_username,
            u.name,
            u.published,
            u.summary
        having
            count(a.id) > 0
    "#
    )
    .fetch_all(data.pool.as_ref())
    .await?
    .into_iter()
    .map(|r| {
        let apub_id = r.apub_id.parse::<Url>().unwrap();
        let domain = format!(
            "{}{}",
            apub_id.host_str().unwrap(),
            apub_id
                .port()
                .map(|p| format!(":{}", p))
                .unwrap_or(String::new())
        );
        UserItem {
            name: r.name.unwrap_or_else(|| r.preferred_username.clone()),
            novel_count: r.novel_count.map(|c| c as u32).unwrap_or_default(),
            summary: r.summary,
            href: format!(
                "/user/{}{}",
                r.preferred_username,
                domain
                    .eq(data.domain())
                    .then_some(String::new())
                    .unwrap_or(format!("@{}", domain))
            ),
            published: r
                .published
                .map(|p| HumanTime::from(p).to_string())
                .unwrap_or_default(),
        }
    })
    .collect_vec();
    users.shuffle(&mut thread_rng());

    Ok(users)
}

#[server]
async fn user_validate() -> Result<ValidationResult, ServerFnError> {
    use activitypub_federation::config::Data;
    use leptos_axum::extract;
    use tower_sessions::{Expiry, Session};
    use wordforge_api::{
        account::{self, UserValidateError},
        util::SESSION_EXPIRY,
        DataHandle,
    };

    let data: Data<DataHandle> = extract().await?;
    let session: Session = extract().await?;

    let apub_id = match session.get::<String>("id").await {
        Err(e) => return Ok(Err(ValidationError::Error(e.to_string()))),
        Ok(Some(u)) => u,
        Ok(None) => {
            return Ok(Err(ValidationError::Unauthorized(
                "Not signed in".to_string(),
            )))
        }
    };

    match account::validate(&data.pool, &apub_id).await {
        Ok(u) => {
            if let Some(Expiry::OnInactivity(_)) = session.expiry() {
                session.set_expiry(Some(Expiry::OnInactivity(SESSION_EXPIRY)))
            };
            Ok(Ok(UserInfo {
                id: u.id,
                name: u.name,
                username: u.username,
            }))
        }
        Err(UserValidateError::NotFound) => Ok(Err(ValidationError::Unauthorized(
            "Local user not found".to_string(),
        ))),
        Err(UserValidateError::InternalServerError(err)) => Ok(Err(ValidationError::Error(err))),
    }
}

#[server]
async fn logout() -> Result<(), ServerFnError> {
    leptos_axum::extract::<tower_sessions::Session>()
        .await?
        .flush()
        .await?;
    Ok(())
}
