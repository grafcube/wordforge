use leptos::*;
use leptos_router::*;

#[derive(Params, Debug, PartialEq, Clone)]
pub struct UserViewParams {
    pub name: String,
}

#[derive(Params, Debug, PartialEq, Clone)]
pub struct AuthQueries {
    pub redirect_to: String,
}
