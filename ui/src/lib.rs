pub mod app;
pub mod components;
pub mod fallback;
pub(crate) mod path;
pub mod routes;

leptos_i18n::load_locales!();
