use ev::Event;
use leptos::{ev::KeyboardEvent, html::*, *};
use leptos_icons::*;
use leptos_router::*;
use serde::de::DeserializeOwned;
use server_fn::{client::Client, codec::PostUrl, request::ClientReq, ServerFn};
use wasm_bindgen::JsCast;
use web_sys::FormData;

pub fn textarea_resize(ev: Event) {
    let target = event_target::<web_sys::HtmlElement>(&ev);
    let style = target.style();
    style.set_property("height", "auto").unwrap();
    style
        .set_property("height", &format!("{}px", target.scroll_height() + 6))
        .unwrap();
}

#[component]
pub fn Panel<F, IV>(
    #[prop(optional)] class: &'static str,
    #[prop(optional)] summary_class: &'static str,
    #[prop(optional)] state: RwSignal<bool>,
    summary: F,
    children: ChildrenFn,
) -> impl IntoView
where
    F: Fn() -> IV,
    IV: IntoView,
{
    let details = NodeRef::<html::Details>::new();

    Effect::new(move |_| {
        if let Some(target) = details() {
            target.set_open(state());
        }
    });

    let loc = use_location();

    Effect::new(move |_| {
        loc.pathname.track();
        if let Some(target) = details() {
            target.set_open(false);
        }
    });

    view! {
        <details
            node_ref=details
            class=class
            on:toggle=move |ev| {
                let target = event_target::<web_sys::HtmlDetailsElement>(&ev);
                state.set(target.open());
            }
        >

            <summary
                class=summary_class
                on:focusout=move |ev| {
                    let target = details().unwrap();
                    let receiver = ev.related_target().map(|r| r.unchecked_into::<web_sys::Node>());
                    if !target.contains(receiver.as_ref()) {
                        target.set_open(false);
                    }
                }

                on:keydown=move |ev: KeyboardEvent| {
                    if ev.key().as_str() == "Escape" {
                        ev.prevent_default();
                        let target = details().unwrap();
                        target.set_open(false);
                    }
                }
            >

                {summary()}
            </summary>
            {children()}
        </details>
    }
}

#[component]
pub fn SelectListbox(
    name: &'static str,
    label: &'static str,
    initial: &'static str,
    #[prop(optional)] selection: Option<String>,
    items: Vec<String>,
) -> impl IntoView {
    view! {
        <div class="flex flex-col gap-1">
            <label for=name class="label-text pl-4">
                {label}
            </label>
            <select name=name id=name class="btn text-left bg-base-200 select">
                <option disabled selected=selection.is_none()>
                    {initial}
                </option>
                {items
                    .into_iter()
                    .map(|v| {
                        view! {
                            <option selected=matches!(&selection, Some(s) if s == &v)>{v}</option>
                        }
                    })
                    .collect_view()}
            </select>
        </div>
    }
}

#[component]
pub fn DialogForm<F, IV, ServFn>(
    title: &'static str,
    #[prop(optional)] class: &'static str,
    node_ref: NodeRef<Dialog>,
    action: Action<ServFn, Result<ServFn::Output, ServerFnError<ServFn::Error>>>,
    pre_form: F,
    children: ChildrenFn,
) -> impl IntoView
where
    F: Fn() -> IV,
    IV: IntoView,
    ServFn: DeserializeOwned + ServerFn<InputEncoding = PostUrl> + 'static,
    <<ServFn::Client as Client<ServFn::Error>>::Request as ClientReq<ServFn::Error>>::FormData:
        From<FormData>,
{
    let form_ref = create_node_ref::<Form>();

    let reset_form = move |_: Event| {
        form_ref().unwrap().reset();
    };

    view! {
        <dialog
            class="modal modal-bottom sm:modal-middle"
            node_ref=node_ref
            on:close=reset_form
            on:cancel=reset_form
        >
            <div class="modal-box">
                <form method="dialog" class="flex flex-row w-full justify-between">
                    <h3 class="my-auto text-xl text-neutral-content">{title}</h3>
                    <button class="my-auto p-1 rounded-full transition ease-in-out duration-[400ms] hover:bg-base-300">
                        <Icon icon=icondata::CgClose class="w-8 h-8 my-auto" />
                    </button>
                </form>
                {pre_form()}
                <ActionForm class node_ref=form_ref action>
                    {children()}
                </ActionForm>
            </div>
            <form method="dialog" class="modal-backdrop">
                <button class="cursor-default"></button>
            </form>
        </dialog>
    }
}
