use leptos::*;
use leptos_meta::*;

#[component]
pub fn NotFoundPage() -> impl IntoView {
    view! {
        <Title text="Page not found" />
        <span class="mx-auto text-center dark:text-white">"404: Not found"</span>
    }
}

#[component]
pub fn InternalErrorPage() -> impl IntoView {
    view! {
        <Title text="Something went wrong" />
        <span class="mx-auto text-center dark:text-white">"500: Internal Server Error"</span>
    }
}

#[component]
pub fn ForbiddenPage() -> impl IntoView {
    view! {
        <Title text="Access denied" />
        <span class="mx-auto text-center dark:text-white">"403: Forbidden"</span>
    }
}
