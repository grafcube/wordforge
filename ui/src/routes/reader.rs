use crate::{
    components::{textarea_resize, DialogForm},
    fallback::NotFoundPage,
};
use leptos::{ev::KeyboardEvent, html::*, logging::error, *};
use leptos_icons::*;
use leptos_meta::*;
use leptos_router::*;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Chapter {
    pub apub_id: String,
    pub npreview: String,
    pub cpreview: String,
    pub novel_name: String,
    pub novel_id: String,
    pub editable: bool,
    pub title: String,
    pub summary: String,
    pub sensitive: bool,
    pub sequence: i32,
    pub published: String,
    pub updated: Option<String>,
    pub content: String,
    pub toc: String,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct ChapterUpdate {
    title: String,
    summary: String,
    sensitive: bool,
    updated: Option<String>,
}

#[component]
pub fn ReaderView() -> impl IntoView {
    let params = use_params_map();

    let (chapter_id, set_chapter_id) = create_query_signal::<String>("id");
    let chapter_id = chapter_id();
    set_chapter_id(None);

    let Some(novel_id) = params.with(|p| p.get("uuid").cloned()) else {
        return view! { <NotFoundPage /> }.into_view();
    };
    let novel_id = StoredValue::new(novel_id);

    let Some(seq) = params.with(|p| p.get("seq").cloned()) else {
        return view! { <NotFoundPage /> }.into_view();
    };

    let Ok(seq) = seq.parse() else {
        return view! { <NotFoundPage /> }.into_view();
    };

    let chapter = Resource::new(
        move || (chapter_id.clone(), novel_id, seq),
        move |(chapter_id, id, seq)| get_chapter(chapter_id, id(), seq),
    );

    let npreview = params.with(|p| p.get("npreview").cloned());
    let npreview = StoredValue::new(npreview);
    let cpreview = params.with(|p| p.get("cpreview").cloned());
    let cpreview = StoredValue::new(cpreview);

    view! {
        <Title text="Chapter" />
        <div class="mx-auto w-full px-4">
            <Suspense fallback=move || {
                view! { <span class="loading loading-spinner loading-lg m-auto"></span> }
            }>
                <ErrorBoundary fallback=move |e| {
                    let err = e()
                        .into_iter()
                        .map(|(_, e)| e.to_string())
                        .collect::<Vec<_>>()
                        .join("\n");
                    error!("reader: {}", err);
                    view! { <span class="px-4 py-2 my-2">"Something went wrong"</span> }
                }>
                    {move || {
                        chapter
                            .get()
                            .map(|c| {
                                c.map(|chapter| match chapter {
                                    None => view! { <NotFoundPage /> },
                                    Some(chapter) => {
                                        if let Some(npreview) = npreview() {
                                            if chapter.npreview != npreview {
                                                return view! { <NotFoundPage /> };
                                            }
                                        }
                                        if let Some(cpreview) = cpreview() {
                                            if chapter.cpreview != cpreview {
                                                return view! { <NotFoundPage /> };
                                            }
                                        }
                                        if npreview().is_none() || cpreview().is_none() {
                                            use_navigate()(
                                                &format!(
                                                    "/chapter/{}/{}/{}/{}",
                                                    novel_id(),
                                                    seq,
                                                    chapter.npreview,
                                                    chapter.cpreview,
                                                ),
                                                NavigateOptions {
                                                    replace: true,
                                                    ..Default::default()
                                                },
                                            );
                                        }
                                        view! { <ChapterView chapter /> }
                                    }
                                })
                            })
                    }}

                </ErrorBoundary>
            </Suspense>
        </div>
    }
    .into_view()
}

#[component]
fn ChapterView(chapter: Chapter) -> impl IntoView {
    let chapter = RwSignal::new(chapter);
    let edit_node = create_node_ref::<Dialog>();

    view! {
        <Title text=move || chapter().title />
        <div class="drawer drawer-end">
            <input id="toc-drawer" type="checkbox" class="drawer-toggle" />
            <div
                class="drawer-content tooltip tooltip-left fixed bottom-20 md:bottom-auto md:top-20 right-0"
                data-tip="Table of Contents"
            >
                <label for="toc-drawer" class="drawer-button btn btn-primary rounded-r-none">
                    <Icon icon=icondata::CgMenuRightAlt class="w-8 h-8 m-auto" />
                </label>
            </div>
            <div class="drawer-side">
                <label for="toc-drawer" aria-label="close sidebar" class="drawer-overlay"></label>
                <div
                    class="pb-8 [&>ul]:menu [&>ul]:p-4 [&>ul]:min-h-full [&>ul]:bg-base-200 [&>ul]:text-base-content"
                    inner_html=move || chapter().toc
                ></div>
            </div>
        </div>
        <div class="max-w-[65ch] mx-auto">
            <A
                href=move || format!("/novel/{}", chapter().novel_id)
                class="flex flex-row w-fit max-w-4xl gap-1 my-2 font-bold text-md link link-accent link-hover"
            >
                <Icon icon=icondata::CgArrowLeft class="w-6 h-6 my-auto" />
                <span class="my-auto w-full overflow-hidden whitespace-nowrap text-ellipsis">
                    {move || chapter().novel_name}
                </span>
            </A>
            <article class="prose prose-table:table prose-table:table-zebra prose-thead:text-base prose-code:bg-neutral prose-code:rounded-md w-full text-justify">
                <div class="flex flex-row gap-1">
                    <span class="mb-auto mr-2 mt-1 text-2xl italic opacity-80">
                        {move || chapter().sequence}
                    </span>
                    <span
                        class:hidden=move || !chapter().sensitive
                        class="mb-auto mt-3 badge badge-error"
                    >
                        "CW"
                    </span>
                    <button
                        class="p-1 mb-auto"
                        class:hidden=move || !chapter().editable
                        on:click=move |_| {
                            if let Some(edit) = edit_node() {
                                edit.show_modal().unwrap();
                            }
                        }
                    >

                        <Icon icon=icondata::BiPencilRegular class="w-8 h-8 my-auto" />
                    </button>
                    <ChapterEdit chapter node_ref=edit_node />
                    <h1 class="px-1">{move || chapter().title}</h1>
                    <div class="ml-auto mb-auto" class:hidden=move || !chapter().editable>
                        <A
                            class="btn btn-outline btn-primary"
                            href=move || {
                                format!("/edit/{}/{}", chapter().novel_id, chapter().sequence)
                            }
                        >

                            <Icon icon=icondata::ImQuill class="w-8 h-8 my-auto" />
                            <span>"Edit content"</span>
                        </A>
                    </div>
                </div>
                <div class="bg-secondary text-secondary-content mx-auto rounded-xl text-base overflow-auto w-10/12 my-2 px-4 py-2">
                    <div>{move || chapter().summary}</div>
                    <span class="italic text-sm">
                        "Published " {move || chapter().published}
                        {move || {
                            chapter().updated.map(|updated| format!(" (updated on {updated})"))
                        }}

                    </span>
                </div>
                <div
                    class="w-full pb-8 text-neutral-content"
                    inner_html=move || chapter().content
                ></div>
            </article>
        </div>
    }
}

#[component]
pub fn ChapterEdit(chapter: RwSignal<Chapter>, node_ref: NodeRef<Dialog>) -> impl IntoView {
    let edit = create_server_action::<EditChapter>();
    let response = edit.value();
    let err = move || {
        response().and_then(|response| match response {
            Ok(res) => {
                if let Some(dialog) = node_ref() {
                    dialog.close();
                }
                update!(|chapter| {
                    chapter.title = res.title;
                    chapter.summary = res.summary;
                    chapter.sensitive = res.sensitive;
                    chapter.updated = res.updated;
                });
                None
            }
            Err(e) => Some(e.to_string()),
        })
    };

    let delete_action = Action::new(move |apub_id: &String| delete_chapter(apub_id.clone()));
    let confirm_delete = RwSignal::new(false);
    let deleted = RwSignal::new(false);

    let summary = create_node_ref::<Textarea>();

    view! {
        <DialogForm
            class="flex flex-col justify-center text-center place-content-center items-center space-y-4 p-4 w-full"
            title="Chapter Metadata"
            node_ref
            action=edit
            pre_form=move || {
                view! {
                    <div
                        class="card mx-1 my-4 bg-error text-error-content"
                        class:hidden=move || err().is_none()
                    >
                        <div class="card-body">{err}</div>
                    </div>
                }
            }
        >

            <div class="relative w-full">
                <textarea
                    name="title"
                    id="title"
                    placeholder=" "
                    rows=1
                    wrap="soft"
                    class="floating-input input-bordered focus:input-primary rounded-xl text-xl w-full peer"
                    on:keydown=move |ev: KeyboardEvent| {
                        if ev.key() == "Enter" {
                            ev.prevent_default();
                            summary().unwrap().focus().unwrap();
                        }
                    }
                >

                    {move || chapter().title}
                </textarea>
                <label for="title" class="floating-label">
                    "Title"
                </label>
            </div>
            <div class="relative w-full">
                <textarea
                    name="summary"
                    id="summary"
                    placeholder=" "
                    on:input=textarea_resize
                    on:paste=textarea_resize
                    class="floating-input input-bordered focus:input-primary rounded-xl text-xl w-full peer"
                >
                    {move || chapter().summary}
                </textarea>
                <label for="summary" class="floating-label">
                    "Summary"
                </label>
            </div>
            <label class="flex flex-row gap-2 justify-start cursor-pointer label w-full">
                <input
                    type="checkbox"
                    name="sensitive"
                    value="true"
                    checked=move || chapter().sensitive
                    class="checkbox checkbox-error"
                />
                <span class="label-text text-xl">"Content warning"</span>
            </label>
            <input type="hidden" name="chapter_id" value=move || chapter().apub_id />
            <div class="relative">
                <button class="btn btn-primary" type="submit" disabled=deleted>
                    "Save"
                </button>
            </div>
            <Show
                when=confirm_delete
                fallback=move || {
                    view! {
                        <button
                            class="relative btn btn-outline btn-error"
                            class:hidden=deleted
                            on:click=move |_| confirm_delete.set(true)
                        >
                            <Icon icon=icondata::CgTrash class="w-6 h-6 m-auto" />
                            <span>"Delete"</span>
                        </button>
                        <span
                            class="loading loading-spinner loading-md m-auto"
                            class:hidden=move || !deleted()
                        ></span>
                        <Suspense>
                            <ErrorBoundary fallback=move |e| {
                                view! {
                                    <div class="w-full px-4 py-2 my-1 rounded-md bg-error text-error-content">
                                        {e()
                                            .into_iter()
                                            .map(|(_, e)| view! { <p>{e.to_string()}</p> })
                                            .collect_view()}
                                    </div>
                                }
                            }>
                                {move || {
                                    if let Some(dialog) = node_ref() {
                                        dialog.close();
                                    }
                                    delete_action
                                        .value()()
                                        .map(|v| {
                                            v.map(|()| use_navigate()(
                                                &format!("/novel/{}", chapter().novel_id),
                                                NavigateOptions::default(),
                                            ))
                                        })
                                }}

                            </ErrorBoundary>
                        </Suspense>
                    }
                }
            >

                <div class="flex flex-row gap-1">
                    <span>"Are you sure you want to delete this chapter?"</span>
                    <button
                        class="btn btn-error"
                        on:click=move |_| {
                            delete_action.dispatch(chapter().apub_id);
                            confirm_delete.set(false);
                            deleted.set(true);
                        }
                    >

                        <Icon icon=icondata::BsCheckLg class="w-8 h-8 m-auto" />
                    </button>
                    <button
                        class="btn btn-outline btn-error"
                        on:click=move |_| confirm_delete.set(false)
                    >
                        <Icon icon=icondata::BsXLg class="w-8 h-8 m-auto" />
                    </button>
                </div>
            </Show>
        </DialogForm>
    }
}

#[server]
pub async fn get_chapter(
    apub_id: Option<String>,
    id: String,
    seq: i32,
) -> Result<Option<Chapter>, ServerFnError> {
    use activitypub_federation::{
        config::Data,
        fetch::{object_id::ObjectId, webfinger::webfinger_resolve_actor},
    };
    use leptos_axum::extract;
    use rayon::prelude::*;
    use tower_sessions::Session;
    use wordforge_api::{
        api::chapter::get_toc_content,
        objects::{chapter, novel::DbNovel},
        util::generate_url_preview,
        DataHandle,
    };

    let data: Data<DataHandle> = extract().await?;
    let session: Session = extract().await?;

    let user_id = session.get::<String>("id").await.ok().flatten();

    let novel = if id.contains('@') {
        webfinger_resolve_actor(&id, &data)
            .await
            .map_err(ServerFnError::new)?
    } else if let Some(novel) = DbNovel::from_novel_id(&id, &data)
        .await
        .map_err(ServerFnError::new)?
    {
        novel
    } else {
        return Ok(None);
    };

    let editable = if let Some(user_id) = user_id {
        novel
            .data
            .as_ref()
            .ok_or_else(|| ServerFnError::new("Not found"))?
            .authors
            .par_iter()
            .any(|a| a.apub_id == user_id)
    } else {
        false
    };

    if let Some(apub_id) = apub_id {
        let toc = get_toc_content(&apub_id, &data)
            .await
            .unwrap_or_else(|e| format!("Something went wrong: {}", e));

        let chapter_id: ObjectId<chapter::Chapter> = apub_id.parse().map_err(ServerFnError::new)?;
        let chapter = chapter_id
            .dereference(&data)
            .await
            .map_err(|e| log::warn!("Failed to resolve {}: {}", apub_id, e))
            .ok();

        if let Some(chapter) = chapter {
            let ch = Chapter {
                apub_id: chapter.apub_id,
                npreview: generate_url_preview(&novel.data.as_ref().unwrap().title),
                cpreview: generate_url_preview(&chapter.title),
                novel_name: novel.data.unwrap().title,
                novel_id: id,
                editable,
                title: chapter.title,
                summary: chapter.summary,
                sensitive: chapter.sensitive,
                sequence: chapter.sequence,
                published: chapter.published.format("%A, %d %B, %Y").to_string(),
                updated: chapter
                    .updated
                    .map(|t| t.format("%A, %d %B, %Y").to_string()),
                content: chapter.content,
                toc,
            };
            return Ok(Some(ch));
        }
    }

    let chapter = match novel
        .resolve_chapter(seq, &data.pool)
        .await
        .map_err(ServerFnError::new)?
    {
        Some(chapter) => chapter
            .dereference(&data)
            .await
            .map_err(ServerFnError::new)?,
        None => return Ok(None),
    };

    let toc = get_toc_content(&chapter.apub_id, &data)
        .await
        .unwrap_or_else(|e| format!("Something went wrong: {}", e));

    let chapter = Chapter {
        apub_id: chapter.apub_id,
        npreview: generate_url_preview(&novel.data.as_ref().unwrap().title),
        cpreview: generate_url_preview(&chapter.title),
        novel_name: novel.data.unwrap().title,
        novel_id: id,
        editable,
        title: chapter.title,
        summary: chapter.summary,
        sensitive: chapter.sensitive,
        sequence: chapter.sequence,
        published: chapter.published.format("%A, %d %B, %Y").to_string(),
        updated: chapter
            .updated
            .map(|t| t.format("%A, %d %B, %Y").to_string()),
        content: chapter.content,
        toc,
    };

    Ok(Some(chapter))
}

#[server]
async fn edit_chapter(
    chapter_id: String,
    title: Option<String>,
    summary: Option<String>,
    sensitive: Option<bool>,
) -> Result<ChapterUpdate, ServerFnError> {
    use activitypub_federation::config::Data;
    use leptos_axum::extract;
    use tower_sessions::Session;
    use wordforge_api::{
        api::chapter::{edit_chapter, ChapterEdit},
        DataHandle,
    };

    let summary = Some(summary.unwrap_or_default());
    let sensitive = Some(sensitive.unwrap_or(false));

    let data: Data<DataHandle> = extract().await?;
    let session: Session = extract().await?;

    let user_id = session
        .get::<String>("id")
        .await?
        .ok_or_else(|| ServerFnError::new("Not signed in"))?;

    let info = ChapterEdit {
        title,
        summary,
        sensitive,
        ..Default::default()
    };

    let c = edit_chapter(&data, user_id.parse()?, chapter_id.parse()?, info).await?;

    Ok(ChapterUpdate {
        title: c.title,
        summary: c.summary,
        sensitive: c.sensitive,
        updated: c.updated.map(|t| t.format("%A, %d %B, %Y").to_string()),
    })
}

#[server]
async fn delete_chapter(apub_id: String) -> Result<(), ServerFnError> {
    use activitypub_federation::config::Data;
    use leptos_axum::extract;
    use tower_sessions::Session;
    use wordforge_api::{api::chapter::delete_chapter, DataHandle};

    let data = extract::<Data<DataHandle>>().await?;
    let session = extract::<Session>().await?;

    let user_id = session
        .get::<String>("id")
        .await?
        .ok_or_else(|| ServerFnError::new("Not signed in"))?;

    delete_chapter(&data, apub_id.parse()?, user_id.parse()?).await?;

    Ok(())
}
