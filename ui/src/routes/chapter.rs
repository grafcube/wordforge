use crate::components::{textarea_resize, DialogForm};
use leptos::{ev::KeyboardEvent, html::*, *};
use leptos_icons::*;
use leptos_router::*;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct ChapterItem {
    pub preview: String,
    pub apub_id: String,
    pub novel: String,
    pub title: String,
    pub summary: String,
    pub sensitive: bool,
    pub sequence: i32,
    pub published: String,
    pub published_exact: String,
    pub updated: Option<String>,
    pub updated_exact: Option<String>,
}

#[component]
pub fn ReorderChaptersButton(
    uuid: StoredValue<String>,
    chapters: Resource<(), Result<Vec<ChapterItem>, ServerFnError>>,
) -> impl IntoView {
    let chapter_button = create_node_ref::<Dialog>();
    let hide_button = move || {
        chapters()
            .and_then(|v| v.map(|v| v.len()).ok())
            .unwrap_or(0)
            == 0
    };

    view! {
        <Suspense>
            <button
                class="flex flex-row gap-1 p-1 rounded-md text-neutral-content"
                class:hidden=hide_button
                on:click=move |_| {
                    if let Some(v) = chapter_button() {
                        v.show_modal().unwrap()
                    }
                }
            >

                <Icon icon=icondata::TbMenuOrder class="w-6 h-6 my-auto" />
                <span class="my-auto pr-1">"Reorder"</span>
            </button>
            <ReorderChapters uuid node_ref=chapter_button chapters />
        </Suspense>
    }
}

#[component]
fn ReorderChapters(
    uuid: StoredValue<String>,
    node_ref: NodeRef<Dialog>,
    chapters: Resource<(), Result<Vec<ChapterItem>, ServerFnError>>,
) -> impl IntoView {
    let chapter_list = RwSignal::new(Vec::new());
    let update_order = Action::new(move |chapter_list: &Vec<String>| {
        update_chapters_order(uuid(), chapter_list.clone())
    });
    let reorder_res = move || update_order.value()().map(|v| v.map(|()| chapters.refetch()));

    view! {
        <dialog node_ref=node_ref class="modal modal-bottom sm:modal-middle">
            <div class="modal-box">
                <form method="dialog" class="flex flex-row w-full justify-between">
                    <h3 class="my-auto text-xl text-neutral-content">"Reorder chapters"</h3>
                    <button class="my-auto p-1 rounded-full transition ease-in-out duration-[400ms] hover:bg-base-300">
                        <Icon icon=icondata::CgClose class="w-8 h-8 my-auto" />
                    </button>
                </form>
                <ErrorBoundary fallback=move |e| {
                    view! {
                        <p class="text-error">
                            {move || {
                                e.get()
                                    .into_iter()
                                    .map(|(_, e)| {
                                        view! { <span>{e.to_string()}</span> }
                                    })
                                    .collect_view()
                            }}

                        </p>
                    }
                }>
                    {move || {
                        chapters.get().map(|v| { v.map(|v| chapter_list.set(v)) });
                        update!(| chapter_list | chapter_list.sort_by_key(| c | c.sequence));
                    }}

                </ErrorBoundary>
                <ul>
                    {move || {
                        chapter_list()
                            .into_iter()
                            .enumerate()
                            .map(|(index, chapter)| {
                                view! { <ChapterOrderItem index chapter chapter_list /> }
                            })
                            .collect_view()
                    }}

                </ul>
                <div class="flex flex-row gap-1 p-2 w-fit mx-auto">
                    <button
                        class="btn btn-primary"
                        on:click=move |_| {
                            update_order
                                .dispatch(
                                    chapter_list()
                                        .into_iter()
                                        .map(|c: ChapterItem| c.apub_id)
                                        .collect(),
                                )
                        }
                    >

                        "Save"
                    </button>
                    <span
                        class="loading loading-spinner loading-md"
                        class:hidden=move || !update_order.pending()()
                    ></span>
                </div>
                {reorder_res}
            </div>
            <form method="dialog" class="modal-backdrop">
                <button class="cursor-default"></button>
            </form>
        </dialog>
    }
}

#[component]
fn ChapterOrderItem(
    index: usize,
    chapter: ChapterItem,
    chapter_list: RwSignal<Vec<ChapterItem>>,
) -> impl IntoView {
    view! {
        <li class="flex flex-row justify-between w-full">
            <Icon icon=icondata::TbMenuOrder class="w-16 h-16 my-auto cursor-grab" />
            <h4 class="font-bold my-auto overflow-hidden text-ellipsis w-full max-w-full whitespace-nowrap">
                {chapter.title}
            </h4>
            <div class="flex flex-row gap-4">
                <span class="my-auto text-lg text-neutral-content">{chapter.sequence}</span>
                <button on:click=move |_| {
                    update!(| chapter_list | chapter_list.swap(index, index + 1))
                }>
                    <Icon icon=icondata::CgChevronDown class="w-8 h-8 my-auto" />
                </button>
                <button on:click=move |_| {
                    update!(| chapter_list | chapter_list.swap(index, index - 1))
                }>
                    <Icon icon=icondata::CgChevronUp class="w-8 h-8 my-auto" />
                </button>
            </div>
        </li>
    }
}

#[component]
pub fn NewChapterButton(
    uuid: StoredValue<String>,
    chapters: Resource<(), Result<Vec<ChapterItem>, ServerFnError>>,
) -> impl IntoView {
    let chapter_button = create_node_ref::<Dialog>();

    view! {
        <Suspense>
            <button
                class="flex flex-row gap-1 p-1 rounded-md text-neutral-content"
                on:click=move |_| {
                    if let Some(v) = chapter_button() {
                        v.show_modal().unwrap()
                    }
                }
            >

                <Icon icon=icondata::CgMathPlus class="w-6 h-6 my-auto" />
                <span class="my-auto pr-1">"New chapter"</span>
            </button>
            <NewChapter novel=uuid() node_ref=chapter_button chapters />
        </Suspense>
    }
}

#[component]
fn NewChapter(
    novel: String,
    node_ref: NodeRef<Dialog>,
    chapters: Resource<(), Result<Vec<ChapterItem>, ServerFnError>>,
) -> impl IntoView {
    let create = create_server_action::<CreateChapter>();
    let response = create.value();
    let err = move || {
        response().and_then(|response| match response {
            Ok(()) => {
                if let Some(v) = node_ref() {
                    v.close();
                }
                chapters.refetch();
                None
            }
            Err(e) => Some(e.to_string()),
        })
    };

    let summary = create_node_ref::<Textarea>();

    view! {
        <DialogForm
            class="flex flex-col justify-center text-center place-content-center items-center space-y-4 p-4 w-full"
            title="New Chapter"
            node_ref
            action=create
            pre_form=move || {
                view! {
                    <div
                        class="card mx-1 my-4 bg-error text-error-content"
                        class:hidden=move || err().is_none()
                    >
                        <div class="card-body">{err}</div>
                    </div>
                }
            }
        >

            <div class="relative w-full">
                <textarea
                    name="title"
                    id="title"
                    placeholder=" "
                    rows=1
                    wrap="soft"
                    class="floating-input input-bordered focus:input-primary rounded-xl text-xl w-full peer"
                    on:keydown=move |ev: KeyboardEvent| {
                        if ev.key() == "Enter" {
                            ev.prevent_default();
                            summary().unwrap().focus().unwrap();
                        }
                    }

                    required
                ></textarea>
                <label for="title" class="floating-label">
                    "Title"
                </label>
            </div>
            <div class="relative w-full">
                <textarea
                    name="summary"
                    id="summary"
                    placeholder=" "
                    node_ref=summary
                    on:input=textarea_resize
                    on:paste=textarea_resize
                    class="floating-input input-bordered focus:input-primary rounded-xl text-xl w-full peer"
                ></textarea>
                <label for="summary" class="floating-label">
                    "Summary"
                </label>
            </div>
            <label class="flex flex-row gap-2 justify-start cursor-pointer label w-full">
                <input
                    type="checkbox"
                    name="sensitive"
                    value="true"
                    class="checkbox checkbox-error"
                />
                <span class="label-text text-xl">"Content warning"</span>
            </label>
            <input type="hidden" name="novel" value=novel.clone() />
            <div class="relative">
                <button class="btn btn-primary" type="submit">
                    "Create"
                </button>
            </div>
        </DialogForm>
    }
}

#[component]
fn ChapterEntry(chapter: ChapterItem, preview: StoredValue<Option<String>>) -> impl IntoView {
    view! {
        <A
            href=format!(
                "/chapter/{}/{}/{}/{}?id={}",
                chapter.novel,
                chapter.sequence,
                preview().unwrap_or_default(),
                chapter.preview,
                chapter.apub_id,
            )

            class="flex flex-col gap-1 w-full p-2 rounded-xl transition ease-in-out duration-[400ms] hover:bg-base-300"
        >
            <div class="flex flex-row gap-1 justify-start max-w-full">
                <span class="mr-1 my-auto text-right text-neutral-content">{chapter.sequence}</span>
                <span class:hidden=!chapter.sensitive class="badge badge-error my-auto">
                    "CW"
                </span>
                <h3 class="font-bold my-auto overflow-hidden text-ellipsis w-full max-w-full whitespace-nowrap">
                    {chapter.title}
                </h3>
                <div class="tooltip" data-tip=move || chapter.published_exact.clone()>
                    <span class="text-neutral-content text-right w-fit whitespace-nowrap">
                        {chapter.published}
                    </span>
                </div>
                <div class="ml-auto">
                    {chapter
                        .updated_exact
                        .map(|v| {
                            view! {
                                <div class="tooltip" data-tip=move || v.clone()>
                                    <span class="text-neutral-content w-fit overflow-hidden overflow-ellipsis whitespace-nowrap">
                                        {chapter.updated}
                                    </span>
                                </div>
                            }
                        })}

                </div>
            </div>
            <div class="italic h-6 md:text-sm overflow-hidden overflow-ellipsis text-neutral-content">
                {chapter
                    .summary
                    .lines()
                    .map(|p| {
                        view! { <p>{p.to_string()}</p> }
                    })
                    .collect::<Vec<_>>()}
            </div>
        </A>
    }
}

#[component]
pub fn ChapterList(
    chapters: Resource<(), Result<Vec<ChapterItem>, ServerFnError>>,
    preview: StoredValue<Option<String>>,
) -> impl IntoView {
    let chapters_found = move || {
        chapters
            .get()
            .map(|v| v.map(|v| !v.is_empty()))
            .unwrap_or(Ok(true))
            .unwrap_or(true)
    };

    view! {
        <span class:hidden=chapters_found class="w-full mx-auto italic text-neutral-content">
            "No chapters found"
        </span>
        <ul>
            <ErrorBoundary fallback=move |e| {
                view! {
                    <p class="text-error">
                        {move || {
                            e.get()
                                .into_iter()
                                .map(|(_, e)| {
                                    view! { <span>{e.to_string()}</span> }
                                })
                                .collect_view()
                        }}

                    </p>
                }
            }>
                {move || {
                    chapters
                        .get()
                        .map(|v| {
                            v.map(|v| {
                                v.into_iter()
                                    .map(|c| {
                                        view! {
                                            <li class="pl-1 mx-auto w-full">
                                                <ChapterEntry chapter=c preview />
                                            </li>
                                        }
                                    })
                                    .collect_view()
                            })
                        })
                }}

            </ErrorBoundary>
        </ul>
    }
}

#[server]
async fn create_chapter(
    novel: String,
    title: String,
    summary: String,
    sensitive: Option<bool>,
) -> Result<(), ServerFnError> {
    use activitypub_federation::config::Data;
    use leptos_axum::extract;
    use tower_sessions::Session;
    use url::{ParseError, Url};
    use wordforge_api::{
        activities::add::NewChapter,
        api::chapter::{new_chapter, ChapterError},
        DataHandle,
    };

    let sensitive = sensitive.unwrap_or(false);

    let data: Data<DataHandle> = extract().await?;
    let session: Session = extract().await?;

    let apub_id: Url = session
        .get::<String>("id")
        .await?
        .ok_or(ChapterError::Unauthorized)?
        .parse()
        .map_err(|e: ParseError| ChapterError::InternalError(e.to_string()))?;

    let chapter = NewChapter {
        title,
        summary,
        sensitive,
    };

    new_chapter(novel, chapter, apub_id, &data).await?;

    Ok(())
}

#[server]
async fn update_chapters_order(uuid: String, order: Vec<String>) -> Result<(), ServerFnError> {
    use activitypub_federation::{config::Data, fetch::webfinger::webfinger_resolve_actor};
    use leptos_axum::extract;
    use tower_sessions::Session;
    use wordforge_api::{api::novel::reorder_chapters, objects::novel::DbNovel, DataHandle};

    let data = extract::<Data<DataHandle>>().await?;
    let session = extract::<Session>().await?;

    let author = session
        .get::<String>("id")
        .await?
        .ok_or_else(|| ServerFnError::new("Not signed in"))?;

    let novel = if uuid.contains('@') {
        webfinger_resolve_actor(&uuid, &data)
            .await
            .map_err(ServerFnError::new)?
    } else {
        DbNovel::from_novel_id(&uuid, &data)
            .await
            .map_err(ServerFnError::new)?
            .ok_or_else(|| ServerFnError::new("Novel not found"))?
    };

    reorder_chapters(&data, author.parse()?, novel.apub_id.parse()?, &order).await?;

    Ok(())
}

#[server]
pub async fn get_chapter_list(novel: String) -> Result<Vec<ChapterItem>, ServerFnError> {
    use activitypub_federation::config::Data;
    use chrono_humanize::HumanTime;
    use leptos_axum::extract;
    use wordforge_api::{api::chapter::get_chapters, util::generate_url_preview, DataHandle};

    let data: Data<DataHandle> = extract().await?;

    let chapters = get_chapters(novel.as_str(), &data)
        .await?
        .into_iter()
        .map(|c| ChapterItem {
            preview: generate_url_preview(&c.title),
            apub_id: c.href.to_string(),
            novel: novel.clone(),
            title: c.title,
            summary: c.summary,
            sensitive: c.sensitive,
            sequence: c.sequence,
            published: HumanTime::from(c.published).to_string(),
            published_exact: c.published.to_rfc2822(),
            updated: c.updated.map(|c| format!(" ({})", HumanTime::from(c))),
            updated_exact: c.updated.map(|u| u.to_rfc2822()),
        })
        .collect();

    Ok(chapters)
}
