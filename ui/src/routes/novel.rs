use crate::{
    app::ValidationResult,
    components::{textarea_resize, DialogForm, SelectListbox},
    fallback::*,
    routes::chapter::{get_chapter_list, ChapterList, NewChapterButton, ReorderChaptersButton},
};
use isolang::Language;
use leptos::{
    ev::KeyboardEvent,
    html::*,
    logging::{debug_warn, error},
    *,
};
use leptos_icons::*;
use leptos_meta::*;
use leptos_router::*;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq)]
pub struct AuthorItem {
    pub href: String,
    pub role: String,
    pub name: Result<String, String>,
}

#[derive(Clone, Default, Serialize, Deserialize)]
pub struct Novel {
    pub apub_id: String,
    pub preview: String,
    pub name: String,
    pub summary: String,
    pub authors: Vec<AuthorItem>,
    pub genre: String,
    pub tags: Vec<String>,
    pub language: Language,
    pub sensitive: bool,
    pub published: Option<String>,
    pub is_local: bool,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct NovelUpdate {
    name: String,
    summary: String,
    genre: String,
    language: Language,
    tags: Vec<String>,
    sensitive: bool,
}

#[component]
pub fn CreateBook() -> impl IntoView {
    let summary = create_node_ref::<Textarea>();
    let cw = create_node_ref::<Input>();

    let create = create_server_action::<CreateNovel>();
    let pending = create.pending();
    let response = create.value();
    let err = move || {
        response().and_then(|v| match v {
            Ok(Ok(_)) => None,
            Ok(Err(e)) => Some(e),
            Err(e) => Some(e.to_string()),
        })
    };

    let genres = Resource::once(get_genres);
    let roles = Resource::once(get_roles);
    let langs = Resource::once(get_langs);

    view! {
        <Title text="Create a new book" />
        <div class="mx-auto w-full">
            <h1 class="p-2 text-3xl text-center">"Create a new book"</h1>
            <ActionForm action=create class="form-control mx-auto gap-4 p-4 w-full max-w-xl">
                <div
                    class="card mx-1 my-4 bg-error text-error-content"
                    class:hidden=move || err().is_none()
                >
                    <div class="card-body">{err}</div>
                </div>
                <div class="relative">
                    <input
                        name="title"
                        id="title"
                        placeholder=" "
                        autocomplete="off"
                        class="floating-input input-bordered focus:input-primary rounded-xl text-xl w-full peer"
                        on:keydown=move |ev: KeyboardEvent| {
                            if ev.key() == "Enter" {
                                ev.prevent_default();
                                summary().unwrap().focus().unwrap();
                            }
                        }

                        required
                    />
                    <label for="title" class="floating-label">
                        "Title"
                    </label>
                </div>
                <div class="relative">
                    <textarea
                        name="summary"
                        id="summary"
                        placeholder=" "
                        node_ref=summary
                        on:input=textarea_resize
                        on:paste=textarea_resize
                        class="floating-input input-bordered focus:input-primary rounded-xl text-xl w-full peer"
                    ></textarea>
                    <label for="summary" class="floating-label">
                        "Summary"
                    </label>
                </div>
                <Transition>
                    {move || {
                        genres
                            .get()
                            .map(|v| match v {
                                Ok(items) => {
                                    view! {
                                        <SelectListbox
                                            name="genre"
                                            label="Genre"
                                            initial="Select a genre"
                                            items
                                        />
                                    }
                                        .into_view()
                                }
                                Err(e) => {
                                    error!("{}", e.to_string());
                                    view! { <span>"Something went wrong"</span> }.into_view()
                                }
                            })
                    }}

                </Transition>
                <Transition>
                    {move || {
                        roles
                            .get()
                            .map(|v| match v {
                                Ok(items) => {
                                    view! {
                                        <SelectListbox
                                            name="role"
                                            label="Your role"
                                            initial="Select your role"
                                            items
                                        />
                                    }
                                        .into_view()
                                }
                                Err(e) => {
                                    error!("{}", e.to_string());
                                    view! { <span>"Something went wrong"</span> }.into_view()
                                }
                            })
                    }}

                </Transition>
                <Transition>
                    {move || {
                        langs
                            .get()
                            .map(|v| match v {
                                Ok(items) => {
                                    view! {
                                        <SelectListbox
                                            name="lang"
                                            label="Language"
                                            initial="Select the book's language"
                                            items
                                        />
                                    }
                                        .into_view()
                                }
                                Err(e) => {
                                    error!("{}", e.to_string());
                                    view! { <span>"Something went wrong"</span> }.into_view()
                                }
                            })
                    }}

                </Transition>
                <div class="relative">
                    <input
                        name="tags"
                        id="tags"
                        placeholder=" "
                        autocomplete="off"
                        class="floating-input input-bordered focus:input-primary rounded-xl text-xl w-full peer"
                        on:keydown=move |ev: KeyboardEvent| {
                            if ev.key() == "Enter" {
                                ev.prevent_default();
                                cw().unwrap().focus().unwrap();
                            }
                        }
                    />

                    <label for="tags" class="floating-label">
                        "Tags"
                    </label>
                </div>
                <label class="flex flex-row gap-2 justify-start cursor-pointer label">
                    <input type="checkbox" name="cw" value="true" class="checkbox checkbox-error" />
                    <span class="label-text text-xl">"Content warning"</span>
                </label>
                <button class="btn btn-primary mx-auto" type="submit">
                    "Create"
                </button>
                <span
                    class="loading loading-spinner loading-md m-auto"
                    class:hidden=move || !pending()
                ></span>
            </ActionForm>
        </div>
    }
}

#[component]
pub fn NovelPage() -> impl IntoView {
    let params = use_params_map();
    let Some(uuid) = params.with(|p| p.get("uuid").cloned()) else {
        return view! { <NotFoundPage /> }.into_view();
    };
    let uuid = StoredValue::new(uuid);

    let validate = expect_context::<Callback<(), Option<ValidationResult>>>();

    let chapters = Resource::once(move || get_chapter_list(uuid()));
    let novel = RwSignal::new(Novel::default());
    let novel_res = Resource::once(move || get_novel(uuid()));

    let preview = params.with(|p| p.get("preview").cloned());
    let preview = StoredValue::new(preview);
    let (bad_preview, set_bad_preview) = create_signal(true);

    view! {
        <Title text="Novel" />
        <Suspense>
            {move || {
                let name = with!(|novel| novel.name.clone());
                if !name.is_empty() {
                    Some(
                        view! {
                            <Link
                                href=format!("/novel/{}/feed.atom", uuid())
                                type_="application/atom+xml"
                                rel="alternate"
                                title=with!(|novel| novel.name.clone())
                            />
                        },
                    )
                } else {
                    None
                }
            }}
        </Suspense>

        <div class="mx-auto max-w-2xl px-4">
            <Suspense fallback=move || {
                view! { <span class="loading loading-spinner loading-lg m-auto"></span> }
            }>
                {move || {
                    novel_res
                        .get()
                        .map(|v| {
                            match v {
                                Err(e) => {
                                    error!("novel server fn: {}", e);
                                    view! { <InternalErrorPage /> }.into_view()
                                }
                                Ok(Err(e)) => {
                                    debug_warn!("novel view: {}", e);
                                    view! { <NotFoundPage /> }.into_view()
                                }
                                Ok(Ok(n)) => {
                                    if let Some(preview) = preview() {
                                        if preview != n.preview {
                                            return view! { <NotFoundPage /> }.into_view();
                                        }
                                    } else {
                                        use_navigate()(
                                            &format!("/novel/{}/{}", uuid(), n.preview),
                                            NavigateOptions {
                                                replace: true,
                                                ..Default::default()
                                            },
                                        );
                                    }
                                    set_bad_preview(false);
                                    novel.set(n);
                                    view! { <NovelView novel uuid /> }.into_view()
                                }
                            }
                        })
                }}
                <div
                    class="flex flex-col w-full rounded-xl px-4 py-2 my-2 bg-base-200"
                    class:hidden=bad_preview
                >
                    <div class="flex flex-row justify-between w-full">
                        <h2 class="text-neutral-content text-lg my-auto p-1">"Chapters"</h2>
                        <div
                            class="flex flex-row gap-1"
                            class:hidden=move || {
                                !matches!(
                                    validate(()),
                                    Some(Ok(user))
                                    if novel()
                                        .authors
                                        .into_iter()
                                        .map(|a| a.href)
                                        .collect::<Vec<_>>()
                                        .contains(&user.id)
                                )
                            }
                        >

                            <ReorderChaptersButton uuid chapters />
                            <NewChapterButton uuid chapters />
                        </div>
                    </div>
                    <div class="my-2 gap-1 w-full">
                        <ChapterList chapters preview />
                    </div>
                </div>
            </Suspense>
        </div>
    }
    .into_view()
}

#[component]
fn NovelView(novel: RwSignal<Novel>, uuid: StoredValue<String>) -> impl IntoView {
    let validate = use_context::<Callback<(), Option<ValidationResult>>>().unwrap();
    let edit_node = create_node_ref::<Dialog>();

    #[allow(unused_variables)]
    #[cfg(feature = "hydrate")]
    let genre_href = move || {
        let path: js_sys::JsString = novel().genre.to_lowercase().into();
        let re = js_sys::RegExp::new(r"\s+", "g");
        let path = path.replace_all_by_pattern(&re, "_").as_string().unwrap();
        format!("/explore/{}", path)
    };

    #[allow(unused_variables)]
    #[cfg(feature = "ssr")]
    let genre_href = move || {
        let path = novel().genre.to_lowercase();
        let re = regex::Regex::new(r"\s+").unwrap();
        let path = re.replace_all(&path, "_").to_string();
        format!("/explore/{}", path)
    };

    view! {
        <Title text=move || novel().name />
        <div class="flex flex-row gap-2 w-full justify-center">
            <h1 class="text-center p-2 text-3xl">{move || novel().name}</h1>
            <button
                class="p-1"
                class:hidden=move || {
                    !matches!(
                        validate(()),
                        Some(Ok(user))
                        if novel()
                            .authors
                            .into_iter()
                            .map(|a| a.href)
                            .collect::<Vec<_>>()
                            .contains(&user.id)
                    )
                }

                on:click=move |_| {
                    if let Some(edit) = edit_node() {
                        edit.show_modal().unwrap();
                    }
                }
            >

                <Icon icon=icondata::BiPencilRegular class="w-8 h-8 my-auto" />
            </button>
            <NovelEdit novel node_ref=edit_node />
        </div>
        <div class="flex flex-row overflow-auto whitespace-nowrap gap-1 text-xl md:text-base">
            <A class="badge badge-primary text-base h-fit" href=genre_href>

                {move || novel().genre}
            </A>
            <A
                class="badge badge-secondary text-base h-fit"
                href=move || format!("/explore/{}", novel().language.to_639_1().unwrap_or_default())
            >
                {move || novel().language.to_name()}
            </A>
            <span
                class="badge badge-error text-base h-fit cursor-default"
                class:hidden=move || !novel().sensitive
            >
                "Content warning"
            </span>
            <div class="mx-auto"></div>
            <div class:hidden=move || novel().is_local>
                <A class="badge badge-accent text-base h-fit" href=move || novel().apub_id>
                    "Go to host instance"
                </A>
            </div>
            <a
                class="badge badge-accent text-base h-fit"
                href=move || format!("/novel/{}/feed.atom", uuid())
                rel="external"
            >
                "Feed"
            </a>
        </div>
        <div class="bg-base-200 rounded-xl text-xl md:text-base overflow-auto max-h-40 my-2 px-4 py-2">
            {move || {
                novel()
                    .summary
                    .lines()
                    .map(|line| { view! { <p>{line.to_string()}</p> }.into_view() })
                    .collect::<Vec<_>>()
            }}
            {move || {
                novel()
                    .published
                    .map(|published| {
                        view! {
                            <span class="block mt-2 italic text-neutral-content">
                                "Created " {published}
                            </span>
                        }
                    })
            }}

        </div>
        <AuthorPage novel />
        <div class="flex flex-row justify-start gap-2 h-fit whitespace-nowrap overflow-x-auto overflow-y-hidden">
            {move || {
                novel()
                    .tags
                    .iter()
                    .map(|tag| {
                        view! {
                            <a
                                href=format!("/explore/tags/{tag}")
                                class="link link-hover hover:text-gray-400 italic mb-2 mt-auto text-xl md:text-base"
                            >
                                {format!("#{tag}")}
                            </a>
                        }
                            .into_view()
                    })
                    .collect::<Vec<_>>()
            }}

        </div>
    }
}

#[component]
fn NovelEdit(novel: RwSignal<Novel>, node_ref: NodeRef<Dialog>) -> impl IntoView {
    let edit = create_server_action::<EditNovel>();
    let response = edit.value();
    let err = move || {
        response().and_then(|response| match response {
            Ok(res) => {
                if let Some(dialog) = node_ref() {
                    dialog.close();
                }
                update!(|novel| {
                    novel.name = res.name;
                    novel.summary = res.summary;
                    novel.genre = res.genre;
                    novel.tags = res.tags;
                    novel.language = res.language;
                    novel.sensitive = res.sensitive;
                });
                None
            }
            Err(e) => Some(e.to_string()),
        })
    };

    let summary = create_node_ref::<Textarea>();
    let genres = Resource::once(get_genres);
    let langs = Resource::once(get_langs);

    let delete_action = Action::new(move |apub_id: &String| delete_novel(apub_id.clone()));
    let confirm_delete = RwSignal::new(false);
    let deleted = RwSignal::new(false);

    view! {
        <DialogForm
            class="flex flex-col justify-center text-center place-content-center items-center space-y-4 p-4 w-full"
            title="Novel Metadata"
            node_ref
            action=edit
            pre_form=move || {
                view! {
                    <div
                        class="card mx-1 my-4 bg-error text-error-content"
                        class:hidden=move || err().is_none()
                    >
                        <div class="card-body">{err}</div>
                    </div>
                }
            }
        >

            <div class="relative w-full">
                <textarea
                    name="title"
                    id="title"
                    placeholder=" "
                    rows=1
                    wrap="soft"
                    class="floating-input input-bordered focus:input-primary rounded-xl text-xl w-full peer"
                    on:keydown=move |ev: KeyboardEvent| {
                        if ev.key() == "Enter" {
                            ev.prevent_default();
                            summary().unwrap().focus().unwrap();
                        }
                    }
                >

                    {move || novel().name}
                </textarea>
                <label for="title" class="floating-label">
                    "Title"
                </label>
            </div>
            <div class="relative w-full">
                <textarea
                    name="summary"
                    id="summary"
                    placeholder=" "
                    on:input=textarea_resize
                    on:paste=textarea_resize
                    class="floating-input input-bordered focus:input-primary rounded-xl text-xl w-full peer"
                >
                    {move || novel().summary}
                </textarea>
                <label for="summary" class="floating-label">
                    "Summary"
                </label>
            </div>
            <div class="relative w-full text-left">
                {move || {
                    genres
                        .get()
                        .map(|v| match v {
                            Ok(items) => {
                                view! {
                                    <SelectListbox
                                        name="genre"
                                        label="Genre"
                                        initial="Select a genre"
                                        selection=novel().genre
                                        items
                                    />
                                }
                                    .into_view()
                            }
                            Err(e) => {
                                error!("{}", e.to_string());
                                view! { <span>"Something went wrong"</span> }.into_view()
                            }
                        })
                }}

            </div>
            <div class="relative w-full text-left">
                {move || {
                    langs
                        .get()
                        .map(|v| match v {
                            Ok(items) => {
                                view! {
                                    <SelectListbox
                                        name="lang"
                                        label="Language"
                                        initial="Select the book's language"
                                        selection=novel().language.to_name().to_string()
                                        items
                                    />
                                }
                                    .into_view()
                            }
                            Err(e) => {
                                error!("{}", e.to_string());
                                view! { <span>"Something went wrong"</span> }.into_view()
                            }
                        })
                }}

            </div>
            <div class="relative w-full">
                <input
                    name="tags"
                    id="tags"
                    placeholder=" "
                    value=move || novel().tags.join(" ")
                    autocomplete="off"
                    class="floating-input input-bordered focus:input-primary rounded-xl text-xl w-full peer"
                />
                <label for="tags" class="floating-label">
                    "Tags"
                </label>
            </div>
            <label class="flex flex-row gap-2 justify-start cursor-pointer label w-full">
                <input
                    type="checkbox"
                    name="sensitive"
                    value="true"
                    checked=move || novel().sensitive
                    class="checkbox checkbox-error"
                />
                <span class="label-text text-xl">"Content warning"</span>
            </label>
            <input type="hidden" name="novel" value=move || novel().apub_id />
            <div class="relative">
                <button class="btn btn-primary" type="submit">
                    "Save"
                </button>
            </div>
            <Show
                when=confirm_delete
                fallback=move || {
                    view! {
                        <button
                            class="relative btn btn-outline btn-error"
                            class:hidden=deleted
                            on:click=move |_| confirm_delete.set(true)
                        >
                            <Icon icon=icondata::CgTrash class="w-6 h-6 m-auto" />
                            <span>"Delete"</span>
                        </button>
                        <span
                            class="loading loading-spinner loading-md m-auto"
                            class:hidden=move || !deleted()
                        ></span>
                        <Suspense>
                            <ErrorBoundary fallback=move |e| {
                                view! {
                                    <div class="w-full px-4 py-2 my-1 rounded-md bg-error text-error-content">
                                        {e()
                                            .into_iter()
                                            .map(|(_, e)| view! { <p>{e.to_string()}</p> })
                                            .collect_view()}
                                    </div>
                                }
                            }>
                                {move || {
                                    if let Some(dialog) = node_ref() {
                                        dialog.close();
                                    }
                                    delete_action
                                        .value()()
                                        .map(|v| {
                                            v.map(|()| use_navigate()("/", NavigateOptions::default()))
                                        })
                                }}

                            </ErrorBoundary>
                        </Suspense>
                    }
                }
            >

                <div class="flex flex-row gap-1">
                    <span>"Are you sure you want to delete this book?"</span>
                    <button
                        class="btn btn-error"
                        on:click=move |_| {
                            delete_action.dispatch(novel().apub_id);
                            confirm_delete.set(false);
                            deleted.set(true);
                        }
                    >

                        <Icon icon=icondata::BsCheckLg class="w-8 h-8 m-auto" />
                    </button>
                    <button
                        class="btn btn-outline btn-error"
                        on:click=move |_| confirm_delete.set(false)
                    >
                        <Icon icon=icondata::BsXLg class="w-8 h-8 m-auto" />
                    </button>
                </div>
            </Show>
        </DialogForm>
    }
}

#[component]
fn AuthorPage(novel: RwSignal<Novel>) -> impl IntoView {
    let usernames = Resource::new(
        move || novel().authors,
        |authors| get_usernames(Some(authors)),
    );

    view! {
        <div class="bg-base-200 rounded-xl w-full px-4 py-2 my-2">
            <h2 class="text-neutral-content">"Authors"</h2>
            <Suspense fallback=move || {
                view! { <span class="loading loading-spinner loading-lg m-auto"></span> }
            }>
                <ErrorBoundary fallback=move |e| {
                    let err = e()
                        .into_iter()
                        .map(|(_, e)| e.to_string())
                        .collect::<Vec<_>>()
                        .join("\n");
                    error!("usernames: {}", err);
                    view! { <span class="px-4 py-2 my-2">"Something went wrong"</span> }
                }>
                    {move || {
                        usernames().map(|v| { v.map(|users| view! { <AuthorView users /> }) })
                    }}

                </ErrorBoundary>
            </Suspense>
        </div>
    }
}

#[component]
fn AuthorView(users: Vec<AuthorItem>) -> impl IntoView {
    let is_users_empty = users.is_empty();

    view! {
        <span class:hidden=!is_users_empty class="w-full mx-auto italic text-neutral-content">
            "No users found"
        </span>
        <div class="flex flex-row justify-start flex-wrap gap-1 overflow-auto max-h-40">
            {users
                .into_iter()
                .map(|res| {
                    let (apub_id, role) = (res.href, res.role);
                    match res.name {
                        Err(e) => {
                            debug_warn!("UNKNOWN: {}", e);
                            view! {
                                <a
                                    href=apub_id
                                    class="flex flex-col gap-0 bg-base-300 rounded-full px-4"
                                >
                                    <span class="my-auto text-xs text-base-content">{role}</span>
                                    <span class="my-auto text-neutral-content">"UNKNOWN"</span>
                                </a>
                            }
                        }
                        Ok(v) => {
                            view! {
                                <a
                                    href=apub_id
                                    class="flex flex-row gap-1 bg-base-300 rounded-full pr-4"
                                >
                                    <span class="w-6 h-6 ml-2 m-auto rounded-full bg-pink-500"></span>
                                    <div class="flex flex-col gap-0 h-10">
                                        {role
                                            .ne("None")
                                            .then(|| {
                                                view! {
                                                    <span class="my-auto text-xs text-neutral-content">
                                                        {role}
                                                    </span>
                                                }
                                            })}
                                        <span class="my-auto max-w-[16ch] whitespace-nowrap text-ellipsis overflow-hidden">
                                            {v}
                                        </span>
                                    </div>
                                </a>
                            }
                        }
                    }
                })
                .collect_view()}
        </div>
    }
}

#[server]
async fn create_novel(
    title: String,
    summary: String,
    genre: String,
    role: String,
    lang: String,
    tags: Option<String>,
    cw: Option<bool>,
) -> Result<Result<(), String>, ServerFnError> {
    use activitypub_federation::config::Data;
    use leptos_axum::extract;
    use std::str::FromStr;
    use tower_sessions::Session;
    use wordforge_api::{
        api::novel::{self, NewNovel},
        enums::*,
        DataHandle,
    };

    let cw = cw.unwrap_or(false);

    let data: Data<DataHandle> = extract().await?;
    let session: Session = extract().await?;

    let apub_id = session
        .get::<String>("id")
        .await?
        .ok_or_else(|| ServerFnError::new("Not signed in"))?;

    let info = NewNovel {
        title,
        summary,
        genre: match Genres::from_str(&genre) {
            Ok(g) => g,
            Err(_) => {
                return Ok(Err("Select a genre".to_string()));
            }
        },
        role: match Roles::from_str(&role) {
            Ok(r) => r,
            Err(_) => {
                return Ok(Err("Select your role".to_string()));
            }
        },
        lang,
        sensitive: cw,
        tags: tags.unwrap_or_default(),
    };

    let id = novel::create_novel(data, &apub_id, info).await?;

    leptos_axum::redirect(&format!("/novel/{}", id));

    Ok(Ok(()))
}

#[server]
async fn get_genres() -> Result<Vec<String>, ServerFnError> {
    use strum::IntoEnumIterator;
    use wordforge_api::enums::Genres;

    Ok(Genres::iter().map(|g| g.to_string()).collect())
}

#[server]
async fn get_roles() -> Result<Vec<String>, ServerFnError> {
    use strum::IntoEnumIterator;
    use wordforge_api::enums::Roles;

    Ok(Roles::iter().map(|g| g.to_string()).collect())
}

#[server]
async fn get_langs() -> Result<Vec<String>, ServerFnError> {
    Ok(isolang::languages()
        .filter_map(|lang| lang.to_639_1().map(|_| lang.to_name().to_string()))
        .collect())
}

#[server]
async fn get_novel(uuid: String) -> Result<Result<Novel, String>, ServerFnError> {
    use activitypub_federation::config::Data;
    use leptos_axum::extract;
    use url::Url;
    use wordforge_api::{
        api::novel::{self, NovelError},
        util::generate_url_preview,
        DataHandle,
    };

    let data: Data<DataHandle> = extract().await?;

    match novel::get_novel(&uuid, &data).await {
        Ok(n) => {
            let Some(ndata) = n.data else {
                return Err(ServerFnError::new("Not found"));
            };
            let apub_id: Url = n.apub_id.parse().unwrap();
            let domain = format!(
                "{}{}",
                apub_id.host_str().unwrap(),
                apub_id
                    .port()
                    .map(|p| format!(":{}", p))
                    .unwrap_or_default()
            );
            let novel = Novel {
                apub_id: n.apub_id,
                preview: generate_url_preview(&ndata.title),
                name: ndata.title,
                summary: ndata.summary,
                authors: ndata
                    .authors
                    .into_iter()
                    .map(|a| AuthorItem {
                        href: a.apub_id,
                        role: a.role.to_string(),
                        name: Err("Loading".to_string()),
                    })
                    .collect(),
                genre: ndata.genre.to_string(),
                tags: ndata.tags,
                language: ndata.language,
                sensitive: ndata.sensitive,
                published: Some(ndata.published.format("%A, %d %B, %Y").to_string()),
                is_local: data.domain() == domain,
            };
            Ok(Ok(novel))
        }
        Err(NovelError::PermanentRedirect(loc)) => {
            leptos_axum::redirect(&loc);
            Ok(Err("Redirect".to_string()))
        }
        Err(NovelError::InternalServerError(e)) => Err(ServerFnError::new(e)),
        _ => Ok(Err("NotFound".to_string())),
    }
}

#[server]
async fn edit_novel(
    novel: String,
    title: Option<String>,
    summary: Option<String>,
    tags: Option<String>,
    genre: Option<String>,
    lang: Option<String>,
    sensitive: Option<bool>,
) -> Result<NovelUpdate, ServerFnError> {
    use activitypub_federation::config::Data;
    use leptos_axum::extract;
    use tower_sessions::Session;
    use wordforge_api::{
        api::novel::{edit_novel, EditNovel},
        enums::Genres,
        DataHandle,
    };

    let tags = Some(tags.unwrap_or_default());
    let sensitive = Some(sensitive.unwrap_or(false));

    let data: Data<DataHandle> = extract().await?;
    let session: Session = extract().await?;

    let user = session
        .get::<String>("id")
        .await?
        .ok_or_else(|| ServerFnError::new("Not signed in"))?;

    let info = EditNovel {
        title,
        summary,
        tags,
        genre: genre.and_then(|g| Genres::try_from(g.as_str()).ok()),
        lang: lang.and_then(|l| Language::from_name(&l)),
        sensitive,
    };

    let res = edit_novel(&data, user.parse()?, novel.parse()?, info).await?;
    Ok(NovelUpdate {
        name: res.title,
        summary: res.summary,
        tags: res.tags,
        genre: res.genre,
        language: Language::from_639_1(&res.language).unwrap(),
        sensitive: res.sensitive,
    })
}

#[server]
async fn delete_novel(apub_id: String) -> Result<(), ServerFnError> {
    use activitypub_federation::config::Data;
    use itertools::Itertools;
    use leptos_axum::extract;
    use tower_sessions::Session;
    use wordforge_api::{api::novel, DataHandle};

    let data: Data<DataHandle> = extract().await?;
    let session: Session = extract().await?;

    let user_id = session
        .get::<String>("id")
        .await?
        .ok_or_else(|| ServerFnError::new("Failed to validate cookie"))?;

    match novel::delete_novel(&data, apub_id.parse()?, user_id.parse()?).await {
        Ok((_, chapter_errors)) => {
            if !chapter_errors.is_empty() {
                log::warn!(
                    "{}",
                    chapter_errors.into_iter().map(|e| e.to_string()).join("\n")
                );
            }
            Ok(())
        }
        Err((e, chapter_errors)) => Err(ServerFnError::new({
            let e = e.to_string();
            let mut chapter_errors = chapter_errors
                .into_iter()
                .map(|e| e.to_string())
                .collect_vec();
            chapter_errors.push(e.to_string());
            chapter_errors.join("\n")
        })),
    }
}

#[server]
async fn get_usernames(authors: Option<Vec<AuthorItem>>) -> Result<Vec<AuthorItem>, ServerFnError> {
    use activitypub_federation::{config::Data, fetch::object_id::ObjectId};
    use itertools::Itertools;
    use leptos_axum::extract;
    use rayon::prelude::*;
    use wordforge_api::{objects::person::User, DataHandle};

    let authors = if let Some(authors) = authors {
        authors
    } else {
        return Ok(vec![]);
    };

    let data: Data<DataHandle> = extract().await?;

    let urls_ref = authors
        .into_par_iter()
        .filter_map(|author| {
            ObjectId::<User>::parse(&author.href)
                .map(|u| (u, author.role))
                .ok()
        })
        .collect::<Vec<_>>();

    let mut urls = Vec::new();
    for (apub_id, role) in urls_ref.into_iter() {
        urls.push((apub_id.clone(), apub_id.dereference(&data).await, role));
    }

    let authors = urls
        .into_par_iter()
        .map(|(apub_id, user, role)| {
            let domain = format!(
                "{}{}",
                apub_id.inner().host_str().unwrap(),
                apub_id
                    .inner()
                    .port()
                    .map(|p| format!(":{}", p))
                    .unwrap_or_default()
            );
            let (href, name) = match user {
                Ok(user) => (
                    format!(
                        "/user/{}{}",
                        user.preferred_username,
                        domain
                            .eq(data.domain())
                            .then_some(String::new())
                            .unwrap_or(format!("@{}", domain))
                    ),
                    Ok(user.name.unwrap_or(user.preferred_username)),
                ),
                Err(e) => (
                    apub_id.inner().to_string(),
                    Err(format!(
                        "{}: {}",
                        apub_id.inner(),
                        e.chain().map(|e| e.to_string()).join("\n")
                    )),
                ),
            };
            AuthorItem { href, role, name }
        })
        .collect();

    Ok(authors)
}
