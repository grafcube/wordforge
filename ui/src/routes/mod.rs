use leptos::*;
use serde::{Deserialize, Serialize};

#[allow(clippy::too_many_arguments)]
pub mod auth;
pub mod chapter;
#[allow(clippy::too_many_arguments)]
pub mod novel;
pub mod reader;
pub mod settings;
pub mod user;
pub mod writer;

#[derive(Clone, Copy, Debug, Default, Serialize, Deserialize)]
pub struct InstanceInfo {
    pub is_open: bool,
    pub email_required: bool,
}

#[server]
pub async fn get_instance_info() -> Result<InstanceInfo, ServerFnError> {
    use wordforge_api::nodeinfo::InstanceState;

    let nodeinfo = expect_context::<InstanceState>();
    let is_open = nodeinfo.read().unwrap().open_registrations;
    let email_required = nodeinfo.read().unwrap().email_required_for_signup;

    Ok(InstanceInfo {
        is_open,
        email_required,
    })
}
