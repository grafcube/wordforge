use super::reader::{Chapter, ChapterEdit};
use crate::{components::textarea_resize, fallback::NotFoundPage, routes::reader::get_chapter};
use leptos::{html::*, logging::error, *};
use leptos_icons::*;
use leptos_meta::*;
use leptos_router::*;

#[component]
pub fn WriterView() -> impl IntoView {
    let params = use_params_map();

    let (chapter_id, set_chapter_id) = create_query_signal::<String>("id");
    let chapter_id = chapter_id();
    set_chapter_id(None);

    let Some(novel_id) = params.with(|p| p.get("uuid").cloned()) else {
        return view! { <NotFoundPage /> }.into_view();
    };
    let novel_id = StoredValue::new(novel_id);

    let Some(seq) = params.with(|p| p.get("seq").cloned()) else {
        return view! { <NotFoundPage /> }.into_view();
    };

    let Ok(seq) = seq.parse() else {
        return view! { <NotFoundPage /> }.into_view();
    };

    let chapter = Resource::new(
        move || (chapter_id.clone(), novel_id, seq),
        move |(chapter_id, id, seq)| get_chapter(chapter_id, id(), seq),
    );

    view! {
        <Title text="Editor" />
        <div class="mx-auto w-full px-4">
            <Suspense fallback=move || {
                view! { <span class="loading loading-spinner loading-lg m-auto"></span> }
            }>
                <ErrorBoundary fallback=move |e| {
                    let err = e()
                        .into_iter()
                        .map(|(_, e)| e.to_string())
                        .collect::<Vec<_>>()
                        .join("\n");
                    error!("editor: {}", err);
                    view! { <span class="px-4 py-2 my-2">"Something went wrong"</span> }
                }>
                    {move || {
                        chapter()
                            .map(|c| {
                                c.map(|chapter| match chapter {
                                    None => view! { <NotFoundPage /> },
                                    Some(chapter) => view! { <EditorView chapter /> },
                                })
                            })
                    }}

                </ErrorBoundary>
            </Suspense>
        </div>
    }
    .into_view()
}

#[component]
fn EditorView(chapter: Chapter) -> impl IntoView {
    let chapter = RwSignal::new(chapter);
    let edit_node = create_node_ref::<Dialog>();

    let source_resource = Resource::once(move || get_chapter_source(chapter().apub_id));
    let source_content = move || {
        source_resource()
            .and_then(|res| {
                if let Err(ref err) = res {
                    error!("{}", err);
                };
                res.ok()
            })
            .unwrap_or_default()
    };

    let (unsaved, set_unsaved) = create_signal(true);

    let content_ref = create_node_ref::<Textarea>();
    let preview_res = create_action(move |content: &String| preview_content(content.clone()));
    let preview_value = preview_res.value();
    let preview_content = move || {
        preview_value()
            .and_then(|v| {
                if let Err(ref e) = v {
                    error!("Failed to render preview: {}", e);
                }
                v.ok()
            })
            .unwrap_or_default()
    };

    let notes_res = Resource::once(move || get_notes(chapter().apub_id));
    let notes_bar = move || {
        notes_res().map(|v| match v {
            Err(err) => {
                error!("notes: {}", err);
                view! { <span>"Something went wrong"</span> }.into_view()
            }
            Ok(notes) => notes
                .into_iter()
                .map(|(line, note)| {
                    view! {
                        <div class="card border-solid border-accent border-2 px-1 w-full">
                            <div class="card-body">
                                <h3 class="card-title">"Line " {line}</h3>
                                <p class="text-base" inner_html=note></p>
                            </div>
                        </div>
                    }
                })
                .collect_view(),
        })
    };

    let edit = create_server_action::<UpdateContent>();
    let response = edit.value();
    let err = move || {
        response().and_then(|res| match res {
            Ok((content, updated)) => {
                update!(|chapter| {
                    chapter.content = content;
                    chapter.updated = updated;
                });
                set_unsaved(false);
                preview_value.set(Some(Ok(chapter().content)));
                notes_res.refetch();
                None
            }
            Err(err) => Some(err.to_string()),
        })
    };

    view! {
        <Title text=move || chapter().title />
        <div class="drawer drawer-end">
            <input id="note-drawer" type="checkbox" class="drawer-toggle" />
            <div
                class="drawer-content tooltip tooltip-left fixed bottom-20 md:bottom-auto md:top-20 right-0"
                data-tip="Notes"
            >
                <label for="note-drawer" class="drawer-button btn btn-primary rounded-r-none">
                    <Icon icon=icondata::BiNoteRegular class="w-8 h-8 m-auto" />
                </label>
            </div>
            <div class="drawer-side">
                <label for="note-drawer" aria-label="close sidebar" class="drawer-overlay"></label>
                <div class="flex flex-col gap-1 menu p-4 z-40 min-h-full bg-base-200 text-base-content">
                    {notes_bar}
                </div>
            </div>
        </div>
        <div class="max-w-[65ch] mx-auto">
            <A
                class="flex flex-row w-fit max-w-4xl gap-1 my-2 font-bold text-md link link-accent link-hover"
                href=move || {
                    format!(
                        "/chapter/{}/{}/{}/{}",
                        chapter().novel_id,
                        chapter().sequence,
                        chapter().npreview,
                        chapter().cpreview,
                    )
                }
            >

                <Icon icon=icondata::CgArrowLeft class="w-6 h-6 my-auto" />
                <span class="my-auto w-full overflow-hidden whitespace-nowrap text-ellipsis">
                    "Return to chapter"
                </span>
            </A>
            <article class="prose prose-table:table prose-table:table-zebra prose-thead:text-base prose-code:bg-neutral prose-code:rounded-md w-full text-justify">
                <div class="flex flex-row gap-1">
                    <span class="mb-auto mr-2 mt-1 text-2xl italic opacity-80">
                        {move || chapter().sequence}
                    </span>
                    <span
                        class:hidden=move || !chapter().sensitive
                        class="mb-auto mt-3 badge badge-error"
                    >
                        "CW"
                    </span>
                    <h1 class="px-1">{move || chapter().title}</h1>
                    <button
                        class="p-1 ml-auto mb-auto"
                        class:hidden=move || !chapter().editable
                        on:click=move |_| {
                            if let Some(edit) = edit_node() {
                                edit.show_modal().unwrap();
                            }
                        }
                    >

                        <Icon icon=icondata::BiPencilRegular class="w-8 h-8 my-auto" />
                    </button>
                    <ChapterEdit chapter node_ref=edit_node />
                </div>
                <div class="bg-secondary text-secondary-content mx-auto rounded-xl text-base overflow-auto w-10/12 my-2 px-4 py-2">
                    <div>{move || chapter().summary}</div>
                    <span class="italic text-sm">
                        "Published " {move || chapter().published}
                        {move || {
                            chapter().updated.map(|updated| format!(" (updated on {updated})"))
                        }}

                    </span>
                </div>
            </article>
            <ActionForm class="w-full py-4" action=edit>
                <textarea
                    node_ref=content_ref
                    class="textarea z-0 textarea-primary text-base bg-base-300 w-full"
                    name="content"
                    autofocus=true
                    on:input=move |ev| {
                        textarea_resize(ev);
                        set_unsaved(true);
                    }

                    on:paste=move |ev| {
                        textarea_resize(ev);
                        set_unsaved(true);
                    }
                >

                    {source_content}
                </textarea>
                <input type="hidden" name="chapter_id" value=move || chapter().apub_id />
                <div class="relative flex flex-row gap-2 py-2">
                    <button class="btn btn-primary" type="submit">
                        "Save"
                    </button>
                    <span class="my-auto" class:hidden=unsaved>
                        "Saved!"
                    </span>
                </div>
            </ActionForm>
            <div
                class="card mx-1 my-4 bg-error text-error-content"
                class:hidden=move || err().is_none()
            >
                <div class="card-body">{err}</div>
            </div>
            <button
                class="btn btn-outline btn-secondary"
                on:click=move |_| {
                    if let Some(content_ref) = content_ref() {
                        preview_res.dispatch(content_ref.value());
                    }
                }
            >

                "Preview"
            </button>
            <article class="prose prose-table:table prose-table:table-zebra prose-thead:text-base prose-code:bg-neutral prose-code:rounded-md w-full text-justify">
                <h2 class="px-1">"Preview"</h2>
                <div class="w-full text-neutral-content" inner_html=preview_content></div>
            </article>
        </div>
    }
}

#[server]
async fn get_chapter_source(chapter_id: String) -> Result<String, ServerFnError> {
    use activitypub_federation::config::Data;
    use leptos_axum::extract;
    use tower_sessions::Session;
    use wordforge_api::{
        api::chapter::{resolve_source_md, ChapterError},
        DataHandle,
    };

    let data: Data<DataHandle> = extract().await?;
    let session: Session = extract().await?;

    let apub_id = chapter_id.parse().map_err(ServerFnError::new)?;

    let author = session
        .get::<String>("id")
        .await?
        .ok_or_else(|| ServerFnError::new("Not signed in"))?
        .parse()
        .map_err(ServerFnError::new)?;

    match resolve_source_md(&data, apub_id, author).await {
        Ok(source) => Ok(source),
        Err(ChapterError::NotFound) => Err(ServerFnError::new("Not found")),
        Err(ChapterError::Unauthorized) => Err(ServerFnError::new("Unauthorized")),
        Err(ChapterError::InternalError(e)) => Err(ServerFnError::new(e)),
    }
}

#[server]
async fn update_content(
    chapter_id: String,
    content: String,
) -> Result<(String, Option<String>), ServerFnError> {
    use activitypub_federation::config::Data;
    use leptos_axum::extract;
    use tower_sessions::Session;
    use wordforge_api::{
        api::chapter::{edit_chapter, ChapterEdit},
        DataHandle,
    };

    let data: Data<DataHandle> = extract().await?;
    let session: Session = extract().await?;

    let author = session
        .get::<String>("id")
        .await?
        .ok_or_else(|| ServerFnError::new("Not signed in"))?;

    let info = ChapterEdit {
        content: Some(content),
        ..Default::default()
    };

    let res = edit_chapter(&data, author.parse()?, chapter_id.parse()?, info).await?;
    Ok((
        res.content.unwrap(),
        res.updated.map(|t| t.format("%A, %d %B, %Y").to_string()),
    ))
}

#[server]
async fn preview_content(content: String) -> Result<String, ServerFnError> {
    use wordforge_api::api::chapter::preview_html;
    let res = preview_html(content).await?;
    Ok(res)
}

#[server]
async fn get_notes(chapter_id: String) -> Result<Vec<(u32, String)>, ServerFnError> {
    use activitypub_federation::config::Data;
    use leptos_axum::extract;
    use tower_sessions::Session;
    use wordforge_api::{api::chapter::get_notes, DataHandle};

    let data: Data<DataHandle> = extract().await?;
    let session: Session = extract().await?;

    let author = session
        .get::<String>("id")
        .await?
        .ok_or_else(|| ServerFnError::new("Not signed in"))?;

    let notes = get_notes(&data, &chapter_id, &author).await?;

    Ok(notes)
}
