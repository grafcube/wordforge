use crate::{
    app::ValidationResult,
    i18n::use_i18n,
    path::AuthQueries,
    routes::{get_instance_info, InstanceInfo},
};
use leptos::{logging::warn, *};
use leptos_i18n::t;
use leptos_meta::*;
use leptos_router::*;
use std::ops::Not;
use zxcvbn::zxcvbn;

#[component]
pub fn SetupAdmin(admin_exists_res: Resource<(), Result<bool, ServerFnError>>) -> impl IntoView {
    let valid = use_context::<Resource<(), Result<ValidationResult, ServerFnError>>>().unwrap();
    let (errormsg, set_errormsg) = create_signal(String::new());
    let setup_complete = RwSignal::new(Some(false));
    let instance_info = Callback::new(|()| {
        Some(InstanceInfo {
            is_open: true,
            email_required: false,
        })
    });

    view! {
        <Title text="First time setup" />
        <div class="p-4 w-fit mx-auto">
            {move || {
                if setup_complete().unwrap() {
                    admin_exists_res.refetch();
                    valid.refetch();
                }
            }}
            <article class="prose prose-table:table prose-table:table-zebra prose-thead:text-base prose-code:bg-neutral prose-code:rounded-md w-full text-justify">
                <h1>"Welcome!"</h1>
                <p>"To get started, you need to set up an admin account for your instance."</p>
                <p>
                    "Check the server logs and find the admin token. It should look something like"
                    <code>"ADMIN TOKEN: <token here>"</code>
                    ". You need to enter that in the token field below to verify that you can access the server."
                </p>
                <p>"Now go forth and create worlds!"</p>
            </article>
            <Register redirect_to="/".to_string() set_errormsg setup_complete instance_info />
            <div class="flex mx-auto text-2xl m-4 justify-center text-center">
                <p class="text-error">{errormsg}</p>
            </div>
        </div>
    }
}

#[component]
pub(crate) fn Auth() -> impl IntoView {
    let query = use_query::<AuthQueries>();
    let path = move || {
        with!(|query| query.clone().map(|p| p
            .redirect_to
            .is_empty()
            .then(|| "/".to_string())
            .unwrap_or(p.redirect_to)))
        .unwrap_or_else(|e| {
            warn!("AuthQuery: {e}");
            "/".to_string()
        })
    };
    let (errormsg, set_errormsg) = create_signal(String::new());

    let instance_info_res = Resource::once(get_instance_info);
    let instance_info = Callback::new(move |()| {
        instance_info_res().and_then(|v| {
            if let Err(ref e) = v {
                logging::error!("Failed to get instance info: {}", e);
            }
            v.ok()
        })
    });

    view! {
        <Title text="Sign in or create an account" />
        <div class="mx-auto w-full">
            <div class="flex flex-col md:flex-row w-full mx-auto max-w-3xl m-4 justify-center">
                <Login redirect_to=path() set_errormsg />
                <div class="divider divider-vertical md:divider-horizontal"></div>
                <Register redirect_to=path() set_errormsg instance_info />
            </div>
            <div class="flex mx-auto text-2xl m-4 justify-center text-center">
                <p class="text-error">{errormsg}</p>
            </div>
        </div>
    }
}

#[component]
fn Login(redirect_to: String, set_errormsg: WriteSignal<String>) -> impl IntoView {
    let i18n = use_i18n();
    let valid = expect_context::<Resource<(), Result<ValidationResult, ServerFnError>>>();
    let login = create_server_action::<ServerLogin>();
    let response = login.value();
    let err = move || {
        response.get().map(|v| match v {
            Ok(Err(v)) => set_errormsg(v),
            Err(e) => set_errormsg(e.to_string()),
            Ok(Ok(_)) => {
                valid.refetch();
                let redirect_to = if redirect_to == "/auth" {
                    "/"
                } else {
                    &redirect_to
                };
                use_navigate()(redirect_to, NavigateOptions::default());
            }
        })
    };

    view! {
        <ActionForm action=login class="form-control space-y-4 p-4 w-full">
            <h2 class="text-2xl w-full text-center">"Login"</h2>
            <div class="relative">
                <input
                    name="login_id"
                    id="login_id"
                    placeholder=" "
                    class="floating-input input-bordered focus:input-primary rounded-xl text-xl w-full peer"
                    required
                />
                <label for="login_id" class="floating-label">
                    {t!(i18n, common.username_or_email)}
                </label>
            </div>
            <div class="relative">
                <input
                    name="password"
                    id="password"
                    type="password"
                    placeholder=" "
                    minlength=8
                    class="floating-input input-bordered focus:input-primary rounded-xl text-xl w-full peer"
                    required
                />
                <label for="password" class="floating-label">
                    {t!(i18n, common.password)}
                </label>
            </div>
            <div class="relative">
                <label class="flex flex-row gap-2 justify-start cursor-pointer label">
                    <input
                        type="checkbox"
                        name="remember"
                        value="true"
                        class="toggle toggle-primary"
                    />
                    <span class="label-text text-xl">{t!(i18n, common.stay_signed_in)}</span>
                </label>
            </div>
            <div class="relative w-full flex justify-around">
                <input type="hidden" name="client_app" value="Web" />
                <input type="submit" class="btn btn-primary" value=t!(i18n, common.login) />
            </div>
            {err}
        </ActionForm>
    }
}

#[server]
async fn server_login(
    login_id: String,
    password: String,
    client_app: String,
    client_website: Option<String>,
    remember: Option<bool>,
) -> Result<Result<String, String>, ServerFnError> {
    use activitypub_federation::config::Data;
    use leptos_axum::extract;
    use tower_sessions::Session;
    use wordforge_api::{
        account::{self, FormAuthError, LoginError},
        DataHandle,
    };

    let remember = remember.unwrap_or(false);

    let data: Data<DataHandle> = extract().await?;
    let session: Session = extract().await?;

    match account::login(
        &data.pool,
        session,
        login_id
            .try_into()
            .map_err(|_| ServerFnError::new("Invalid username or password"))?,
        password,
        client_app,
        client_website,
        remember,
    )
    .await
    {
        Ok(apub_id) => Ok(Ok(apub_id)),
        Err(LoginError::InternalServerError(e)) => Err(ServerFnError::new(e)),
        Err(LoginError::BadRequest(e)) => Ok(Err(e)),
        Err(LoginError::Unauthorized(FormAuthError::LoginId)) => {
            Ok(Err("Username or email not found".to_string()))
        }
        Err(LoginError::Unauthorized(FormAuthError::Password)) => {
            Ok(Err("Wrong password".to_string()))
        }
        Err(LoginError::Unauthorized(_)) => unreachable!(),
    }
}

#[component]
fn Register(
    redirect_to: String,
    set_errormsg: WriteSignal<String>,
    #[prop(optional)] setup_complete: RwSignal<Option<bool>>,
    instance_info: Callback<(), Option<InstanceInfo>>,
) -> impl IntoView {
    let i18n = use_i18n();
    let valid = use_context::<Resource<(), Result<ValidationResult, ServerFnError>>>().unwrap();
    let register = create_server_action::<ServerRegister>();
    let response = register.value();
    let err = move || {
        response.get().map(|v| match v {
            Ok(Err(v)) => set_errormsg(v),
            Err(e) => set_errormsg(e.to_string()),
            Ok(Ok(_)) => {
                if setup_complete().is_none() {
                    valid.refetch();
                    let redirect_to = if redirect_to == "/auth" {
                        "/"
                    } else {
                        &redirect_to
                    };
                    use_navigate()(redirect_to, NavigateOptions::default());
                } else {
                    setup_complete.set(Some(true));
                }
            }
        })
    };

    let password_input = NodeRef::new();
    let (is_pass_matching, set_pass_matching) = create_signal(true);

    let (score, set_score) = create_signal(0u8);
    let feedback = RwSignal::new(None);

    let display_name = create_node_ref::<html::Input>();
    let username = create_node_ref::<html::Input>();
    let email = create_node_ref::<html::Input>();

    view! {
        <ActionForm action=register class="form-control space-y-4 p-4 w-full">
            <h2 class="text-2xl w-full text-center">"Register"</h2>
            <div class="relative">
                <input
                    name="display_name"
                    id="display_name"
                    type="text"
                    placeholder=" "
                    node_ref=display_name
                    class="floating-input input-bordered focus:input-primary rounded-xl text-xl w-full peer"
                />
                <label for="display_name" class="floating-label">
                    {t!(i18n, common.display_name)}
                    " ("
                    {t!(i18n, common.optional)}
                    ")"
                </label>
            </div>
            <div class="relative">
                <input
                    name="username"
                    id="username"
                    type="text"
                    placeholder=" "
                    node_ref=username
                    class="floating-input input-bordered focus:input-primary rounded-xl text-xl w-full peer"
                    required
                />
                <label for="username" class="floating-label">
                    {t!(i18n, common.username)}
                </label>
            </div>
            <div class="relative">
                <input
                    name="email"
                    id="email"
                    type="email"
                    placeholder=" "
                    node_ref=email
                    class="floating-input input-bordered focus:input-primary rounded-xl text-xl w-full peer"
                    required=move || instance_info(()).map(|v| v.email_required).unwrap_or(false)
                />
                <label for="email" class="floating-label">
                    {t!(i18n, common.email)}
                </label>
            </div>
            {move || {
                setup_complete()
                    .map(|_| {
                        view! {
                            <div class="relative flex flex-col gap-3">
                                <input
                                    name="admin_token"
                                    id="admin_token"
                                    type="password"
                                    placeholder=" "
                                    autocomplete="off"
                                    class="floating-input input-bordered focus:input-primary rounded-xl text-xl w-full peer"
                                    required
                                />
                                <label for="admin_token" class="floating-label">
                                    {t!(i18n, common.admin_token)}
                                </label>
                            </div>
                        }
                    })
            }}

            {move || {
                instance_info(())
                    .and_then(|info| {
                        info.is_open
                            .not()
                            .then(|| {
                                view! {
                                    <div class="relative flex flex-col gap-3">
                                        <input
                                            name="reg_token"
                                            id="reg_token"
                                            type="password"
                                            placeholder=" "
                                            autocomplete="off"
                                            class="floating-input input-bordered focus:input-primary rounded-xl text-xl w-full peer"
                                            required
                                        />
                                        <label for="admin_token" class="floating-label">
                                            {t!(i18n, common.registration_token)}
                                        </label>
                                    </div>
                                }
                            })
                    })
            }}

            <div class="relative flex flex-col gap-3">
                <input
                    name="password"
                    id="password"
                    type="password"
                    placeholder=" "
                    node_ref=password_input
                    minlength=8
                    class="floating-input input-bordered focus:input-primary rounded-xl text-xl w-full peer"
                    required
                    on:input=move |ev| {
                        let display_name = display_name().map(|v| v.value()).unwrap_or_default();
                        let username = username().map(|v| v.value()).unwrap_or_default();
                        let email = email().map(|v| v.value()).unwrap_or_default();
                        let password = event_target_value(&ev);
                        let entropy = zxcvbn(&password, &[&display_name, &username, &email]);
                        set_score(entropy.score() as u8 * 25);
                        let feedback_ref = entropy.feedback().cloned();
                        feedback.set(feedback_ref);
                    }
                />

                <label for="password" class="floating-label">
                    {t!(i18n, common.password)}
                </label>
            </div>
            <div class="relative flex flex-col gap-3">
                <input
                    name="repassword"
                    id="repassword"
                    type="password"
                    placeholder=" "
                    minlength=8
                    class="floating-input input-bordered focus:input-primary rounded-xl text-xl w-full peer"
                    required
                    on:input=move |ev| {
                        let password = password_input().map(|v| v.value()).unwrap_or_default();
                        let repassword = event_target_value(&ev);
                        if repassword != password && !repassword.is_empty() {
                            set_pass_matching(false);
                        } else {
                            set_pass_matching(true);
                        }
                    }
                />

                <label for="repassword" class="floating-label">
                    {t!(i18n, common.confirm_password)}
                </label>
            </div>
            <div class="relative flex flex-col gap-3">
                <div class="flex flex-col px-1 gap-3 text-left text-base">
                    <progress
                        max=100
                        value=score
                        class=move || {
                            format!(
                                "progress w-full {}",
                                match score() {
                                    0..=25 => "progress-error",
                                    26..=50 => "progress-warning",
                                    51..=75 => "progress-info",
                                    _ => "progress-success",
                                },
                            )
                        }
                    ></progress>
                    <div
                        class="card rounded-lg w-full p-2 bg-error text-error-content"
                        class:hidden=is_pass_matching
                    >
                        {t!(i18n, common.passwords_not_matching)}
                    </div>
                    // TODO: Localisation for zxcvbn
                    {move || {
                        feedback()
                            .map(|f| {
                                view! {
                                    {f
                                        .warning()
                                        .map(|w| {
                                            view! {
                                                <div class="card rounded-lg w-full p-2 bg-warning text-warning-content">
                                                    {w.to_string()}
                                                </div>
                                            }
                                        })}

                                    <div class="card rounded-lg w-full p-2 bg-info text-info-content">
                                        <ul
                                            class="ml-4 list-disc"
                                            class:hidden={
                                                let suggestions = f.suggestions().to_vec();
                                                move || suggestions.is_empty()
                                            }
                                        >

                                            {f
                                                .suggestions()
                                                .iter()
                                                .map(|s| view! { <li>{s.to_string()}</li> })
                                                .collect_view()}

                                        </ul>
                                    </div>
                                }
                            })
                    }}

                </div>
            </div>
            <div class="relative">
                <label class="flex flex-row gap-2 justify-start cursor-pointer label">
                    <input
                        type="checkbox"
                        name="remember"
                        value="true"
                        class="toggle toggle-primary"
                    />
                    <span class="label-text text-xl">{t!(i18n, common.stay_signed_in)}</span>
                </label>
            </div>
            <input type="hidden" name="client_app" value="Web" />
            <div class="relative w-full flex justify-around">
                <input
                    type="submit"
                    class="btn btn-primary"
                    disabled=move || !is_pass_matching()
                    value=t!(i18n, common.sign_up)
                />
            </div>
            <span
                class:hidden=move || !register.pending()()
                class="loading loading-spinner loading-lg mx-auto"
            ></span>
            {err}
        </ActionForm>
    }
}

#[server]
async fn server_register(
    display_name: Option<String>,
    username: String,
    email: Option<String>,
    password: String,
    client_app: String,
    client_website: Option<String>,
    admin_token: Option<String>,
    reg_token: Option<String>,
    remember: Option<bool>,
) -> Result<Result<String, String>, ServerFnError> {
    use activitypub_federation::config::Data;
    use leptos_axum::extract;
    use sqlx::query;
    use wordforge_api::{
        account::{self, FormAuthError, RegistrationError},
        enums::InstanceUserRoles,
        nodeinfo::InstanceState,
        tokens::is_reg_token_valid,
        util::ADMIN_TOKEN_PATH,
        DataHandle,
    };

    let nodeinfo = expect_context::<InstanceState>();

    let email_required = nodeinfo.read().unwrap().email_required_for_signup;
    if email_required && email.is_none() {
        return Ok(Err("Email is required on this instance".to_string()));
    }

    if let Some(admin_token) = admin_token.clone() {
        let token =
            std::fs::read_to_string(ADMIN_TOKEN_PATH.as_ref()).map_err(ServerFnError::new)?;
        if token != admin_token {
            return Ok(Err("Invalid token".to_string()));
        }
    }

    let data: Data<DataHandle> = extract().await?;

    let is_open = nodeinfo.read().unwrap().open_registrations;
    if let Some(reg_token) = reg_token {
        if !is_open
            && !is_reg_token_valid(&reg_token, &data.pool)
                .await
                .map_err(ServerFnError::new)?
        {
            return Ok(Err("Invalid token".to_string()));
        }
    }

    match account::register(
        &data,
        display_name,
        username.clone(),
        email,
        password.clone(),
    )
    .await
    {
        Ok(apub_id) => {
            if let Err(err) = std::fs::remove_file(ADMIN_TOKEN_PATH.as_ref()) {
                log::warn!("Failed to delete token file: {}", err);
            }
            if admin_token.is_some() {
                query!(
                    r#"
                    INSERT INTO
                        admins (apub_id, role)
                    VALUES
                        ($1, $2)
                "#,
                    apub_id,
                    InstanceUserRoles::Admin.to_string(),
                )
                .execute(data.pool.as_ref())
                .await
                .map_err(ServerFnError::new)?;
            }
            server_login(username, password, client_app, client_website, remember).await
        }
        Err(RegistrationError::BadRequest(e)) => Ok(Err(e)),
        Err(RegistrationError::Conflict(FormAuthError::Email)) => {
            Ok(Err("Email in use".to_string()))
        }
        Err(RegistrationError::Conflict(FormAuthError::Username)) => {
            Ok(Err("Username is taken".to_string()))
        }
        Err(RegistrationError::InternalServerError(e)) => Err(ServerFnError::new(e)),
        Err(RegistrationError::Conflict(_)) => unreachable!(),
    }
}
