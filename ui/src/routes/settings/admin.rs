use super::TokenResponse;
use crate::{components::DialogForm, routes::get_instance_info};
use chrono::{DateTime, Utc};
use leptos::{html::*, *};
use leptos_icons::*;
use leptos_meta::*;
use leptos_router::*;
use serde::{Deserialize, Serialize};
use std::num::NonZeroU32;

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct RegToken {
    pub token: String,
    pub used: i32,
    pub max_uses: Option<i32>,
    pub created: DateTime<Utc>,
    pub expiry: Option<DateTime<Utc>>,
}

#[component]
pub fn Admin() -> impl IntoView {
    let instance_info_res = Resource::once(get_instance_info);
    let instance_info = move || {
        instance_info_res().and_then(|v| {
            if let Err(ref e) = v {
                logging::error!("Failed to get instance info: {}", e);
            }
            v.ok()
        })
    };

    let set_state = create_server_action::<SetRegistrationState>();

    view! {
        <Title text="Instance Settings" />
        <h1 class="text-4xl pb-2">"Instance Settings"</h1>
        <h2 class="text-3xl">"Manage registrations"</h2>
        <ActionForm
            action=set_state
            class="form-control flex flex-row place-items-start justify-start gap-2 p-4 w-full"
        >
            <span class="label-text text-xl w-full mr-auto my-auto">
                "Allow anyone to register"
            </span>
            <input
                type="checkbox"
                name="state"
                value="true"
                checked=move || instance_info().map(|v| v.is_open).unwrap_or(false)
                disabled=move || instance_info().is_none()
                class="toggle toggle-accent my-auto"
            />
            <div class="relative w-full flex justify-around">
                <input type="submit" class="btn btn-primary" value="Apply" />
            </div>
            <span
                class:hidden=move || !set_state.pending()()
                class="loading loading-spinner loading-lg mx-auto"
            ></span>
        </ActionForm>
        <Show when=move || instance_info().map(|v| !v.is_open).unwrap_or(false)>
            <RegistrationTokens />
        </Show>
    }
}

#[component]
fn RegistrationTokens() -> impl IntoView {
    let gen_ref = create_node_ref::<Dialog>();
    let list_ref = create_node_ref::<Dialog>();
    let token_res = Resource::once(get_reg_tokens);

    let (token, set_token) = create_signal(None);
    let (unlimited_uses, set_unlimited_uses) = create_signal(false);
    let (date_disabled, set_date_disabled) = create_signal(false);
    let generate = create_server_action::<NewToken>();
    let response = generate.value();
    let err = move || {
        response().and_then(|response| match response {
            Ok(res) => {
                if let Some(dialog) = gen_ref() {
                    dialog.close();
                }
                set_token(Some(res));
                token_res.refetch();
                None
            }
            Err(e) => Some(e.to_string()),
        })
    };

    view! {
        <div class="w-full p-1">
            {move || token().map(|t| view! { <TokenResponse token=t.token hide_message=true /> })}
        </div>
        <div class="flex flex-row gap-2 justify-start">
            <button
                class="btn btn-primary"
                on:click=move |_| {
                    if let Some(gen_ref) = gen_ref() {
                        if let Err(e) = gen_ref.show_modal() {
                            logging::error!("Failed to open token list: {:?}", e);
                        }
                    }
                    token_res.refetch();
                }
            >

                "Generate token"
            </button>
            <DialogForm
                class="flex flex-col justify-start space-y-4 p-4 w-full"
                title="New Token"
                node_ref=gen_ref
                action=generate
                pre_form=move || {
                    view! {
                        <div
                            class="card mx-1 my-4 bg-error text-error-content"
                            class:hidden=move || err().is_none()
                        >
                            <div class="card-body">{err}</div>
                        </div>
                    }
                }
            >

                <div class="flex flex-row gap-1 relative">
                    <input
                        type="checkbox"
                        id="uses"
                        class="checkbox checkbox-accent my-auto w-6 h-6 rounded-md"
                        on:change=move |ev| {
                            let target = event_target::<web_sys::HtmlInputElement>(&ev);
                            set_unlimited_uses(target.checked());
                        }
                    />

                    <label
                        class="px-1"
                        for="uses"
                        class=move || unlimited_uses().then_some("text-error").unwrap_or_default()
                    >
                        "Unlimited uses"
                    </label>
                </div>
                <div class="block relative" class:hidden=unlimited_uses>
                    <input
                        type="number"
                        placeholder=" "
                        class="floating-input input-bordered focus:input-primary rounded-md peer"
                        id="max_uses"
                        name="max_uses"
                        min=1
                    />
                    <label for="label" class="floating-label">
                        "Maximum uses"
                    </label>
                </div>
                <div class="flex flex-row gap-1 relative">
                    <input
                        type="checkbox"
                        id="expires"
                        class="checkbox checkbox-accent my-auto w-6 h-6 rounded-md"
                        on:change=move |ev| {
                            let target = event_target::<web_sys::HtmlInputElement>(&ev);
                            set_date_disabled(target.checked());
                        }
                    />

                    <label
                        class="px-1"
                        for="expires"
                        class=move || date_disabled().then_some("text-error").unwrap_or_default()
                    >
                        "No expiry"
                    </label>
                </div>
                <div class="block relative" class:hidden=date_disabled>
                    <label class="pr-1" for="expiry">
                        "Expires on "
                    </label>
                    <input
                        type="date"
                        name="expiry"
                        id="expiry"
                        disabled=date_disabled
                        min=(chrono::Utc::now() + chrono::Duration::try_days(1).unwrap())
                            .date_naive()
                            .to_string()
                        max=(chrono::Utc::now() + chrono::Duration::try_days(366).unwrap())
                            .date_naive()
                            .to_string()
                        class="rounded-md input input-bordered focus:input-accent"
                        required
                    />
                </div>
                <input type="submit" class="btn btn-accent mx-auto" value="Generate" />
            </DialogForm>
            <button
                class="btn btn-primary btn-outline"
                on:click=move |_| {
                    if let Some(reg_list) = list_ref() {
                        if let Err(e) = reg_list.show_modal() {
                            logging::error!("Failed to open token list: {:?}", e);
                        }
                    }
                    token_res.refetch();
                }
            >

                "Show tokens"
            </button>
            <dialog class="modal modal-bottom sm:modal-middle" node_ref=list_ref>
                <div class="modal-box overflow-auto max-w-[80rem] w-fit">
                    <form method="dialog" class="flex flex-row w-full justify-between">
                        <h3 class="my-auto text-xl text-neutral-content">"Registration Tokens"</h3>
                        <button class="my-auto p-1 rounded-full transition ease-in-out duration-[400ms] hover:bg-base-300">
                            <Icon icon=icondata::CgClose class="w-8 h-8 my-auto" />
                        </button>
                    </form>
                    <table class="table text-base">
                        <thead class="text-base cursor-default">
                            <tr>
                                <th>"Token"</th>
                                <th>"Uses"</th>
                                <th>"Created"</th>
                                <th>"Expires"</th>
                                <th>"Delete"</th>
                            </tr>
                        </thead>
                        <tbody>
                            <Suspense fallback=move || {
                                view! {
                                    <span class="loading loading-spinner loading-lg m-auto"></span>
                                }
                            }>
                                <ErrorBoundary fallback=move |e| {
                                    view! {
                                        <div class="w-full px-4 py-2 my-1 text-error">
                                            {e()
                                                .into_iter()
                                                .map(|(_, e)| view! { <p>{e.to_string()}</p> })
                                                .collect_view()}
                                        </div>
                                    }
                                }>
                                    {move || {
                                        token_res()
                                            .map(|v| {
                                                v.map(|v| {
                                                    if v.is_empty() {
                                                        view! { <div class="w-full mx-auto my-4">"No tokens"</div> }
                                                            .into_view()
                                                    } else {
                                                        v.into_iter()
                                                            .map(|token| view! { <TokenRow token token_res /> })
                                                            .collect_view()
                                                    }
                                                })
                                            })
                                    }}

                                </ErrorBoundary>
                            </Suspense>
                        </tbody>
                    </table>
                </div>
            </dialog>
        </div>
    }
}

#[component]
fn TokenRow(
    token: RegToken,
    token_res: Resource<(), Result<Vec<RegToken>, ServerFnError>>,
) -> impl IntoView {
    let delete_action = create_action(|token: &String| delete_token(token.clone()));
    let awaiting_delete = RwSignal::<Option<String>>::new(None);
    let response = delete_action.value();
    let err = move || match response() {
        Some(Ok(())) => {
            awaiting_delete.set(None);
            token_res.refetch();
        }
        Some(Err(e)) => logging::error!("Failed to delete token: {}", e),
        None => (),
    };
    let (confirm_hidden, hide_confirm) = create_signal(true);

    view! {
        <tr class:hidden=confirm_hidden>
            <td colspan="100%" class="border-error border-solid border-2">
                <div class="flex flex-row justify-between w-full p-2">
                    <div class="flex flex-row gap-2 w-fit">
                        <Icon icon=icondata::BsArrowDownCircleFill class="w-8 h-8 m-auto" />
                        <span class="my-auto">"Are you sure you want to delete this token?"</span>
                    </div>
                    <div class="flex flex-row w-fit gap-1">
                        <button
                            class="btn btn-error"
                            on:click={
                                let token = token.token.clone();
                                move |_| {
                                    awaiting_delete.set(Some(token.clone()));
                                    delete_action.dispatch(token.to_owned());
                                    hide_confirm(true);
                                }
                            }
                        >

                            <Icon icon=icondata::BsCheckLg class="w-8 h-8 m-auto" />
                        </button>
                        <button
                            class="btn btn-outline btn-error"
                            on:click=move |_| hide_confirm(true)
                        >
                            <Icon icon=icondata::BsXLg class="w-8 h-8 m-auto" />
                        </button>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            {err} <td>
                <button
                    class="flex flex-row p-1 m-auto rounded-r-md btn btn-neutral join-item"
                    on:click={
                        let token = token.token.clone();
                        move |_| {
                            let clipboard = window().navigator().clipboard();
                            let _ = clipboard.write_text(&token);
                        }
                    }
                >

                    <Icon icon=icondata::BiCopyRegular class="py-1 w-6 h-6 my-auto" />
                    <span class="my-auto mr-1">"Copy"</span>
                </button>
            </td> <td>{token.used} {token.max_uses.map(|u| format!("/{}", u))}</td>
            <td>{token.created.format("%c").to_string()}</td>
            <td>
                {token
                    .expiry
                    .map(|d| {
                        view! { <span>{d.format("%c").to_string()}</span> }
                    })
                    .unwrap_or_else(|| {
                        view! { <span class="text-error">"Never"</span> }
                    })}

            </td> <td>
                <button
                    class="btn btn-outline btn-error"
                    on:click=move |_| {
                        if confirm_hidden() || awaiting_delete().is_some() {
                            hide_confirm(false);
                        }
                    }
                >

                    <Show
                        when=move || {
                            awaiting_delete() == Some(token.token.to_owned())
                                && delete_action.pending()()
                        }

                        fallback=move || {
                            view! {
                                <Icon icon=icondata::CgTrash class="stroke-error w-6 h-6 m-auto" />
                            }
                        }
                    >

                        <span class="loading loading-spinner loading-md m-auto"></span>
                    </Show>
                </button>
            </td>
        </tr>
    }
}

#[server]
pub async fn check_is_admin() -> Result<bool, ServerFnError> {
    use activitypub_federation::config::Data;
    use leptos_axum::extract;
    use sqlx::query;
    use tower_sessions::Session;
    use wordforge_api::{enums::InstanceUserRoles, DataHandle};

    let data: Data<DataHandle> = extract().await?;
    let session: Session = extract().await?;

    let owner = session
        .get::<String>("id")
        .await?
        .ok_or_else(|| ServerFnError::new("Unauthorized"))?;

    let is_admin = query!(
        r#"
        SELECT
            apub_id
        FROM
            admins
        WHERE
            role = $1 AND
            apub_id = $2
    "#,
        InstanceUserRoles::Admin.to_string(),
        owner
    )
    .fetch_optional(data.pool.as_ref())
    .await?
    .is_some();

    Ok(is_admin)
}

#[server]
async fn set_registration_state(state: Option<bool>) -> Result<bool, ServerFnError> {
    use wordforge_api::nodeinfo::InstanceState;

    let state = state.unwrap_or(false);

    if !check_is_admin().await? {
        return Err(ServerFnError::new("Unauthorized"));
    }

    let nodeinfo = expect_context::<InstanceState>();
    let mut nodeinfo = nodeinfo.write().unwrap();
    nodeinfo.open_registrations = state;
    nodeinfo.write()?;

    Ok(state)
}

#[server]
async fn get_reg_tokens() -> Result<Vec<RegToken>, ServerFnError> {
    use activitypub_federation::config::Data;
    use leptos_axum::extract;
    use sqlx::query_as;
    use wordforge_api::DataHandle;

    let data: Data<DataHandle> = extract().await?;
    if !check_is_admin().await? {
        return Err(ServerFnError::new("Unauthorized"));
    }

    let tokens = query_as!(RegToken, r"SELECT * FROM reg_tokens")
        .fetch_all(data.pool.as_ref())
        .await?;

    Ok(tokens)
}

#[server]
async fn new_token(
    max_uses: Option<NonZeroU32>,
    expiry: Option<String>,
) -> Result<RegToken, ServerFnError> {
    use activitypub_federation::config::Data;
    use leptos_axum::extract;
    use wordforge_api::{
        tokens::{generate_token, store_reg_token},
        DataHandle,
    };

    let data: Data<DataHandle> = extract().await?;
    if !check_is_admin().await? {
        return Err(ServerFnError::new("Unauthorized"));
    }

    let expiry = if let Some(expiry) = expiry.map(|v| format!("{} 00:00:00 +00:00", v)) {
        Some(DateTime::parse_from_str(&expiry, "%Y-%m-%d %H:%M:%S %z")?.into())
    } else {
        None
    };

    let token = generate_token();
    let created = store_reg_token(&token, max_uses, expiry, &data.pool)
        .await
        .map_err(ServerFnError::new)?;

    Ok(RegToken {
        token,
        used: 0,
        max_uses: max_uses.map(|u| u.get() as i32),
        created,
        expiry,
    })
}

#[server]
async fn delete_token(token: String) -> Result<(), ServerFnError> {
    use activitypub_federation::config::Data;
    use leptos_axum::extract;
    use sqlx::query;
    use wordforge_api::DataHandle;

    let data: Data<DataHandle> = extract().await?;
    if !check_is_admin().await? {
        return Err(ServerFnError::new("Unauthorized"));
    }

    query!(
        r#"
            DELETE FROM
                reg_tokens
            WHERE
                token = $1
        "#,
        token
    )
    .execute(data.pool.as_ref())
    .await?;

    Ok(())
}
