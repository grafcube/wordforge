use super::TokenResponse;
use chrono::{DateTime, Utc};
use leptos::*;
use leptos_icons::*;
use leptos_meta::*;
use leptos_router::*;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct TokenItem {
    pub label: String,
    pub created: DateTime<Utc>,
    pub expiry: Option<DateTime<Utc>>,
    pub permissions: Vec<String>,
}

#[component]
pub fn Developer() -> impl IntoView {
    let gen_token = create_server_action::<CreateToken>();
    let gen_token_response = gen_token.value();

    let tokens = Resource::once(get_tokens);

    let delete_action = Action::new(|label: &String| delete_token(label.clone()));
    let awaiting_delete = RwSignal::<Option<String>>::new(None);

    view! {
        <Title text="Developer Options" />
        <h1 class="text-4xl">"Developer Options"</h1>
        <Suspense>
            <ErrorBoundary fallback=move |e| {
                view! {
                    <div class="w-full px-4 py-2 my-1 rounded-md bg-error text-error-content">
                        {e()
                            .into_iter()
                            .map(|(_, e)| view! { <p>{e.to_string()}</p> })
                            .collect_view()}
                    </div>
                }
            }>
                {move || {
                    gen_token_response
                        .get()
                        .map(|r| {
                            r.map(|token| {
                                tokens.refetch();
                                view! { <TokenResponse token /> }
                            })
                        })
                }}

            </ErrorBoundary>
        </Suspense>
        <TokenGenForm action=gen_token />
        <h2 class="text-2xl">"Tokens"</h2>
        <Suspense>
            <ErrorBoundary fallback=move |e| {
                view! {
                    <div class="w-full px-4 py-2 my-1 rounded-md bg-error text-error-content">
                        {e()
                            .into_iter()
                            .map(|(_, e)| view! { <p>{e.to_string()}</p> })
                            .collect_view()}
                    </div>
                }
            }>
                {move || {
                    delete_action
                        .value()()
                        .map(|v| {
                            awaiting_delete.set(None);
                            v.map(|token| {
                                tokens.refetch();
                                view! {
                                    <div class="flex flex-col w-full p-2 gap-1 rounded-md bg-info text-info-content">
                                        "The token `" {token} "` has been successfully deleted!"
                                    </div>
                                }
                            })
                        })
                }}

            </ErrorBoundary>
        </Suspense>
        <div class="overflow-x-auto">
            <table class="table text-base">
                <thead class="text-base cursor-default">
                    <tr>
                        <th>"Label"</th>
                        <th>"Created"</th>
                        <th>"Expires"</th>
                        <th>"Permissions"</th>
                        <th>"Delete"</th>
                    </tr>
                </thead>
                <tbody>
                    <Suspense>
                        <ErrorBoundary fallback=move |e| {
                            view! {
                                <div class="w-full px-4 py-2 my-1 text-error">
                                    {e()
                                        .into_iter()
                                        .map(|(_, e)| view! { <p>{e.to_string()}</p> })
                                        .collect_view()}
                                </div>
                            }
                        }>
                            {move || {
                                tokens()
                                    .map(move |v| {
                                        v.map(|t| {
                                            t.into_iter()
                                                .map(|token| {
                                                    view! { <TokenRow token awaiting_delete delete_action /> }
                                                })
                                                .collect_view()
                                        })
                                    })
                            }}

                        </ErrorBoundary>
                    </Suspense>
                </tbody>
            </table>
        </div>
    }
}

#[component]
fn TokenGenForm(action: Action<CreateToken, Result<String, ServerFnError>>) -> impl IntoView {
    let (date_disabled, set_date_disabled) = create_signal(false);
    let (permissions, set_permissions) = create_signal::<Vec<String>>(vec![]);

    view! {
        <ActionForm action class="space-y-2 p-4 w-full">
            <div class="relative">
                <input
                    type="text"
                    placeholder=" "
                    class="floating-input input-bordered focus:input-primary rounded-md peer"
                    id="label"
                    name="label"
                    autocomplete="off"
                    required
                />
                <label for="label" class="floating-label">
                    "Label"
                </label>
            </div>
            <div class="flex flex-row gap-1 relative">
                <input
                    type="checkbox"
                    id="expires"
                    class="checkbox checkbox-accent my-auto w-6 h-6 rounded-md"
                    on:change=move |ev| {
                        let target = event_target::<web_sys::HtmlInputElement>(&ev);
                        set_date_disabled(target.checked());
                    }
                />

                <label
                    class="px-1"
                    for="expires"
                    class=move || date_disabled().then_some("text-error").unwrap_or_default()
                >
                    "No expiry (NOT RECOMMENDED)"
                </label>
            </div>
            <div class="block relative" class:hidden=date_disabled>
                <label class="pr-1" for="expiry">
                    "Expires on "
                </label>
                <input
                    type="date"
                    name="expiry"
                    id="expiry"
                    disabled=date_disabled
                    min=(chrono::Utc::now() + chrono::Duration::try_days(1).unwrap())
                        .date_naive()
                        .to_string()
                    max=(chrono::Utc::now() + chrono::Duration::try_days(366).unwrap())
                        .date_naive()
                        .to_string()
                    class="rounded-md input input-bordered focus:input-accent"
                    required
                />
            </div>
            <table class="table text-base w-full table-auto mt-2">
                <PermissionSection text="user" />
                <PermissionOption
                    id="user_edit"
                    label="edit"
                    value="UserEdit"
                    description="Edit user info (display name, description, icon etc.)"
                    set_permissions
                />
                <tr>
                    <td colspan="2" class="p-1"></td>
                </tr>
                <PermissionSection text="novel" />
                <PermissionOption
                    id="novel_edit"
                    label="edit"
                    value="NovelEdit"
                    description="Edit novel metadata (title, summary etc.)"
                    set_permissions
                />
                <PermissionOption
                    id="novel_write"
                    label="write"
                    value="NovelWrite"
                    description="Create and delete novels"
                    set_permissions
                />
                <tr>
                    <td colspan="2" class="p-1"></td>
                </tr>
                <PermissionSection text="chapter" />
                <PermissionOption
                    id="chapter_edit"
                    label="edit"
                    value="ChapterEdit"
                    description="Edit chapter content and metadata"
                    set_permissions
                />
                <PermissionOption
                    id="chapter_write"
                    label="write"
                    value="ChapterWrite"
                    description="Create and delete chapters"
                    set_permissions
                />
            </table>
            <input type="hidden" name="permissions" value=move || permissions().join(" ") />
            <input type="submit" class="btn btn-accent" value="Generate" />
            <div class="divider divider-vertical"></div>
        </ActionForm>
    }
}

#[component]
fn TokenRow(
    token: TokenItem,
    awaiting_delete: RwSignal<Option<String>>,
    delete_action: Action<String, Result<String, ServerFnError>>,
) -> impl IntoView {
    let (confirm_hidden, hide_confirm) = create_signal(true);

    view! {
        <tr class:hidden=confirm_hidden>
            <td colspan="100%" class="border-error border-solid border-2">
                <div class="flex flex-row justify-between w-full p-2">
                    <div class="flex flex-row gap-2 w-fit">
                        <span class="my-auto">"Are you sure you want to delete this token?"</span>
                    </div>
                    <div class="flex flex-row w-fit gap-1">
                        <button
                            class="btn btn-error"
                            on:click={
                                let label = token.label.clone();
                                move |_| {
                                    awaiting_delete.set(Some(label.clone()));
                                    delete_action.dispatch(label.to_owned());
                                    hide_confirm(true);
                                }
                            }
                        >

                            <Icon icon=icondata::BsCheckLg class="w-8 h-8 m-auto" />
                        </button>
                        <button
                            class="btn btn-outline btn-error"
                            on:click=move |_| hide_confirm(true)
                        >
                            <Icon icon=icondata::BsXLg class="w-8 h-8 m-auto" />
                        </button>
                    </div>
                </div>
            </td>
        </tr>
        <tr class:hidden=move || !confirm_hidden()>
            <td>{token.label.clone()}</td>
            <td>{token.created.format("%c").to_string()}</td>
            <td>
                {token
                    .expiry
                    .map(|d| {
                        view! { <span>{d.format("%c").to_string()}</span> }
                    })
                    .unwrap_or_else(|| {
                        view! { <span class="text-error">"Never"</span> }
                    })}

            </td>
            <td>
                <div class="flex flex-row justify-start flex-wrap gap-1">
                    {token
                        .permissions
                        .into_iter()
                        .map(|p| {
                            view! {
                                <span class="font-mono px-1 w-fit rounded-md bg-neutral">{p}</span>
                            }
                        })
                        .collect_view()}
                </div>
            </td>
            <td>
                <button
                    class="btn btn-outline btn-error"
                    on:click=move |_| {
                        if confirm_hidden() || awaiting_delete().is_some() {
                            hide_confirm(false);
                        }
                    }
                >

                    <Show
                        when=move || {
                            awaiting_delete() == Some(token.label.to_owned())
                                && delete_action.pending()()
                        }

                        fallback=move || {
                            view! {
                                <Icon icon=icondata::CgTrash class="stroke-error w-6 h-6 m-auto" />
                            }
                        }
                    >

                        <span class="loading loading-spinner loading-md m-auto"></span>
                    </Show>
                </button>
            </td>
        </tr>
    }
}

#[component]
fn PermissionSection(text: &'static str) -> impl IntoView {
    view! {
        <tr>
            <td colspan="2" class="py-1">
                <h2 class="font-mono px-1 w-fit rounded-md bg-neutral">{text}</h2>
            </td>
        </tr>
    }
}

#[component]
fn PermissionOption(
    id: &'static str,
    label: &'static str,
    value: &'static str,
    description: &'static str,
    set_permissions: WriteSignal<Vec<String>>,
) -> impl IntoView {
    view! {
        <tr>
            <td class="flex flex-row w-full justify-start gap-4 py-1">
                <input
                    type="checkbox"
                    class="checkbox checkbox-accent my-auto w-6 h-6 rounded-md"
                    id=id
                    on:change=move |ev| {
                        let target = event_target::<web_sys::HtmlInputElement>(&ev);
                        if target.checked() {
                            update!(| set_permissions | set_permissions.push(value.to_string()))
                        } else {
                            update!(| set_permissions | set_permissions.retain(| v | v != value))
                        }
                    }
                />

                <label class="my-auto w-fit px-1 font-mono rounded-md bg-neutral" for=id>
                    {label}
                </label>
            </td>
            <td>{description}</td>
        </tr>
    }
}

#[server]
async fn create_token(
    label: String,
    permissions: String,
    expiry: Option<String>,
) -> Result<String, ServerFnError> {
    use activitypub_federation::config::Data;
    use chrono::DateTime;
    use leptos_axum::extract;
    use std::str::FromStr;
    use tower_sessions::Session;
    use wordforge_api::{
        tokens::{generate_token, store_api_token, Claims, Permission},
        DataHandle,
    };

    let data: Data<DataHandle> = extract().await?;
    let session: Session = extract().await?;

    let owner = session
        .get::<String>("id")
        .await?
        .ok_or_else(|| ServerFnError::new("Unauthorized"))?;

    let permissions = permissions.split(' ').flat_map(Claims::from_str).collect();

    // Append timezone before parsing
    let expiry = if let Some(expiry) = expiry.map(|v| format!("{} 00:00:00 +00:00", v)) {
        Some(DateTime::parse_from_str(&expiry, "%Y-%m-%d %H:%M:%S %z")?.into())
    } else {
        None
    };

    let info = Permission {
        label,
        owner,
        permissions,
        expiry,
    };

    let token = generate_token();
    store_api_token(&token, info, &data.pool)
        .await
        .map_err(ServerFnError::new)?;

    Ok(token)
}

#[server]
async fn get_tokens() -> Result<Vec<TokenItem>, ServerFnError> {
    use activitypub_federation::config::Data;
    use leptos_axum::extract;
    use sqlx::query_as;
    use tower_sessions::Session;
    use wordforge_api::DataHandle;

    let data: Data<DataHandle> = extract().await?;
    let session: Session = extract().await?;

    let owner = session
        .get::<String>("id")
        .await?
        .ok_or_else(|| ServerFnError::new("Unauthorized"))?;

    let pool = data.pool.as_ref();

    let result = query_as!(
        TokenItem,
        r#"
            SELECT
                label, created, expiry, permissions
            FROM
                api_tokens
            WHERE
                owner = $1
        "#,
        owner
    )
    .fetch_all(pool)
    .await
    .map(|v| {
        v.into_iter()
            .map(|t| {
                let mut t = t;
                t.permissions.sort();
                t
            })
            .collect()
    })
    .map_err(ServerFnError::new)?;

    Ok(result)
}

#[server]
async fn delete_token(label: String) -> Result<String, ServerFnError> {
    use activitypub_federation::config::Data;
    use leptos_axum::extract;
    use sqlx::query;
    use tower_sessions::Session;
    use wordforge_api::DataHandle;

    let data: Data<DataHandle> = extract().await?;
    let session: Session = extract().await?;

    let owner = session
        .get::<String>("id")
        .await?
        .ok_or_else(|| ServerFnError::new("Unauthorized"))?;

    let pool = data.pool.as_ref();

    query!(
        r#"
            DELETE FROM
                api_tokens
            WHERE
                label = $1 AND
                owner = $2
        "#,
        label,
        owner
    )
    .execute(pool)
    .await
    .map_err(ServerFnError::new)?;

    Ok(label)
}
