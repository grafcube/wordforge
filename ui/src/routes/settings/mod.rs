use crate::components::Panel;
use admin::check_is_admin;
use leptos::*;
use leptos_icons::*;
use leptos_meta::*;
use leptos_router::*;

pub mod account;
pub mod admin;
pub mod developer;

pub use account::Account;
pub use admin::Admin;
pub use developer::Developer;

#[component]
pub fn SettingsView() -> impl IntoView {
    view! {
        <Title text="Settings"/>
        <div class="flex flex-col md:flex-row w-full gap-4 px-2 md:px-0">
            <SettingsNav/>
            <div class="w-full">
                <Outlet/>
            </div>
        </div>
    }
}

#[component]
fn SettingsNav() -> impl IntoView {
    let panel = RwSignal::new(false);
    let (category, set_category) = create_signal("Account".to_string());

    view! {
        <Panel
            class="dropdown md:hidden"
            summary_class="btn flex flex-row w-full mt-2 light:btn-primary"
            state=panel
            summary=move || {
                view! {
                    <span class="text-left">{category}</span>
                    {move || {
                        view! {
                            <Icon
                                icon=if panel() {
                                    icondata::CgChevronUp
                                } else {
                                    icondata::CgChevronDown
                                }

                                class="my-auto ml-auto w-8 h-8"
                            />
                        }
                    }}
                }
            }
        >

            <ul class="shadow menu dropdown-content flex-nowrap max-h-80 overflow-y-auto mt-1 z-50 bg-base-200 light:bg-primary rounded-md text-left justify-items-stretch w-full">
                <NavList set_category/>
            </ul>
        </Panel>
        <div class="hidden md:block">
            <ul class="menu h-fit gap-1 rounded-md md:w-60 md:text-lg overflow-y-auto bg-base-200 light:bg-primary">
                <NavList set_category/>
            </ul>
        </div>
    }
}

#[component]
fn NavList(set_category: WriteSignal<String>) -> impl IntoView {
    let is_admin = Resource::once(check_is_admin);

    view! {
        <NavItem label="Account" href="account" set_category/>
        <NavItem label="Developer" href="developer" set_category/>
        <Suspense>
            <ErrorBoundary fallback=move |e| {
                e()
                    .into_iter()
                    .for_each(|(_, e)| logging::error!("Failed to check admin status: {}", e));
                ().into_view()
            }>
                {move || {
                    is_admin()
                        .map(|v| {
                            v.map(|v| {
                                v.then(|| {
                                    view! { <NavItem label="Admin" href="admin" set_category/> }
                                })
                            })
                        })
                }}

            </ErrorBoundary>
        </Suspense>
    }
}

#[component]
fn NavItem(
    label: &'static str,
    href: &'static str,
    set_category: WriteSignal<String>,
) -> impl IntoView {
    view! {
        <li>
            <A
                href
                replace=true
                on:click=move |_| set_category(label.to_string())
                class="w-full hover:btn-primary"
            >
                {label}
            </A>
        </li>
    }
}

#[component]
pub fn TokenResponse(token: String, #[prop(optional)] hide_message: bool) -> impl IntoView {
    view! {
        <div class="flex flex-col w-full p-2 my-1 gap-1 rounded-md bg-info text-info-content">
            <span class:hidden=hide_message>
                "This token will not be shown again. Do not share it with anyone."
            </span>
            <div class="flex flex-row justify-stretch join bg-neutral rounded-md">
                <textarea
                    rows="1"
                    class="p-2 w-full rounded-l-md resize-none overflow-x-auto whitespace-nowrap textarea textarea-bordered text-base-content join-item"
                    readonly
                >
                    {token.clone()}
                </textarea>
                <button
                    class="flex flex-row p-1 rounded-r-md btn btn-neutral join-item"
                    on:click={
                        let token = token.clone();
                        move |_| {
                            let clipboard = window().navigator().clipboard();
                            let _ = clipboard.write_text(&token);
                        }
                    }
                >

                    <Icon icon=icondata::BiCopyRegular class="py-1 w-6 h-6 my-auto"/>
                    <span class="my-auto mr-1">"Copy"</span>
                </button>
            </div>
        </div>
    }
}
