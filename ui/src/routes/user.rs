use crate::{
    app::ValidationResult,
    components::{textarea_resize, DialogForm},
    fallback::{InternalErrorPage, NotFoundPage},
    path::UserViewParams,
};
use leptos::{
    html::*,
    logging::{error, log},
    *,
};
use leptos_icons::*;
use leptos_meta::*;
use leptos_router::*;
use serde::{Deserialize, Serialize};

#[component]
pub fn UserView() -> impl IntoView {
    let params = use_params::<UserViewParams>();
    let name = move || with!(|params| params.clone().map(|p| p.name)).unwrap();
    let user = Resource::new(name, get_user);

    view! {
        <Title text="User" />
        <div class="mx-auto max-w-2xl px-4">
            <Suspense fallback=move || {
                view! { <span class="loading loading-spinner loading-lg m-auto"></span> }
                    .into_view()
            }>
                {move || {
                    user.get()
                        .map(|v| match v {
                            Err(e) => {
                                error!("user server fn: {}", e.to_string());
                                view! { <InternalErrorPage /> }.into_view()
                            }
                            Ok(Err(e)) => {
                                log!("user view: {e}");
                                view! { <NotFoundPage /> }.into_view()
                            }
                            Ok(Ok(user)) => view! { <UserPage user /> }.into_view(),
                        })
                }}

            </Suspense>
        </div>
    }
}

#[component]
fn UserPage(user: User) -> impl IntoView {
    let user = RwSignal::new(user);
    let validate = use_context::<Callback<(), Option<ValidationResult>>>().unwrap();
    let edit_node = create_node_ref::<Dialog>();

    view! {
        <Title text=move || {
            with!(| user | user.name.clone().unwrap_or_else(|| user.preferred_username.clone()))
        } />
        <div class="flex flex-row gap-2 w-full justify-center">
            <h1 class="p-2 text-3xl">
                {move || {
                    with!(
                        | user | user.name.clone().unwrap_or_else(|| user.preferred_username
                        .clone())
                    )
                }}

            </h1>
            <button
                class="p-1"
                class:hidden=move || {
                    !matches!(validate(()), Some(Ok(session)) if session.id == user().apub_id)
                }

                on:click=move |_| {
                    if let Some(edit) = edit_node() {
                        edit.show_modal().unwrap();
                    }
                }
            >

                <Icon icon=icondata::BiPencilRegular class="w-8 h-8 my-auto" />
            </button>
            <UserEdit user node_ref=edit_node />
        </div>
        <div class="flex flex-col bg-base-200 rounded-xl text-xl md:text-base overflow-auto max-h-40 my-2 px-4 py-2">
            {move || {
                user()
                    .summary
                    .lines()
                    .map(|line| { view! { <p>{line.to_string()}</p> }.into_view() })
                    .collect::<Vec<_>>()
            }}
            {move || {
                user()
                    .published
                    .as_ref()
                    .map(|published| {
                        view! {
                            <span class="mt-2 italic text-neutral-content">
                                "Joined " {published}
                            </span>
                        }
                    })
            }}

        </div>
        <div class="flex flex-col w-full rounded-xl px-4 py-2 my-2 bg-base-200">
            <div class="flex flex-row justify-between w-full">
                <span class="text-neutral-content text-lg my-auto p-1">"Books"</span>
            </div>
            <div class="my-2 gap-1 w-full">
                <Suspense fallback=move || {
                    view! { <span class="loading loading-spinner loading-lg m-auto"></span> }
                }>
                    <span
                        class:hidden=move || !user().books.is_empty()
                        class="w-full mx-auto italic text-neutral-content"
                    >
                        "No books found"
                    </span>
                    {move || view! { <BookList list=user().books.clone() /> }}
                </Suspense>
            </div>
        </div>
    }
}

#[component]
fn UserEdit(user: RwSignal<User>, node_ref: NodeRef<Dialog>) -> impl IntoView {
    let valid = use_context::<Resource<(), Result<ValidationResult, ServerFnError>>>().unwrap();
    let edit = create_server_action::<EditUser>();
    let response = edit.value();
    let err = move || {
        response().and_then(|response| match response {
            Ok((name, summary)) => {
                if let Some(dialog) = node_ref() {
                    dialog.close();
                }
                valid.refetch();
                update!(|user| {
                    user.name = name;
                    user.summary = summary;
                });
                None
            }
            Err(e) => Some(e.to_string()),
        })
    };

    view! {
        <DialogForm
            class="flex flex-col justify-center text-center place-content-center items-center space-y-4 p-4 w-full"
            title="User Profile"
            node_ref
            action=edit
            pre_form=move || {
                view! {
                    <div
                        class="card mx-1 my-4 bg-error text-error-content"
                        class:hidden=move || err().is_none()
                    >
                        <div class="card-body">{err}</div>
                    </div>
                }
            }
        >

            <div class="relative w-full">
                <input
                    name="name"
                    id="name"
                    placeholder=" "
                    value=move || user().name
                    class="floating-input input-bordered focus:input-primary rounded-xl text-xl w-full peer"
                />
                <label for="name" class="floating-label">
                    "Display name"
                </label>
            </div>
            <div class="relative w-full">
                <textarea
                    name="summary"
                    id="summary"
                    placeholder=" "
                    on:input=textarea_resize
                    on:paste=textarea_resize
                    class="floating-input input-bordered focus:input-primary rounded-xl text-xl w-full peer"
                >
                    {move || user().summary}
                </textarea>
                <label for="summary" class="floating-label">
                    "Summary"
                </label>
            </div>
            <div class="relative">
                <button class="btn btn-primary" type="submit">
                    "Save"
                </button>
            </div>
        </DialogForm>
    }
}

#[server]
async fn edit_user(
    name: Option<String>,
    summary: Option<String>,
) -> Result<(Option<String>, String), ServerFnError> {
    use activitypub_federation::config::Data;
    use leptos_axum::extract;
    use tower_sessions::Session;
    use wordforge_api::{
        api::user::{edit_user, EditUser, EditUserError},
        DataHandle,
    };

    let data: Data<DataHandle> = extract().await?;
    let session: Session = extract().await?;

    let name = Some(name.unwrap_or_default());
    let summary = Some(summary.unwrap_or_default());

    let apub_id = session
        .get::<String>("id")
        .await?
        .ok_or_else(|| ServerFnError::new("Failed to validate cookie"))?;

    let info = EditUser {
        display_name: name,
        summary,
    };

    match edit_user(&data.pool, info, &apub_id).await {
        Ok(v) => Ok((v.name, v.summary)),
        Err(EditUserError::InternalServerError(e)) => Err(ServerFnError::new(e)),
    }
}

#[component]
fn BookList(list: Vec<BookEntry>) -> impl IntoView {
    view! {
        {list
            .into_iter()
            .map(|book| {
                view! {
                    <A
                        href=book.href
                        class="flex flex-col gap-1 w-full p-2 rounded-xl transition ease-in-out duration-[400ms] hover:bg-base-300"
                    >
                        <div class="flex flex-row justify-start gap-1">
                            <span class:hidden=!book.sensitive class="badge badge-error my-auto">
                                "CW"
                            </span>
                            <h3 class="font-bold my-auto overflow-hidden text-ellipsis w-full max-w-full whitespace-nowrap">
                                {book.title}
                            </h3>
                            <span class="text-neutral-content my-auto text-right w-fit whitespace-nowrap">
                                {book.published}
                            </span>
                        </div>
                        <div class="italic h-6 md:text-sm overflow-hidden overflow-ellipsis text-neutral-content">
                            {book
                                .summary
                                .lines()
                                .map(|p| {
                                    view! { <p>{p.to_string()}</p> }
                                })
                                .collect::<Vec<_>>()}
                        </div>
                    </A>
                }
            })
            .collect_view()}
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct BookEntry {
    href: String,
    title: String,
    summary: String,
    genre: String,
    tags: Vec<String>,
    language: String,
    sensitive: bool,
    published: String,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct User {
    apub_id: String,
    preferred_username: String,
    name: Option<String>,
    summary: String,
    books: Vec<BookEntry>,
    published: Option<String>,
}

#[server]
async fn get_user(name: String) -> Result<Result<User, String>, ServerFnError> {
    use activitypub_federation::{config::Data, fetch::object_id::ObjectId};
    use chrono::DateTime;
    use chrono_humanize::HumanTime;
    use leptos_axum::extract;
    use rayon::prelude::*;
    use wordforge_api::{
        api::user::{self, get_user_books, GetUserError, UserBooksError},
        objects::novel::DbNovel,
        util::generate_url_preview,
        DataHandle,
    };

    let data: Data<DataHandle> = extract().await?;

    match user::get_user(&name, &data).await {
        Ok(v) => {
            let books = get_user_books(&name, &data)
                .await
                .unwrap_or_else(|e| match e {
                    UserBooksError::NotFound => {
                        log::warn!("User books not found: {name}");
                        vec![]
                    }
                    UserBooksError::InternalServerError(e) => {
                        log::warn!("User books Internal Server Error: {e}");
                        vec![]
                    }
                });

            let books_ref: Vec<ObjectId<DbNovel>> = books
                .into_par_iter()
                .filter_map(|v| v.parse().ok())
                .collect();

            let mut books = Vec::new();
            for book in books_ref.into_iter() {
                books.push((book.clone().into_inner(), book.dereference(&data).await));
            }

            let books = books
                .into_par_iter()
                .filter_map(|(book_id, book)| {
                    #[allow(clippy::redundant_locals)]
                    let book = book
                        .map_err(|e| log::warn!("Failed to load {}: {}", book_id, e))
                        .ok()?;
                    let ndata = book.data?;

                    let domain = format!(
                        "{}{}",
                        book_id.host_str().expect("apub_id hostname"),
                        book_id
                            .port()
                            .map(|p| format!(":{}", p))
                            .unwrap_or_default()
                    );

                    Some(BookEntry {
                        href: format!(
                            "/novel/{}{}/{}",
                            ndata.preferred_username,
                            domain
                                .eq(data.domain())
                                .then_some(String::new())
                                .unwrap_or(format!("@{}", domain)),
                            generate_url_preview(&ndata.title)
                        ),
                        title: ndata.title,
                        summary: ndata.summary,
                        genre: ndata.genre.to_string(),
                        tags: ndata.tags,
                        language: ndata.language.to_name().to_string(),
                        sensitive: ndata.sensitive,
                        published: HumanTime::from(ndata.published).to_string(),
                    })
                })
                .collect::<Vec<_>>();

            let user = User {
                apub_id: v.id.to_string(),
                preferred_username: v.preferred_username.clone(),
                name: v.name,
                summary: v.summary.unwrap_or_default(),
                published: v.published.and_then(|p| {
                    DateTime::parse_from_rfc3339(&p)
                        .ok()
                        .map(|t| HumanTime::from(t).to_string())
                }),
                books,
            };

            Ok(Ok(user))
        }
        Err(GetUserError::PermanentRedirect(loc)) => {
            leptos_axum::redirect(&loc);
            Ok(Err("Redirect".to_string()))
        }
        Err(GetUserError::InternalServerError(e)) => Err(ServerFnError::new(e)),
        _ => Ok(Err("NotFound".to_string())),
    }
}
