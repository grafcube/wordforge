import { Config } from "tailwindcss";
import { themeVariants, prefersLight, prefersDark } from "tailwindcss-theme-variants";

export default {
  content: {
    files: ["./ui/*.html", "./ui/src/**/*.rs"],
  },
  theme: {
    extend: {},
  },
  plugins: [
    require("@tailwindcss/typography"),
    require("daisyui"),
    themeVariants({
      themes: {
        light: {
          mediaQuery: prefersLight /* "@media (prefers-color-scheme: light)" */,
        },
        dark: {
          mediaQuery: prefersDark /* "@media (prefers-color-scheme: dark)" */,
        },
      },
    }),
  ],
  daisyui: {
    logs: false,
    themes: [
      {
        light: {
          // catppuccin latte
          primary: "#8839ef", // mauve
          secondary: "#209fb5", // sapphire
          accent: "#ea76cb", // pink
          neutral: "#dce0e8", // crust
          "base-100": "#eff1f5", // base
          info: "#209fb5", // sapphire
          success: "#40a02b", // green
          warning: "#df8e1d", // yellow
          error: "#d20f39", // red
        },
        dark: {
          // catppuccin mocha
          primary: "#cba6f7", // mauve
          secondary: "#74c7ec", // sapphire
          accent: "#f5c2e7", // pink
          neutral: "#11111b", // crust
          "base-100": "#1e1e2e", // base
          info: "#74c7ec", // sapphire
          success: "#a6e3a1", // green
          warning: "#f9e2af", // yellow
          error: "#f38ba8", // red
        },
      },
    ],
  },
} satisfies Config;
