use std::path::Path;
use std::process::Command;

fn main() {
    println!("cargo::rerun-if-changed=build.rs");

    let node_dir = Path::new(r"./node_modules");

    if !node_dir.exists() {
        Command::new("npm")
            .arg("install")
            .spawn()
            .expect("Failed to install npm dependencies")
            .wait()
            .expect("Failed to install npm dependencies");
    }
}
