# Development Guide

## Requirements

- ActivityPub protocol
- Rust
  - `actix-web`
  - `sqlx`
  - `leptos`
- Podman
- PostgreSQL
- Tailwind CSS
- Nix shell (Optional but recommended)

## Development

1. Ensure that the dependencies are available.

   > [!TIP]
   >
   > Use nix to ensure that all the dependencies are available.
   > Otherwise, refer to the `flake.nix` file for a list of what you need.

   ```sh
   nix develop
   ```

2. Configure the `.env` file.

   ```sh
   cp .env.example .env
   ```

   > [!TIP]
   >
   > To reduce the noise in logging, it is recommended to set this environment
   > variable (you can add it to the `.env` file).
   >
   > ```sh
   > RUST_LOG="debug,leptos_dom=warn,html5ever=warn,tracing::span=warn,leptos_reactive=warn,hyper::proto=warn"
   > ```

3. Start the servers needed at runtime.

   ```sh
   podman-compose up -d
   ```

4. Run SQL migrations.

   ```sh
   just database
   ```

5. Start the server.

   Watch mode:

   ```sh
   cargo leptos watch
   ```

   Serve mode:

   ```sh
   cargo leptos serve
   ```

   Run the release build on localhost and/or http:

   ```sh
   cargo leptos serve --release -- --debug-release
   ```

## Project Structure

The repo is a cargo workspace with multiple projects. The main things you
might be interested in are:

```
.
├── api
├── server
└── ui
```

### `server`

This is the main entry point of the server. It loads the server-side rendered UI
and provides the api endpoints.

```
server/src
├── api
│   ├── chapter.rs  -> Api endpoints related to chapters (create/edit/parse/delete)
│   ├── mod.rs
│   ├── novel.rs    -> Api endpoints related to books (create/edit/delete)
│   └── user.rs     -> Api endpoints related to users (auth/edit/delete)
├── instance.rs     -> Federated instance configs (webfinger, nodeinfo etc.)
└── main.rs         -> Entry point
```

### `ui`

This contains the frontend with the leptos framework. This is where the website
UI is constructed. Check out [the leptos project](https://leptos.dev) to get
started.

```
ui/src
├── app.rs            -> Main entry point for UI
├── components.rs     -> Reusable UI components
├── fallback.rs       -> Fallback routes (404, 403, 500 etc.)
├── lib.rs
├── path.rs
└── routes            -> Specific routes (user page, reader, settings and admin)
    ├── auth.rs
    ├── chapter.rs
    ├── mod.rs
    ├── novel.rs
    ├── settings.rs
    └── user.rs
```

### `api`

This is the main backbone of the program. It contains the behaviour that is
common to the `server` and the `ui` crates. It has all the data structures,
parsing and document processing functions.

This is the main part of the server and contains most of the program's
functionality.

> [!NOTE]
>
> The `ui` crate only depends on `api` on the server-side (server functions). It
> is not available in client-side code.

```
api/src
├── account.rs         -> Account auth (login, register etc.)
├── activities         -> ActivityPub activities
│   ├── add.rs
│   └── mod.rs
├── api                -> Common functions to manage various data structures
│   ├── chapter.rs
│   ├── mod.rs
│   ├── novel.rs
│   └── user.rs
├── enums.rs           -> Metadata enums
├── filters.rs         -> Pandoc filters (image proxy, mentions etc.)
├── lib.rs
├── nodeinfo.rs        -> Instance configuration
├── objects            -> Data structures for UI and federation
│   ├── chapter.rs
│   ├── mod.rs
│   ├── novel_list.rs
│   ├── novel.rs
│   └── person.rs
├── tokens.rs          -> API token generation and validation
└── util.rs            -> Useful and reusable functions
```
