use crate::error::ServerResult;
use activitypub_federation::{
    config::{Data, UrlVerifier},
    fetch::webfinger::{
        build_webfinger_response_with_type, extract_webfinger_name, WEBFINGER_CONTENT_TYPE,
    },
};
use axum::{
    async_trait,
    extract::{Query, State},
    http::{header, StatusCode},
    response::IntoResponse,
    Json,
};
use axum_macros::debug_handler;
use nodeinfo::NodeInfoOwned;
use serde::Deserialize;
use url::Url;
use wordforge_api::{
    nodeinfo::{build_nodeinfo, InstanceState, NodeInfoLinks, NodeInfoLinksBuilder},
    objects::{novel::DbNovel, person::User},
    DataHandle,
};

#[derive(Clone)]
pub(crate) struct VerifyUrl();

#[async_trait]
impl UrlVerifier for VerifyUrl {
    async fn verify(&self, url: &Url) -> Result<(), activitypub_federation::error::Error> {
        log::info!("{}", url);
        Ok(())
    }
}

#[derive(Deserialize)]
pub struct WebfingerQuery {
    resource: String,
}

#[debug_handler]
pub async fn webfinger(
    Query(query): Query<WebfingerQuery>,
    data: Data<DataHandle>,
) -> ServerResult<impl IntoResponse> {
    let Ok(name) = extract_webfinger_name(&query.resource, &data) else {
        return Ok((StatusCode::NOT_FOUND, "Not found").into_response());
    };
    let user = User::read_from_username(name, &data.pool)
        .await
        .map_err(|e| log::error!("Failed to fetch webfinger user {}: {}", name, e))
        .unwrap_or(None);
    let novel = DbNovel::from_novel_id(name, &data)
        .await
        .map_err(|e| log::error!("Failed to fetch webfinger novel {}: {}", name, e))
        .unwrap_or(None);

    let urls: Vec<(Url, Option<&str>)> = vec![
        (
            novel.map(|v| Url::parse(&v.apub_id).expect("novel parse error")),
            Some("Group"),
        ),
        (
            user.map(|v| Url::parse(&v.apub_id).expect("user parse error")),
            Some("Person"),
        ),
    ]
    .into_iter()
    .filter(|v| v.0.is_some())
    .map(|v| (v.0.unwrap(), v.1))
    .collect();

    if urls.is_empty() {
        Ok((StatusCode::NOT_FOUND, "Local actor not found").into_response())
    } else {
        Ok((
            [(header::CONTENT_TYPE, WEBFINGER_CONTENT_TYPE.clone())],
            Json(build_webfinger_response_with_type(
                query.resource.clone(),
                urls,
            )),
        )
            .into_response())
    }
}

#[debug_handler]
pub async fn nodeinfo_links(data: Data<DataHandle>) -> ServerResult<Json<NodeInfoLinks>> {
    let href = format!("{}://{}/nodeinfo/2.1", data.protocol, data.domain())
        .parse()
        .unwrap();

    let nodeinfo_links = NodeInfoLinksBuilder::default()
        .add_link(
            format!(
                "{}://nodeinfo.diaspora.software/ns/schema/2.1",
                data.protocol
            )
            .parse()
            .unwrap(),
            href,
        )
        .build()?;

    Ok(nodeinfo_links.into())
}

#[debug_handler]
pub async fn nodeinfo_2_1(
    State(instance): State<InstanceState>,
) -> ServerResult<Json<NodeInfoOwned>> {
    Ok(build_nodeinfo(&instance)?.into())
}
