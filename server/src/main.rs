use crate::instance::{nodeinfo_links, webfinger, VerifyUrl};
use activitypub_federation::{
    config::{FederationConfig, FederationMiddleware},
    FEDERATION_CONTENT_TYPE,
};
use api::{
    chapter::{get_chapter, get_chapter_toc, get_raw_chapter, redirect_to_chapter},
    novel::{add_chapter, get_novel, new_novel, novel_feed, novel_inbox, novel_outbox},
    user::{edit_user_info, get_user, user_outbox},
};
use axum::{
    extract::Request,
    http::{header::ACCEPT, Uri},
    routing::{get, post, put},
    Router, ServiceExt,
};
use clap::Parser;
use instance::nodeinfo_2_1;
use leptos::*;
use leptos_axum::{generate_route_list, LeptosRoutes};
use leptos_config::ConfFile;
use sqlx::{migrate, postgres::PgPoolOptions, query};
use std::{
    fs::{self, create_dir_all, File},
    io::{self, Write},
    net::SocketAddr,
    path::{Path, PathBuf},
    sync::{Arc, RwLock},
    time::Duration,
};
use tokio::{net::TcpListener, spawn, time};
use tower::Layer;
use tower_http::{
    catch_panic::CatchPanicLayer, compression::CompressionLayer,
    normalize_path::NormalizePathLayer, services::ServeDir, timeout::TimeoutLayer,
    trace::TraceLayer,
};
use tower_sessions::{
    cookie::{self, Key},
    CachingSessionStore, Expiry, MemoryStore, SessionManagerLayer,
};
use tower_sessions_sqlx_store_chrono::PostgresStore;
use url::Url;
use which::which;
use wordforge_api::{
    nodeinfo::{InstanceMetadata, InstanceState},
    DataHandle, DbHandle,
};
use wordforge_ui::app::*;

mod api;
mod error;
mod instance;

/// Initial server configuration for Wordforge
#[derive(Debug, Parser)]
#[command(version, about)]
pub struct Config {
    /// Run the release build of the library with debug features enabled.
    /// The server will run on localhost and http.
    /// NOT RECOMMENDED FOR PRODUCTION
    #[arg(long)]
    pub debug_release: bool,

    /// Path to cookie key file
    #[arg(short, long, value_name = "FILE", env = "COOKIE_FILE")]
    pub cookie_key: Option<PathBuf>,

    /// Path to file store
    #[arg(short, long, value_name = "DIR", env = "FILE_STORE")]
    pub file_store: PathBuf,

    /// Address to serve the site
    #[arg(
        short,
        long,
        value_delimiter = ',',
        value_name = "ADDR",
        env = "SITE_ADDR",
        default_value = "127.0.0.1:3000"
    )]
    pub site_addrs: Vec<SocketAddr>,

    /// Domain of the instance
    #[arg(
        short,
        long,
        value_name = "URL",
        env = "DOMAIN",
        default_value = "http://localhost:3000"
    )]
    pub domain: Url,

    /// Url to PostgreSQL database
    #[arg(long, value_name = "URL", env = "DATABASE_URL")]
    pub db_url: Url,

    /// Address of media storage
    #[arg(long, value_name = "ADDR", env = "MEDIA_ADDR")]
    pub media_addr: Url,
}

#[tokio::main]
async fn main() -> io::Result<()> {
    let args = setup_environment();
    check_dependencies();
    run(args).await
}

/// Run the server with the given configuration
pub async fn run(args: Config) -> io::Result<()> {
    let uiconf = setup_site_addr_conf(args.site_addrs[0]).await?;
    let pool = setup_database(&args.db_url).await?;
    let key = args
        .cookie_key
        .map(|path| Key::from(fs::read(&path).unwrap().as_slice()));

    let domain = format!(
        "{}{}",
        args.domain.host_str().unwrap(),
        args.domain
            .port()
            .map(|p| format!(":{}", p))
            .unwrap_or_default()
    );

    let routes = generate_route_list(App);

    setup_file_store(&args.file_store)?;
    let instance_metadata =
        InstanceMetadata::initialize(&args.file_store.join("instance.json"), args.debug_release)?;
    let instance_metadata: InstanceState = Arc::new(RwLock::new(instance_metadata));

    let app_data = DataHandle {
        protocol: args.domain.scheme().into(),
        file_store: args.file_store.as_path().into(),
        pool: pool.clone(),
        media_addr: args.media_addr,
    };

    let config = FederationConfig::builder()
        .debug(cfg!(debug_assertions) || args.debug_release)
        .domain(domain)
        .url_verifier(Box::new(VerifyUrl()))
        .app_data(app_data)
        .build()
        .await
        .unwrap();

    let postgres_store = PostgresStore::new(pool.as_ref().clone());
    postgres_store.migrate().await.unwrap();

    let caching_store = CachingSessionStore::new(MemoryStore::default(), postgres_store);
    let session_layer = SessionManagerLayer::new(caching_store)
        .with_secure(false)
        .with_expiry(Expiry::OnInactivity(cookie::time::Duration::weeks(1)));

    let apub_router = Router::new()
        .route("/user/:id", get(get_user))
        .route("/novel/:id", get(get_novel))
        .route("/novel/:id/inbox", post(novel_inbox))
        .route("/novel/:id/:seq", get(redirect_to_chapter))
        .route("/chapter/:id", get(get_chapter));

    let api_router = Router::new()
        .route("/user/:name", put(edit_user_info))
        .route("/novel/:id/create", post(add_chapter))
        .route("/novel", post(new_novel));

    let app = Router::new()
        .leptos_routes_with_context(
            &uiconf.leptos_options,
            routes,
            {
                let state = instance_metadata.clone();
                move || provide_context(state.clone())
            },
            App,
        )
        .with_state(uiconf.leptos_options.clone())
        .nest("/apub", apub_router)
        .nest("/api/v1", api_router)
        .route("/user/:name/outbox", get(user_outbox))
        .route("/novel/:id/outbox", get(novel_outbox))
        .route("/novel/:id/feed.atom", get(novel_feed))
        .route("/chapter/:id/toc.html", get(get_chapter_toc))
        .route("/chapter/:id/content.html", get(get_raw_chapter))
        .route("/.well-known/webfinger", get(webfinger))
        .route("/.well-known/nodeinfo", get(nodeinfo_links))
        .route("/nodeinfo/2.1", get(nodeinfo_2_1))
        .fallback_service(ServeDir::new(uiconf.leptos_options.site_root))
        .layer(CompressionLayer::new())
        .layer(NormalizePathLayer::trim_trailing_slash())
        .layer(CatchPanicLayer::new())
        .layer(FederationMiddleware::new(config))
        .with_state(instance_metadata);

    let app = if !(cfg!(debug_assertions) || args.debug_release) {
        app.layer(TimeoutLayer::new(Duration::from_secs(60)))
    } else {
        app.layer(TraceLayer::new_for_http())
    };

    let app = if let Some(key) = key {
        log::info!("Using secure cookies");
        app.layer(session_layer.clone().with_secure(true).with_private(key))
    } else {
        app.layer(session_layer)
    };

    let apub_middleware = axum::middleware::map_request(apub_middleware);
    let app = apub_middleware.layer(app);
    let app = ServiceExt::<Request>::into_make_service(app);

    log::info!("Starting server {}", args.domain);
    args.site_addrs
        .iter()
        .for_each(|addr| log::info!("Listening on: {}", addr));

    let listener = TcpListener::bind(args.site_addrs.as_slice()).await?;

    axum::serve(listener, app).await?;

    Ok(())
}

async fn apub_middleware<B>(mut request: Request<B>) -> Request<B> {
    if let Some(accept) = request.headers().get(ACCEPT) {
        if let Ok(accept) = accept.to_str() {
            if accept == FEDERATION_CONTENT_TYPE {
                let path = request.uri().path();
                if !path.starts_with("/apub") {
                    let new_path = Uri::builder()
                        .path_and_query(format!("/apub{}", path))
                        .build()
                        .unwrap();
                    *request.uri_mut() = new_path;
                }
            }
        }
    }
    request
}

fn setup_environment() -> Config {
    dotenv::dotenv().ok();
    let args = Config::parse();
    env_logger::init_from_env(env_logger::Env::new().default_filter_or(
        if cfg!(debug_assertions) || args.debug_release {
            "debug"
        } else {
            "warn"
        },
    ));
    args
}

fn check_dependencies() {
    // Ensure pandoc is available
    if let Err(e) = which("pandoc") {
        panic!("Failed to find `pandoc`: {}", e);
    }
}

async fn setup_site_addr_conf(site_addr: SocketAddr) -> io::Result<ConfFile> {
    let mut uiconf = leptos::get_configuration(None)
        .await
        .map_err(|e| io::Error::new(io::ErrorKind::Other, e))?;
    uiconf.leptos_options.site_addr = site_addr;
    Ok(uiconf)
}

async fn setup_database(db_url: &Url) -> io::Result<DbHandle> {
    let pool = Arc::new(
        PgPoolOptions::new()
            .max_connections(6)
            .connect(db_url.as_str())
            .await
            .map_err(|e| io::Error::new(io::ErrorKind::Other, e))?,
    );

    migrate!("../migrations")
        .run(pool.as_ref())
        .await
        .map_err(|e| io::Error::new(io::ErrorKind::Other, e))?;

    let pool_interval = pool.clone();
    spawn(async move {
        let mut interval = time::interval(Duration::from_secs(24 * 3600));
        loop {
            interval.tick().await;
            let _ = query!(
                r#"
                    DELETE FROM
                        api_tokens
                    WHERE
                        expiry < now()
                "#
            )
            .execute(pool_interval.as_ref())
            .await
            .map_err(|e| log::error!("Failed to clean expired API tokens: {e}"));
        }
    });

    Ok(pool)
}

fn setup_file_store(file_store: &Path) -> io::Result<()> {
    create_dir_all(file_store.join("chapters/LOCAL"))?;
    create_dir_all(file_store.join("static"))?;
    create_dir_all(file_store.join("feeds"))?;
    File::create(file_store.join("static/toc.template"))?
        .write_all(include_bytes!("../../res/toc.template"))?;
    File::create(file_store.join("static/default-template.md"))?
        .write_all(include_bytes!("../../res/default-template.md"))?;

    Ok(())
}
