use crate::error::ServerResult;
use activitypub_federation::{config::Data, protocol::context::WithContext, traits::Collection};
use axum::{
    extract::Path,
    http::StatusCode,
    response::{IntoResponse, Redirect},
    Json,
};
use axum_macros::debug_handler;
use wordforge_api::{
    api::user::{self, EditUser, GetUserError},
    objects::{novel_list::NovelList, person::User},
    util::OutboxContent,
    DataHandle,
};

#[debug_handler]
pub async fn get_user(Path(apub_id): Path<String>, data: Data<DataHandle>) -> impl IntoResponse {
    match user::get_user(&apub_id, &data).await {
        Ok(v) => Json(WithContext::new_default(v)).into_response(),
        Err(GetUserError::PermanentRedirect(loc)) => Redirect::permanent(&loc).into_response(),
        Err(GetUserError::WebfingerNotFound) => {
            (StatusCode::NOT_FOUND, "Local actor not found").into_response()
        }
        Err(GetUserError::UserNotFound) => {
            (StatusCode::NOT_FOUND, "User not found").into_response()
        }
        Err(GetUserError::InternalServerError(e)) => {
            (StatusCode::INTERNAL_SERVER_ERROR, e).into_response()
        }
    }
}

#[debug_handler]
pub async fn edit_user_info(
    _data: Data<DataHandle>,
    Json(_info): Json<EditUser>,
) -> ServerResult<()> {
    todo!()
}

#[debug_handler]
pub async fn user_outbox(
    Path(name): Path<String>,
    data: Data<DataHandle>,
) -> ServerResult<impl IntoResponse> {
    let Some(owner) = User::read_from_username(&name, &data.pool).await? else {
        return Ok((StatusCode::NOT_FOUND, "User not found").into_response());
    };
    let books = NovelList::read_local(&owner, &data).await?;
    let res = OutboxContent::new(
        format!("{}://{}/user/{}/outbox", data.protocol, data.domain(), name),
        books,
    );
    Ok(Json(WithContext::new_default(res)).into_response())
}
