use crate::error::ServerResult;
use activitypub_federation::{config::Data, protocol::context::WithContext, traits::Object};
use anyhow::anyhow;
use axum::{
    extract::Path,
    http::StatusCode,
    response::{Html, IntoResponse, Redirect},
    Json,
};
use axum_macros::debug_handler;
use wordforge_api::{
    api::chapter::{get_html_content, get_toc_content, ChapterError},
    objects::{chapter::Chapter, novel::DbNovel},
    DataHandle,
};

#[debug_handler]
pub async fn redirect_to_chapter(
    Path((novel_id, seq)): Path<(String, i32)>,
    data: Data<DataHandle>,
) -> ServerResult<impl IntoResponse> {
    let novel = match DbNovel::from_novel_id(&novel_id, &data).await {
        Ok(Some(novel)) => novel,
        Ok(None) => return Ok((StatusCode::NOT_FOUND, "Novel not found").into_response()),
        Err(err) => return Err(err.into()),
    };

    match novel.resolve_chapter(seq, &data.pool).await {
        Ok(Some(chapter)) => Ok(Redirect::permanent(chapter.inner().as_str()).into_response()),
        Ok(None) => Ok((StatusCode::NOT_FOUND, "Chapter not found").into_response()),
        Err(err) => Err(err.into()),
    }
}

#[debug_handler]
pub async fn get_chapter(
    Path(path): Path<String>,
    data: Data<DataHandle>,
) -> ServerResult<impl IntoResponse> {
    match Chapter::from_uuid(&path, &data).await {
        Ok(Some(chapter)) => match chapter.into_json(&data).await {
            Ok(chapter) => Ok(Json(WithContext::new_default(chapter)).into_response()),
            Err(err) => Err(err.into()),
        },
        Ok(None) => Ok((StatusCode::NOT_FOUND, format!("`{}` not found", path)).into_response()),
        Err(err) => Err(err.into()),
    }
}

#[debug_handler]
pub async fn get_raw_chapter(
    Path(id): Path<String>,
    data: Data<DataHandle>,
) -> ServerResult<impl IntoResponse> {
    match get_html_content(&id, &data).await {
        Ok(content) => Ok(Html(content).into_response()),
        Err(ChapterError::NotFound) => {
            Ok((StatusCode::NOT_FOUND, format!("`{}` not found", id)).into_response())
        }
        Err(ChapterError::InternalError(e)) => Err(anyhow!(e).into()),
        Err(ChapterError::Unauthorized) => {
            Ok((StatusCode::UNAUTHORIZED, "Unauthorized").into_response())
        }
    }
}

#[debug_handler]
pub async fn get_chapter_toc(
    Path(id): Path<String>,
    data: Data<DataHandle>,
) -> ServerResult<impl IntoResponse> {
    match get_toc_content(&id, &data).await {
        Ok(content) => Ok(Html(content).into_response()),
        Err(ChapterError::NotFound) => {
            Ok((StatusCode::NOT_FOUND, format!("`{}` not found", id)).into_response())
        }
        Err(ChapterError::InternalError(e)) => Err(anyhow!(e).into()),
        Err(ChapterError::Unauthorized) => {
            Ok((StatusCode::UNAUTHORIZED, "Unauthorized").into_response())
        }
    }
}
