use crate::error::ServerResult;
use activitypub_federation::{
    axum::inbox::{receive_activity, ActivityData},
    config::Data,
    protocol::context::WithContext,
    traits::{Collection, Object},
};
use anyhow::anyhow;
use axum::{
    extract::{Path, Query},
    http::{header, StatusCode},
    response::{IntoResponse, Redirect},
    Json,
};
use axum_macros::debug_handler;
use serde::Deserialize;
use std::fs;
use wordforge_api::{
    activities::add::NewChapter,
    api::novel::{self, NewNovel, NovelError},
    feed::SyndicationFeed,
    objects::{
        chapter::ChapterList,
        novel::{DbNovel, NovelAcceptedActivities},
        person::User,
    },
    util::OutboxContent,
    DataHandle,
};

#[debug_handler]
pub async fn new_novel(_data: Data<DataHandle>, _info: Json<NewNovel>) -> ServerResult<()> {
    unimplemented!("TODO: Access token based auth")
}

#[debug_handler]
pub async fn get_novel(
    Path(apub_id): Path<String>,
    data: Data<DataHandle>,
) -> ServerResult<impl IntoResponse> {
    match novel::get_novel(&apub_id, &data).await {
        Ok(v) => {
            let v = v.into_json(&data).await?;
            Ok(Json(WithContext::new_default(v)).into_response())
        }
        Err(NovelError::PermanentRedirect(loc)) => Ok(Redirect::permanent(&loc).into_response()),
        Err(NovelError::NotFound) => Ok((StatusCode::NOT_FOUND, "Not found").into_response()),
        Err(NovelError::InternalServerError(e)) => Err(anyhow!(e).into()),
        _ => unreachable!(),
    }
}

#[debug_handler]
pub async fn add_chapter(
    _path: Path<String>,
    _data: Data<DataHandle>,
    _info: Json<NewChapter>,
) -> ServerResult<()> {
    unimplemented!("TODO: Access token based auth")
}

#[debug_handler]
pub async fn novel_feed(
    Path(novel_id): Path<String>,
    data: Data<DataHandle>,
) -> ServerResult<impl IntoResponse> {
    let feed_path = data
        .file_store
        .join("feeds")
        .join(novel_id.as_str())
        .with_extension("atom");

    if !feed_path.exists() {
        let Some(novel) = DbNovel::from_webfinger_id(&novel_id, &data).await? else {
            return Ok((StatusCode::NOT_FOUND, "Novel not found").into_response());
        };
        novel.write_feed(&data).await?;
    }

    let feed = fs::read(feed_path)?;
    Ok(([(header::CONTENT_TYPE, "application/atom+xml")], feed).into_response())
}

#[debug_handler]
pub async fn novel_inbox(
    data: Data<DataHandle>,
    activity_data: ActivityData,
) -> ServerResult<impl IntoResponse> {
    Ok(
        receive_activity::<WithContext<NovelAcceptedActivities>, User, DataHandle>(
            activity_data,
            &data,
        )
        .await?,
    )
}

#[derive(Deserialize)]
pub struct NovelOutboxChapter {
    seq: Option<i32>,
}

#[debug_handler]
pub async fn novel_outbox(
    Path(novel_id): Path<String>,
    Query(seq): Query<NovelOutboxChapter>,
    data: Data<DataHandle>,
) -> ServerResult<impl IntoResponse> {
    let Some(owner) = DbNovel::from_novel_id(&novel_id, &data).await? else {
        return Ok((StatusCode::NOT_FOUND, "Novel not found").into_response());
    };
    let mut chapters = ChapterList::read_local(&owner, &data).await?;

    if let Some(seq) = seq.seq {
        chapters.ordered_items.retain(|c| c.sequence == seq)
    }

    let res = OutboxContent::new(
        format!("http://{}/novel/{}/outbox", data.domain(), novel_id),
        chapters,
    );
    let res = WithContext::new_default(res);
    Ok(Json(res).into_response())
}
