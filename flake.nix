{
  description = "Wordforge ActivityPub";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    rust-overlay.url = "github:oxalica/rust-overlay";
  };

  outputs = { self, nixpkgs, flake-utils, rust-overlay }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [
            (import rust-overlay)
          ];
        };
      in
      rec {
        devShells.default = pkgs.mkShell {
          RUSTC_WRAPPER = "sccache";

          nativeBuildInputs = with pkgs; [
            (rust-bin.selectLatestNightlyWith (toolchain: toolchain.default.override {
              extensions = [ "rust-src" "rust-docs" "rust-analyzer" ];
              targets = [ "wasm32-unknown-unknown" ];
            }))
            pkg-config
            gcc
            mold
            cargo-leptos
            leptosfmt
            nodejs
            sccache
            sqlx-cli
            tailwindcss
            pandoc
            podman
            podman-compose
          ];

          shellHook =
            let
              podmanSetupScript =
                let
                  policyConf = pkgs.writeText "policy.conf" ''
                    {"default":[{"type":"insecureAcceptAnything"}],"transports":{"docker-daemon":{"":[{"type":"insecureAcceptAnything"}]}}}
                  '';
                  registriesConf = pkgs.writeText "registries.conf" ''
                    [registries]
                    [registries.block]
                    registries = []
                    [registries.insecure]
                    registries = []
                    [registries.search]
                    registries = ["docker.io", "quay.io"]
                  '';
                in
                pkgs.writeScript "podman-setup" ''
                  #!${pkgs.runtimeShell}
                  if ! test -f ~/.config/containers/policy.json; then
                    install -Dm555 ${policyConf} ~/.config/containers/policy.json
                  fi
                  if ! test -f ~/.config/containers/registries.conf; then
                    install -Dm555 ${registriesConf} ~/.config/containers/registries.conf
                  fi
                '';
            in
            ''
              ${podmanSetupScript}
            '';
        };
      }
    );
}
