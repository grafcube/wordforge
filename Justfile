build:
    cargo leptos build

check:
    cargo clippy

test:
    cargo test

database:
    sqlx database create
    sqlx migrate run

prepare:
    cargo sqlx prepare --workspace -- --all-targets --all-features

build-container tag:
    podman build -t codeberg.org/grafcube/wordforge:{{tag}} .

push-container tag:
    podman login codeberg.org
    podman push codeberg.org/grafcube/wordforge:{{tag}}
