<!-- Disable warning for inline HTML -->
<!-- markdownlint-disable MD033 -->

# Wordforge ActivityPub

> 🚧 This is still in active development. Contributions are welcome! 🚧

> [!WARNING]
>
> The server is very unstable and not suitable for production. Certain
> navigation actions on the UI may cause the entire server to crash.

<a href="https://codeberg.org/Grafcube/wordforge">
  <!-- markdownlint-disable-next-line MD013 -->
  <img alt="Get it on Codeberg" src="https://get-it-on.codeberg.org/get-it-on-white-on-black.svg" height="60">
</a>

A place where anyone can write novels using markdown. Designed with federation
using ActivityPub.

Check out the [contribution guide](CONTRIBUTING.md) and the [development
guide](HACKING.md) to get started.
