use derive_builder::Builder;
use nodeinfo::NodeInfoOwned;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use std::{
    collections::HashMap,
    fs::{self, File},
    io::{self, Write},
    path::Path,
    sync::{Arc, RwLock},
};
use url::Url;

pub type InstanceState = Arc<RwLock<InstanceMetadata>>;

pub const NODEINFO_TEMPLATE: &str = include_str!("../../nodeinfo.json");

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct InfoLink {
    pub rel: Url,
    pub href: Url,
}

#[derive(Builder, Clone, Debug, Serialize, Deserialize)]
pub struct NodeInfoLinks {
    pub links: Vec<InfoLink>,
}

impl NodeInfoLinksBuilder {
    pub fn add_link(&mut self, rel: Url, href: Url) -> &mut Self {
        let link = InfoLink { rel, href };
        self.links.get_or_insert_with(Default::default).push(link);
        self
    }
}

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct InstanceMetadata {
    #[serde(skip)]
    pub debug_mode: bool,
    #[serde(skip)]
    conf_file: Option<Box<Path>>,
    pub open_registrations: bool,
    pub email_required_for_signup: bool,
    // TODO: usage
    pub metadata: HashMap<String, Value>,
}

impl InstanceMetadata {
    fn conf_file(&self) -> &Path {
        self.conf_file.as_ref().unwrap()
    }

    fn default(conf_file: &Path, debug_release: bool) -> Self {
        Self {
            debug_mode: cfg!(debug_assertions) || debug_release,
            conf_file: Some(conf_file.into()),
            open_registrations: true,
            email_required_for_signup: false,
            metadata: Default::default(),
        }
    }

    pub fn initialize(conf_file: &Path, debug_release: bool) -> io::Result<Self> {
        if conf_file.exists() {
            let conf = fs::read(conf_file)?;
            let mut data: Self = serde_json::from_slice(&conf)?;
            data.conf_file = Some(conf_file.into());
            Ok(data)
        } else {
            let meta = Self::default(conf_file, debug_release);
            meta.write()?;
            Ok(meta)
        }
    }

    pub fn write(&self) -> io::Result<()> {
        if let Some(conf_dir) = self.conf_file().parent() {
            fs::create_dir_all(conf_dir)?;
        }
        let meta = serde_json::to_string(&self)?.into_bytes();
        File::create(self.conf_file())?.write_all(&meta)?;
        Ok(())
    }
}

pub fn build_nodeinfo(instance: &InstanceState) -> anyhow::Result<NodeInfoOwned> {
    let mut nodeinfo = nodeinfo::deserialize_owned(NODEINFO_TEMPLATE)?;
    let instance = instance.read().unwrap();

    nodeinfo.software.version = env!("CARGO_PKG_VERSION").to_string();
    nodeinfo.open_registrations = instance.open_registrations;
    // nodeinfo.usage = instance.usage...;
    nodeinfo.metadata.extend(instance.metadata.clone());

    Ok(nodeinfo)
}

#[cfg(test)]
mod tests {
    use crate::nodeinfo::NodeInfoLinksBuilder;

    #[test]
    fn build_nodeinfolinks() {
        let node_info = NodeInfoLinksBuilder::default()
            .add_link(
                "https://nodeinfo.diaspora.software/ns/schema/2.1"
                    .parse()
                    .unwrap(),
                "https://wordforge.org/nodeinfo/2.1".parse().unwrap(),
            )
            .add_link(
                "https://nodeinfo.diaspora.software/ns/schema/2.0"
                    .parse()
                    .unwrap(),
                "https://wordforge.org/nodeinfo/2.0".parse().unwrap(),
            )
            .build()
            .unwrap();

        let json = serde_json::to_string(&node_info).unwrap();
        let expected_json = r#"{"links":[{"rel":"https://nodeinfo.diaspora.software/ns/schema/2.1","href":"https://wordforge.org/nodeinfo/2.1"},{"rel":"https://nodeinfo.diaspora.software/ns/schema/2.0","href":"https://wordforge.org/nodeinfo/2.0"}]}"#;

        assert_eq!(json, expected_json);
    }
}
