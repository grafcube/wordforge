use crate::DataHandle;
use activitypub_federation::config::Data;
use atom_syndication::{Entry, Feed, Link, Person};
use std::io;

pub mod novel;

#[allow(async_fn_in_trait)]
pub trait SyndicationFeed {
    async fn write_feed(&self, data: &Data<DataHandle>) -> io::Result<()>;
    async fn build_feed(&self, data: &Data<DataHandle>) -> Feed;
    fn links(&self, data: &Data<DataHandle>) -> Vec<Link>;
    async fn authors(&self, data: &Data<DataHandle>) -> Vec<Person>;
    async fn entries(&self, data: &Data<DataHandle>) -> Vec<Entry>;
}
