use super::SyndicationFeed;
use crate::{
    api::chapter::get_chapters_from_novel,
    objects::{novel::DbNovel, person::User},
    DataHandle,
};
use activitypub_federation::{config::Data, fetch::object_id::ObjectId};
use atom_syndication::{Category, Entry, Feed, FeedBuilder, Link, Person};
use chrono::Utc;
use rayon::prelude::*;
use std::{fs::File, io};

impl SyndicationFeed for DbNovel {
    async fn write_feed(&self, data: &Data<DataHandle>) -> io::Result<()> {
        let feed_path = data
            .file_store
            .join("feeds")
            .join(self.webfinger_id(data).as_ref())
            .with_extension("atom");

        let file = File::create(feed_path)?;
        let feed_content = self.build_feed(data).await;
        feed_content.write_to(file).unwrap();

        Ok(())
    }

    async fn build_feed(&self, data: &Data<DataHandle>) -> Feed {
        let ndata = self.data.as_ref().unwrap();
        FeedBuilder::default()
            .logo(Some("/favicon.svg".to_string()))
            .links(self.links(data))
            .id(self.apub_id.clone())
            .title(ndata.title.clone())
            .subtitle(
                ndata
                    .summary
                    .is_empty()
                    .then(|| ndata.summary.clone().into()),
            )
            .updated(Utc::now())
            .lang(ndata.language.to_639_1().map(|v| v.into()))
            .category(Category {
                term: ndata.genre.to_string(),
                ..Default::default()
            })
            .authors(self.authors(data).await)
            .entries(self.entries(data).await)
            .build()
    }

    fn links(&self, data: &Data<DataHandle>) -> Vec<Link> {
        let webfinger_id = self.webfinger_id(data);
        vec![
            Link {
                href: format!("/novel/{}", webfinger_id),
                ..Default::default()
            },
            Link {
                href: format!("/novel/{}/feed.atom", webfinger_id),
                rel: "self".to_string(),
                ..Default::default()
            },
            Link {
                href: self.apub_id.clone(),
                rel: "alternate".to_string(),
                ..Default::default()
            },
        ]
    }

    async fn authors(&self, data: &Data<DataHandle>) -> Vec<Person> {
        let authors: Vec<ObjectId<User>> = self
            .data
            .as_ref()
            .unwrap()
            .authors
            .iter()
            .map(|v| v.apub_id.parse().unwrap())
            .collect();
        let mut author_refs = vec![];
        for author in authors {
            author_refs.push(author.dereference(data).await);
        }

        author_refs
            .into_par_iter()
            .flat_map(|author| {
                let author = author.ok()?;
                let author = Person {
                    uri: Some(format!("/user/{}", author.webfinger_id(data))),
                    name: author.name.unwrap_or(author.preferred_username),
                    ..Default::default()
                };
                Some(author)
            })
            .collect()
    }

    async fn entries(&self, data: &Data<DataHandle>) -> Vec<Entry> {
        let Ok(chapters) = get_chapters_from_novel(self, data).await else {
            return Vec::new();
        };

        chapters
            .into_par_iter()
            .map(|chapter| Entry {
                title: chapter.title.into(),
                id: chapter.href.into_inner().to_string(),
                updated: chapter.updated.unwrap_or(chapter.published).into(),
                links: vec![Link {
                    href: format!("/chapter/{}/{}", self.webfinger_id(data), chapter.sequence),
                    rel: "alternate".to_string(),
                    ..Default::default()
                }],
                summary: chapter.summary.is_empty().then_some(chapter.summary.into()),
                published: Some(chapter.published.into()),
                ..Default::default()
            })
            .collect()
    }
}
