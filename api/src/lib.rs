#![feature(box_patterns)]

use cuid2::CuidConstructor;
use lazy_static::lazy_static;
use sqlx::PgPool;
use std::{path::Path, sync::Arc};
use url::Url;

pub mod account;
pub mod activities;
pub mod api;
pub mod enums;
pub mod feed;
pub mod filters;
pub mod mathml;
pub mod nodeinfo;
pub mod objects;
pub mod tokens;
pub mod util;

pub const ID_LENGTH: u16 = 16;

lazy_static! {
    pub static ref CUIDGEN: CuidConstructor = CuidConstructor::new().with_length(ID_LENGTH);
}

pub type DbHandle = Arc<PgPool>;

#[derive(Clone, Debug)]
pub struct DataHandle {
    pub protocol: Box<str>,
    pub file_store: Box<Path>,
    pub pool: DbHandle,
    pub media_addr: Url,
}
