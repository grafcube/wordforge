use crate::mathml::{MATHML_ATTRS, MATHML_TAGS};
use chrono::Local;
use itertools::Itertools;
use lazy_static::lazy_static;
use pandoc::MarkdownExtension;
use regex::Regex;
use serde::{Deserialize, Serialize};
use std::{ops::Not, path::Path};
use url::Url;

pub const SESSION_EXPIRY: time::Duration = time::Duration::days(7);

lazy_static! {
    pub static ref USERNAME_RE: Regex =
        Regex::new(r"(?i)^[a-z0-9_]+(?:[a-z0-9_\.-]+[a-z0-9_]+)?$").unwrap();
    pub static ref TAG_RE: Regex = Regex::new(r"(?i)\b[\p{L}\p{M}\p{N}_]{2,20}\b").unwrap();
    pub static ref MD_EXTENSIONS: Vec<MarkdownExtension> = vec![
        MarkdownExtension::AsciiIdentifiers,
        MarkdownExtension::FancyLists,
        MarkdownExtension::ImplicitFigures,
        MarkdownExtension::ImplicitHeaderReferences,
        MarkdownExtension::PipeTables,
        MarkdownExtension::Smart,
        MarkdownExtension::Strikeout,
        MarkdownExtension::Subscript,
        MarkdownExtension::Superscript,
        MarkdownExtension::TexMathDollars,
        MarkdownExtension::Other("emoji".to_string()),
        MarkdownExtension::Other("east_asian_line_breaks".to_string()),
    ];
    pub static ref ADMIN_TOKEN_PATH: Box<Path> = std::env::temp_dir()
        .join("wordforge-admin-token")
        .as_path()
        .into();
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct OutboxContent<T> {
    id: String,
    #[serde(flatten)]
    data: T,
}

impl<T> OutboxContent<T> {
    pub fn new(id: String, data: T) -> Self {
        Self { id, data }
    }
}

/// Generate an id for an apub activity
///
/// # Example
///
/// ```
/// # use wordforge_api::util::generate_activity_id;
/// let activity_id = generate_activity_id("example.com");
/// ```
pub fn generate_activity_id(protocol: &str, domain: &str) -> Url {
    format!("{}://{}", protocol, domain)
        .parse::<Url>()
        .unwrap()
        .join("activities")
        .unwrap()
        .join(&Local::now().timestamp_nanos_opt().unwrap().to_string())
        .unwrap()
}

/// Takes a `url` representing a chapter object and returns a corresponding
/// path relative to `file_store` and its file name to locally cache the chapter.
///
/// # Returns
///
/// 0: File name without extension
/// 1: Path to the file's parent directory
///
/// # Example
///
/// ```
/// # use std::path::Path;
/// # use wordforge_api::util::get_remote_chapter_store_path;
/// let file_store = Path::new("store");
/// let url = "https://example.com/chapter/deadbeef".parse().unwrap();
/// let (file_name, file_path) = get_remote_chapter_store_path(&url, file_store);
/// assert_eq!(
///     (file_name.as_ref(), file_path.as_ref()),
///     ("deadbeef",         Path::new("store/chapters/example.com/chapter"))
/// )
/// ```
pub fn get_remote_chapter_store_path(url: &Url, file_store: &Path) -> (Box<str>, Box<Path>) {
    let fp = url.path_segments().unwrap().collect_vec();
    let qp = url.query().unwrap_or_default();
    let (file, path_segments) = fp.split_last().unzip();
    let mut domain_path = file_store
        .join("chapters")
        .join(url.host_str().unwrap().to_lowercase());
    if let Some(path_segments) = path_segments {
        if !path_segments.is_empty() {
            domain_path.push(path_segments.join("/"));
        }
    }
    let file = file
        .map(|f| {
            format!(
                "{}{}",
                f,
                qp.is_empty()
                    .not()
                    .then(|| format!("?{}", qp))
                    .unwrap_or_default()
            )
        })
        .and_then(|p| p.is_empty().not().then_some(p))
        .unwrap_or_else(|| "_".to_string());

    (file.into(), domain_path.as_path().into())
}

/// Generate valid tags from string
///
/// # Example
///
/// ```
/// # use wordforge_api::util::generate_tags;
/// let tag_string = "apple banana cherry";
/// let tags = generate_tags(&tag_string);
/// assert_eq!(tags, vec!["apple", "banana", "cherry"]);
/// ```
pub fn generate_tags(tags: &str) -> Vec<String> {
    TAG_RE
        .find_iter(tags)
        .map(|t| t.as_str().to_string())
        .sorted_by(|a, b| a.to_lowercase().cmp(&b.to_lowercase()))
        .dedup_by(|a, b| a.to_lowercase() == b.to_lowercase())
        .collect()
}

/// Generate a preview from `title` to add to the URL path.
///
/// # Example
///
/// ```
/// # use wordforge_api::util::generate_url_preview;
/// let preview = generate_url_preview("A Title");
/// assert_eq!(preview, "A_Title");
/// ```
pub fn generate_url_preview(title: &str) -> String {
    if title.is_empty() {
        return "_".to_string();
    }

    // Remove special characters
    let re = Regex::new(r"[^\w\-\s]+").unwrap();
    let mut title = re.replace_all(title, "").to_string();
    title.truncate(64);

    // Replace whitespace with underscore
    let re = Regex::new(r"\s+").unwrap();
    let title = re.replace_all(&title, "_").to_string();

    title
}

/// Sanitize `html` content
pub fn sanitize_html(html: String) -> String {
    ammonia::Builder::default()
        .add_tag_attributes("h2", ["id"])
        .add_tag_attributes("h3", ["id"])
        .add_tag_attributes("h4", ["id"])
        .add_tag_attributes("h5", ["id"])
        .add_tag_attributes("h6", ["id"])
        .add_tag_attributes("th", ["style"])
        .add_tag_attributes("td", ["style"])
        .add_tags(MATHML_TAGS)
        .add_generic_attributes(MATHML_ATTRS)
        .clean(&html)
        .to_string()
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::path::PathBuf;

    #[rstest::rstest]
    #[case(
        "https://example.com/deadbeef",
        "deadbeef",
        "store/chapters/example.com"
    )]
    #[case(
        "https://localhost/id/deadbeef",
        "deadbeef",
        "store/chapters/localhost/id"
    )]
    #[case("https://LOCAL/id/deadbeef", "deadbeef", "store/chapters/local/id")]
    #[case(
        "https://example.com/chapter?id=deadbeef",
        "chapter?id=deadbeef",
        "store/chapters/example.com"
    )]
    #[case("https://example.com", "_", "store/chapters/example.com")]
    fn remote_chapter_store_path(
        #[case] url: Url,
        #[case] expected_name: &str,
        #[case] expected_path: PathBuf,
    ) {
        let (file, path) = get_remote_chapter_store_path(&url, Path::new("store"));
        assert_eq!(
            (expected_name, expected_path.as_path()),
            (file.as_ref(), path.as_ref())
        )
    }

    #[rstest::rstest]
    #[case("", &[])]
    #[case("apple APPLE Banana banana", &["apple", "Banana"])]
    #[case("Tag_1 tag_2 tag123", &["tag123", "Tag_1", "tag_2"])]
    #[case("apple! banana? @cherry", &["apple", "banana", "cherry"])]
    #[case("a b apple cat", &["apple", "cat"])]
    #[case("too_long_dont_use_max_length_is_20 you_lost_the_game", &["you_lost_the_game"])]
    fn generate_tags(#[case] input: &str, #[case] expected: &[&str]) {
        assert_eq!(expected, super::generate_tags(input))
    }

    #[rstest::rstest]
    #[case("This is a one-shot", "This_is_a_one-shot")]
    #[case("What's the author cooking now?", "Whats_the_author_cooking_now")]
    #[case("What_happens_to_underscore?", "What_happens_to_underscore")]
    #[case("", "_")]
    fn url_preview(#[case] input: &str, #[case] expected: &str) {
        assert_eq!(expected, generate_url_preview(input))
    }
}
