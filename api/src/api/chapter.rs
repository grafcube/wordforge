use crate::{
    activities::{
        add::{Add, NewChapter},
        delete::{Delete, DeleteTarget},
        update::{Update, UpdateKind},
    },
    api::novel::regenerate_feed,
    filters::{extract_notes, notes_to_fenced_divs, proxy_image_urls},
    objects::{
        chapter::{Chapter, ChapterItem, ChapterList},
        novel::DbNovel,
        person::User,
    },
    util::{get_remote_chapter_store_path, sanitize_html, MD_EXTENSIONS},
    DataHandle, CUIDGEN,
};
use activitypub_federation::{
    config::Data,
    fetch::{collection_id::CollectionId, object_id::ObjectId, webfinger::webfinger_resolve_actor},
    traits::Collection,
};
use chrono::{DateTime, Utc};
use glob::glob;
use log::{error, warn};
use pandoc::{
    InputFormat, InputKind, OutputFormat, OutputKind, PandocError, PandocOption, PandocOutput,
};
use serde::{Deserialize, Serialize};
use sqlx::{query, query_as, PgPool};
use std::{
    fs::{self, File},
    io::{self, Write},
    path::Path,
};
use thiserror::Error;
use url::{ParseError, Url};

#[derive(Debug, Error)]
pub enum ChapterError {
    #[error("Chapter Creation Error: Internal Server Error: {0}")]
    InternalError(String),
    #[error("Chapter not found")]
    NotFound,
    #[error("Chapter Error: Unauthorized")]
    Unauthorized,
}

pub async fn new_chapter(
    novel: String,
    chapter: NewChapter,
    apub_id: Url,
    data: &Data<DataHandle>,
) -> Result<(), ChapterError> {
    let (path, is_local) = if novel.contains('@') {
        (novel, false)
    } else {
        (format!("{}@{}", novel, data.domain()), true)
    };

    let novel: DbNovel = webfinger_resolve_actor(&path, data)
        .await
        .map_err(|_| ChapterError::NotFound)?;

    if is_local {
        let novel_id = novel
            .apub_id
            .parse()
            .map_err(|e: ParseError| ChapterError::InternalError(e.to_string()))?;
        create_chapter(chapter, &novel_id, data)
            .await
            .map_err(|e| ChapterError::InternalError(e.to_string()))?;
    } else {
        Add::send(
            chapter,
            apub_id.into(),
            novel
                .apub_id
                .parse()
                .map_err(|e: url::ParseError| ChapterError::InternalError(e.to_string()))?,
            data,
        )
        .await
        .map_err(|e| ChapterError::InternalError(e.to_string()))?;
    }

    if let Err(e) = regenerate_feed(&novel.apub_id.to_string().parse().unwrap(), data).await {
        log::error!(
            "Failed to generate feed for {}: {}",
            novel.webfinger_id(data),
            e
        );
    }

    Ok(())
}

pub async fn create_chapter(
    chapter: NewChapter,
    novel: &ObjectId<DbNovel>,
    data: &Data<DataHandle>,
) -> anyhow::Result<()> {
    let novel = novel.dereference_local(data).await?;

    let pool = data.pool.as_ref();

    let sequence = query!(
        r#"
            SELECT
                max(sequence) AS sequence
            FROM
                chapters
            WHERE
                lower(audience)=$1"#,
        novel.apub_id.as_str()
    )
    .fetch_one(pool)
    .await?
    .sequence
    .map(|s| s + 1)
    .unwrap_or(1);

    let uuid = CUIDGEN.create_id();
    let apub_id = format!("{}://{}/chapter/{}", data.protocol, data.domain(), uuid);

    query!(
        r#"
            INSERT INTO
                chapters (apub_id, audience, title, summary, sensitive, sequence, local_id)
            VALUES
                ($1, $2, $3, $4, $5, $6, $7)
        "#,
        apub_id,
        novel.apub_id.to_string(),
        chapter.title,
        chapter.summary,
        chapter.sensitive,
        sequence,
        uuid.clone(),
    )
    .execute(pool)
    .await?;

    let chapter_path = data
        .file_store
        .join("chapters/LOCAL")
        .join(uuid)
        .with_extension("md");

    let default_template = data.file_store.join("static/default-template.md");

    if !chapter_path.exists() {
        fs::copy(default_template, chapter_path)?;
    }

    Ok(())
}

pub async fn get_chapters(
    novel: &str,
    data: &Data<DataHandle>,
) -> Result<Vec<ChapterItem>, ChapterError> {
    let path = if novel.contains('@') {
        novel.to_string()
    } else {
        format!("{}@{}", novel, data.domain())
    };

    let novel: DbNovel = webfinger_resolve_actor(&path, data).await.map_err(|e| {
        log::warn!("Failed to resolve {}: {}", path, e);
        ChapterError::NotFound
    })?;
    get_chapters_from_novel(&novel, data).await
}

pub async fn get_chapters_from_novel(
    novel: &DbNovel,
    data: &Data<DataHandle>,
) -> Result<Vec<ChapterItem>, ChapterError> {
    let Some(ndata) = novel.data.as_ref() else {
        return Err(ChapterError::NotFound);
    };

    let chapters = match ChapterList::read_local(novel, data).await {
        Ok(c) => Some(c),
        Err(err) => {
            log::warn!(
                "Could not fetch local chapters for `{}`: {}",
                novel.apub_id,
                err
            );
            None
        }
    };

    let chapters = if let Some(chapters) = chapters {
        chapters.ordered_items
    } else {
        let outbox: CollectionId<ChapterList> = ndata
            .outbox
            .parse::<Url>()
            .map_err(|e: ParseError| ChapterError::InternalError(e.to_string()))?
            .into();

        outbox
            .dereference(novel, data)
            .await
            .map_err(|e| ChapterError::InternalError(e.to_string()))?
            .ordered_items
    };

    Ok(chapters)
}

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub struct ChapterEdit {
    pub title: Option<String>,
    pub summary: Option<String>,
    pub sensitive: Option<bool>,
    pub content: Option<String>,
}

#[derive(Clone, Debug)]
pub struct ChapterUpdate {
    pub title: String,
    pub summary: String,
    pub sensitive: bool,
    pub content: Option<String>,
    pub updated: Option<DateTime<Utc>>,
}

pub async fn edit_chapter(
    data: &Data<DataHandle>,
    user: ObjectId<User>,
    chapter: ObjectId<Chapter>,
    info: ChapterEdit,
) -> Result<ChapterUpdate, ChapterError> {
    let Some(local_id) = query!(
        r#"
        SELECT
            c.local_id
        FROM
            chapters AS c, author_roles as a
        WHERE
            c.audience = a.id AND
            c.apub_id = $1 AND
            a.author = $2
    "#,
        chapter.to_string(),
        user.to_string()
    )
    .fetch_optional(data.pool.as_ref())
    .await
    .map_err(|e| ChapterError::InternalError(e.to_string()))?
    .map(|r| r.local_id) else {
        return Err(ChapterError::Unauthorized);
    };

    let file_store = data.file_store.as_ref();

    let res = if let Some(local_id) = local_id {
        if let Some(content) = info.content {
            let chapter_path = file_store
                .join("chapters/LOCAL")
                .join(&local_id)
                .with_extension("md");
            fs::write(&chapter_path, content)
                .map_err(|e| ChapterError::InternalError(e.to_string()))?;
            fs::remove_file(chapter_path.with_extension("html"))
                .map_err(|e| ChapterError::InternalError(e.to_string()))?;
            fs::remove_file(chapter_path.with_extension("toc.html"))
                .map_err(|e| ChapterError::InternalError(e.to_string()))?;
            fs::remove_dir_all(chapter_path.with_extension("notes"))
                .map_err(|e| ChapterError::InternalError(e.to_string()))?;
        }
        let content = get_local_content(&local_id, data)
            .await
            .map_err(|e| log::error!("Failed to fetch content for {}: {}", local_id, e))
            .ok();
        query_as!(
            ChapterUpdate,
            r#"
        UPDATE
            chapters
        SET
            title = COALESCE($2, title),
            summary = COALESCE($3, summary),
            sensitive = COALESCE($4, sensitive),
            updated = NOW()
        WHERE
            apub_id = $1
        RETURNING
            title, summary, sensitive, updated, '' AS content
        "#,
            chapter.to_string(),
            info.title,
            info.summary,
            info.sensitive,
        )
        .fetch_one(data.pool.as_ref())
        .await
        .map(|mut c| {
            c.content = content;
            c
        })
        .map_err(|e| ChapterError::InternalError(e.to_string()))?
    } else {
        let payload = UpdateKind::Chapter {
            target: chapter,
            object: info.clone(),
        };
        Update::send(user, payload, data)
            .await
            .map_err(|e| ChapterError::InternalError(e.to_string()))?;
        todo!("Implement tracking activities")
    };

    if let Ok(novel) = chapter
        .dereference(data)
        .await
        .map(|v| v.audience.parse().unwrap())
    {
        if let Err(e) = regenerate_feed(&novel, data).await {
            log::error!("Failed to generate feed for {}: {}", novel, e);
        }
    }

    Ok(res)
}

pub async fn delete_chapter(
    data: &Data<DataHandle>,
    chapter: ObjectId<Chapter>,
    author: ObjectId<User>,
) -> Result<DateTime<Utc>, ChapterError> {
    if query!(
        r#"
        SELECT
            author
        FROM
            author_roles AS a, chapters AS c
        WHERE
            c.audience = a.id AND
            a.author = $1 AND
            c.apub_id = $2
    "#,
        author.to_string(),
        chapter.to_string()
    )
    .fetch_optional(data.pool.as_ref())
    .await
    .map_err(|e| ChapterError::InternalError(e.to_string()))?
    .is_none()
    {
        return Err(ChapterError::Unauthorized);
    }

    let chapter = chapter
        .dereference(data)
        .await
        .map_err(|e| ChapterError::InternalError(e.to_string()))?;

    if let Some(deleted) = chapter.deleted {
        return Ok(deleted);
    }

    let local_id = query!(
        r#"
        SELECT
            local_id
        FROM
            chapters
        WHERE
            LOWER(apub_id) = $1
    "#,
        chapter.apub_id
    )
    .fetch_optional(data.pool.as_ref())
    .await
    .map_err(|e| log::warn!("Failed to read local chapter id: {}", e))
    .ok()
    .and_then(|r| r.map(|r| r.local_id))
    .flatten();

    let file_store = data.file_store.as_ref();

    let deleted = if let Some(local_id) = local_id {
        let chapter_path = file_store
            .join("chapters/LOCAL")
            .join(&local_id)
            .with_extension("*");
        glob(&chapter_path.to_string_lossy())
            .map_err(|e| ChapterError::InternalError(e.to_string()))?
            .for_each(|path| {
                let Ok(path) = path else {
                    return;
                };
                if path.is_dir() {
                    fs::remove_dir_all(path).ok();
                } else {
                    fs::remove_file(path).ok();
                }
            });
        query!(
            r#"
            UPDATE
                chapters
            SET
                deleted = now(),
                audience = '',
                title = '',
                summary = '',
                sensitive = false
            WHERE
                apub_id = $1
            RETURNING
                deleted
        "#,
            chapter.apub_id
        )
        .fetch_one(data.pool.as_ref())
        .await
        .map(|r| r.deleted.unwrap())
        .map_err(|e| ChapterError::InternalError(e.to_string()))?
    } else {
        Delete::send(
            author,
            DeleteTarget::Chapter(chapter.apub_id.parse().unwrap()),
            data,
        )
        .await
        .map_err(|e| ChapterError::InternalError(e.to_string()))?;
        todo!("API to track activity responses")
    };

    if let Ok(novel) = chapter.audience.parse() {
        if let Err(e) = regenerate_feed(&novel, data).await {
            log::error!("Failed to generate feed for {}: {}", novel, e);
        }
    }

    Ok(deleted)
}

pub async fn resolve_source_md(
    data: &Data<DataHandle>,
    apub_id: Url,
    author: Url,
) -> Result<String, ChapterError> {
    if query!(
        r#"
        SELECT
            author
        FROM
            author_roles AS a, chapters AS c
        WHERE
            c.audience = a.id AND
            a.author = $1 AND
            c.apub_id = $2
    "#,
        author.to_string(),
        apub_id.to_string()
    )
    .fetch_optional(data.pool.as_ref())
    .await
    .map_err(|e| ChapterError::InternalError(e.to_string()))?
    .is_none()
    {
        return Err(ChapterError::Unauthorized);
    }

    let local_id = query!(
        r#"
        SELECT
            local_id
        FROM
            chapters
        WHERE
            LOWER(apub_id) = $1
    "#,
        apub_id.to_string()
    )
    .fetch_optional(data.pool.as_ref())
    .await
    .map_err(|e| log::warn!("Failed to read local chapter id: {}", e))
    .ok()
    .and_then(|r| r.map(|r| r.local_id))
    .flatten();

    let file_store = data.file_store.as_ref();

    let source = if let Some(local_id) = local_id {
        let chapter_path = file_store
            .join("chapters/LOCAL")
            .join(local_id)
            .with_extension("md");
        fetch_chapter_md(file_store, &chapter_path)?
    } else {
        // TODO: Implement API in `server`
        return Err(ChapterError::InternalError(
            "TODO: API to get markdown content for authorized users".to_string(),
        ));
    };

    Ok(source)
}

pub async fn get_html_content(id: &str, data: &Data<DataHandle>) -> Result<String, ChapterError> {
    match resolve_chapter_content(id, &data.pool).await? {
        ChapterIdType::Local(uuid) => get_local_content(&uuid, data).await,
        ChapterIdType::Remote(url) => fetch_remote_content(url, data).await,
    }
    .map(sanitize_html)
}

fn generate_toc_from_html(html_source: &str, file_store: &Path) -> anyhow::Result<String> {
    let toc_template = file_store.join("static/toc.template");

    let mut p = pandoc::new();
    p.add_options(&[
        PandocOption::Standalone,
        PandocOption::Template(toc_template),
        PandocOption::TableOfContents,
    ]);
    p.set_input(InputKind::Pipe(html_source.to_string()));
    p.set_input_format(InputFormat::Html, vec![]);
    p.set_output_format(OutputFormat::Html5, vec![]);
    p.set_output(OutputKind::Pipe);

    if let PandocOutput::ToBuffer(toc) = p.execute()? {
        Ok(toc)
    } else {
        unreachable!()
    }
}

pub async fn get_toc_content(id: &str, data: &Data<DataHandle>) -> Result<String, ChapterError> {
    match resolve_chapter_content(id, &data.pool).await? {
        ChapterIdType::Local(uuid) => {
            let file_path = data
                .file_store
                .join("chapters/LOCAL")
                .join(&uuid)
                .with_extension("md");

            let html_source = get_local_content(&uuid, data).await?;

            let toc_path = file_path.with_extension("toc.html");

            match File::create_new(&toc_path) {
                Ok(mut file) => {
                    let toc_content = generate_toc_from_html(&html_source, &data.file_store)
                        .map_err(|e| ChapterError::InternalError(e.to_string()))?;
                    file.write_all(toc_content.as_bytes())
                        .map_err(|e| ChapterError::InternalError(e.to_string()))?;
                    Ok(toc_content)
                }
                Err(e) => {
                    if matches!(e.kind(), io::ErrorKind::AlreadyExists) {
                        fs::read_to_string(&toc_path)
                            .map_err(|e| ChapterError::InternalError(e.to_string()))
                    } else {
                        generate_toc_from_html(&html_source, &data.file_store)
                            .map_err(|e| ChapterError::InternalError(e.to_string()))
                    }
                }
            }
        }
        ChapterIdType::Remote(url) => {
            let html_source = fetch_remote_content(url, data).await?;
            let toc = generate_toc_from_html(&html_source, data.file_store.as_ref())
                .map_err(|e| ChapterError::InternalError(e.to_string()))?;
            Ok(toc)
        }
    }
}

enum ChapterIdType {
    Local(String),
    Remote(Url),
}

async fn resolve_chapter_content(id: &str, pool: &PgPool) -> Result<ChapterIdType, ChapterError> {
    let id = match Url::parse(id) {
        Ok(id) => id.to_string(),
        Err(_) => return Ok(ChapterIdType::Local(id.to_string())),
    };

    match query!(
        r#"
        SELECT
            local_id
        FROM
            chapters
        WHERE
            lower(apub_id) = $1
    "#,
        id.to_lowercase()
    )
    .fetch_optional(pool)
    .await
    .map(|r| r.and_then(|r| r.local_id))
    {
        Ok(Some(id)) => Ok(ChapterIdType::Local(id)),
        Ok(None) => Ok(ChapterIdType::Remote(
            id.parse::<Url>()
                .map_err(|e| ChapterError::InternalError(e.to_string()))?,
        )),
        Err(err) => Err(ChapterError::InternalError(err.to_string())),
    }
}

fn fetch_chapter_md(file_store: &Path, chapter_path: &Path) -> Result<String, ChapterError> {
    let default_template = file_store.join("static/default-template.md");

    if !chapter_path.exists() {
        if let Some(chapter_store) = chapter_path.parent() {
            if let Err(e) = fs::create_dir_all(chapter_store) {
                error!("Error creating `{}`: {}", chapter_store.display(), e);
            }
        }
        fs::copy(default_template, chapter_path)
            .map_err(|e| ChapterError::InternalError(e.to_string()))?;
    }

    let content =
        fs::read_to_string(chapter_path).map_err(|e| ChapterError::InternalError(e.to_string()))?;
    Ok(content)
}

pub async fn preview_html(content: String) -> Result<String, PandocError> {
    let mut p = pandoc::new();
    p.set_input(InputKind::Pipe(content));
    p.set_input_format(InputFormat::CommonmarkX, MD_EXTENSIONS.to_vec());
    p.set_output_format(OutputFormat::Html5, Default::default());
    p.set_output(OutputKind::Pipe);
    p.add_option(PandocOption::MathML(None));
    p.add_filter(move |json| extract_notes(json, None));

    if let PandocOutput::ToBuffer(html) = p.execute()? {
        Ok(sanitize_html(html))
    } else {
        unreachable!()
    }
}

pub async fn get_notes(
    data: &Data<DataHandle>,
    chapter: &str,
    author: &str,
) -> Result<Vec<(u32, String)>, ChapterError> {
    if query!(
        r#"
        SELECT
            c.apub_id
        FROM
            chapters AS c, author_roles as a
        WHERE
            c.audience = a.id AND
            c.apub_id = $1 AND
            a.author = $2
    "#,
        chapter,
        author
    )
    .fetch_optional(data.pool.as_ref())
    .await
    .map_err(|e| ChapterError::InternalError(e.to_string()))?
    .is_none()
    {
        return Err(ChapterError::Unauthorized);
    }

    let local_id = query!(
        r#"
        SELECT
            local_id
        FROM
            chapters
        WHERE
            LOWER(apub_id) = $1
    "#,
        chapter
    )
    .fetch_optional(data.pool.as_ref())
    .await
    .map_err(|e| log::warn!("Failed to read local chapter id: {}", e))
    .ok()
    .and_then(|r| r.map(|r| r.local_id))
    .flatten();

    let file_store = data.file_store.as_ref();

    let source = if let Some(local_id) = local_id {
        let chapter_path = file_store
            .join("chapters/LOCAL")
            .join(local_id)
            .with_extension("notes");
        chapter_path
            .read_dir()
            .map_err(|e| ChapterError::InternalError(e.to_string()))?
            .filter_map(|d| {
                if let Err(ref e) = d {
                    log::warn!("Failed to list note: {}", e);
                }
                d.ok().and_then(|path| {
                    let note = path
                        .path()
                        .file_stem()
                        .and_then(|l| l.to_string_lossy().parse().ok())
                        .unwrap_or(0);
                    fs::read_to_string(path.path())
                        .map_err(|e| log::warn!("Failed to read note: {}", e))
                        .ok()
                        .map(|v| (note, v))
                })
            })
            .collect()
    } else {
        // TODO: Implement API in `server`
        return Err(ChapterError::InternalError(
            "TODO: API to get notes for authorized users".to_string(),
        ));
    };

    Ok(source)
}

fn generate_local_html(
    chapter_path: &Path,
    chapter_html: &Path,
    file_store: &Path,
    media_addr: Url,
) -> Result<String, ChapterError> {
    let md_content =
        fs::read_to_string(chapter_path).map_err(|e| ChapterError::InternalError(e.to_string()))?;
    let content = notes_to_fenced_divs(&md_content);

    let chapter_pathbuf = chapter_path.to_path_buf();

    let mut p = pandoc::new();
    p.set_input(InputKind::Pipe(content));
    p.set_input_format(InputFormat::CommonmarkX, MD_EXTENSIONS.to_vec());
    p.set_output_format(OutputFormat::Html5, Default::default());
    p.set_output(OutputKind::File(chapter_html.into()));
    p.add_option(PandocOption::MathML(None));
    p.add_filter(move |json| proxy_image_urls(json, media_addr.clone()));
    p.add_filter(move |json| extract_notes(json, Some(chapter_pathbuf.clone())));

    if let PandocOutput::ToFile(result) = p
        .execute()
        .map_err(|e| ChapterError::InternalError(e.to_string()))?
    {
        let content = fs::read_to_string(result.as_path())
            .map_err(|e| ChapterError::InternalError(e.to_string()))?;

        let result = result.with_extension("toc.html");
        generate_toc_from_html(&content, file_store)
            .map(|toc_content| {
                File::create(&result).and_then(|mut file| file.write_all(toc_content.as_bytes()))
            })
            .unwrap_or_else(|e| {
                warn!(
                    "Failed to generate table of contents for {}: {}",
                    chapter_path.display(),
                    e
                );
                Ok(())
            })
            .unwrap_or_else(|e| {
                warn!(
                    "Failed to generate table of contents for {}: {}",
                    chapter_path.display(),
                    e
                );
            });

        Ok(content)
    } else {
        unreachable!()
    }
}

async fn get_local_content(uuid: &str, data: &Data<DataHandle>) -> Result<String, ChapterError> {
    let chapter_store = data.file_store.join("chapters/LOCAL").join(uuid);

    let chapter_html = chapter_store.with_extension("html");

    // Serve existing html
    if let Ok(content_html) = fs::read_to_string(&chapter_html) {
        return Ok(content_html);
    }

    let chapter_path = chapter_store.with_extension("md");

    fetch_chapter_md(data.file_store.as_ref(), &chapter_path)?;

    let html = generate_local_html(
        &chapter_path,
        &chapter_html,
        &data.file_store,
        data.media_addr.clone(),
    )?;

    Ok(html)
}

async fn fetch_remote_content(url: Url, data: &Data<DataHandle>) -> Result<String, ChapterError> {
    let (file, chapter_path) = get_remote_chapter_store_path(&url, data.file_store.as_ref());
    let chapter_path = chapter_path.join(file.as_ref()).with_extension("html");
    let content = if chapter_path.exists() {
        fs::read_to_string(&chapter_path).map_err(|e| ChapterError::InternalError(e.to_string()))?
    } else {
        let chapter: ObjectId<Chapter> = url.into();
        let chapter = chapter
            .dereference(data)
            .await
            .map_err(|e| ChapterError::InternalError(e.to_string()))?;
        chapter.content
    };
    Ok(content)
}
