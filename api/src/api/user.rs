use crate::{
    objects::{
        novel_list::NovelList,
        person::{Person, User},
    },
    DataHandle,
};
use activitypub_federation::{
    config::Data,
    fetch::webfinger::{extract_webfinger_name, webfinger_resolve_actor},
    traits::{Collection, Object},
};
use serde::{Deserialize, Serialize};
use sqlx::{query_as, PgPool};
use thiserror::Error;

#[derive(Debug, Error)]
pub enum GetUserError {
    #[error("GetUser PermanentRedirect: {0}")]
    PermanentRedirect(String),
    #[error("GetUser WebfingerNotFound")]
    WebfingerNotFound,
    #[error("GetUser UserNotFound")]
    UserNotFound,
    #[error("GetUser InternalServerError: {0}")]
    InternalServerError(String),
}

pub async fn get_user(name: &str, data: &Data<DataHandle>) -> Result<Person, GetUserError> {
    if name.ends_with(data.domain()) {
        let name = format!("acct:{}", name);
        let name =
            extract_webfinger_name(&name, data).map_err(|_| GetUserError::WebfingerNotFound)?;
        return Err(GetUserError::PermanentRedirect(format!("/user/{name}")));
    }

    let pool = data.pool.as_ref();

    let user = if name.contains('@') {
        webfinger_resolve_actor(name, data).await.map_err(|e| {
            log::warn!("Failed to resolve actor `{}`: {}", name, e);
            GetUserError::UserNotFound
        })?
    } else {
        User::read_from_username(name, pool)
            .await
            .map_err(|e| GetUserError::InternalServerError(e.to_string()))?
            .ok_or(GetUserError::UserNotFound)?
    };

    user.into_json(data)
        .await
        .map_err(|e| GetUserError::InternalServerError(e.to_string()))
}

#[derive(Debug, Error)]
pub enum UserBooksError {
    #[error("UserBooks NotFound")]
    NotFound,
    #[error("UserBooks InternalServerError: {0}")]
    InternalServerError(String),
}

pub async fn get_user_books(
    user: &str,
    data: &Data<DataHandle>,
) -> Result<Vec<String>, UserBooksError> {
    let name = if user.contains('@') {
        user.to_string()
    } else {
        format!("{}@{}", user, data.domain())
    };

    let user: User = webfinger_resolve_actor(&name, data)
        .await
        .map_err(|_| UserBooksError::NotFound)?;

    let books = NovelList::read_local(&user, data)
        .await
        .map_err(|e| UserBooksError::InternalServerError(e.to_string()))?;

    Ok(books.ordered_items)
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct EditUser {
    pub display_name: Option<String>,
    pub summary: Option<String>,
}

#[derive(Debug, Error)]
pub enum EditUserError {
    #[error("error: {0}")]
    InternalServerError(String),
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct UserUpdate {
    pub name: Option<String>,
    pub summary: String,
}

pub async fn edit_user(
    pool: &PgPool,
    info: EditUser,
    apub_id: &str,
) -> Result<UserUpdate, EditUserError> {
    let res = query_as!(
        UserUpdate,
        r#"
            UPDATE
                users
            SET
                name = COALESCE($2, name),
                summary = COALESCE($3, summary)
            WHERE
                apub_id = $1 AND
                private_key IS NOT NULL
            RETURNING
                name, summary
            "#,
        apub_id,
        info.display_name,
        info.summary,
    )
    .fetch_one(pool)
    .await
    .map_err(|e| EditUserError::InternalServerError(e.to_string()))?;

    Ok(res)
}
