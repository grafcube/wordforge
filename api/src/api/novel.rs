use crate::{
    activities::{
        delete::{Delete, DeleteTarget},
        update::{NovelUpdateKind, Update, UpdateKind},
    },
    api::chapter::{delete_chapter, get_chapters_from_novel, ChapterError},
    enums::{Genres, Roles},
    feed::SyndicationFeed,
    objects::{novel::DbNovel, person::User},
    util::generate_tags,
    DataHandle, CUIDGEN,
};
use activitypub_federation::{
    config::Data,
    fetch::{
        object_id::ObjectId,
        webfinger::{extract_webfinger_name, webfinger_resolve_actor},
    },
    http_signatures::generate_actor_keypair,
};
use chrono::{DateTime, Utc};
use isolang::Language;
use rayon::prelude::*;
use serde::{Deserialize, Serialize};
use sqlx::query;
use std::fs;
use thiserror::Error;

#[derive(Debug, Error)]
pub enum NovelError {
    #[error("Unauthorized access")]
    Unauthorized,
    #[error("Invalid request: {0}")]
    BadRequest(String),
    #[error("Permanent Redirect: {0}")]
    PermanentRedirect(String),
    #[error("Not found")]
    NotFound,
    #[error("Internal error: {0}")]
    InternalServerError(String),
}

#[derive(Serialize, Deserialize)]
pub struct NewNovel {
    pub title: String,
    pub summary: String,
    pub genre: Genres,
    pub role: Roles,
    pub lang: String,
    pub sensitive: bool,
    pub tags: String,
}

pub async fn create_novel(
    data: Data<DataHandle>,
    apub_id: &str,
    info: NewNovel,
) -> Result<String, NovelError> {
    let re = regex::Regex::new(r"[\r\n]+").unwrap();
    let title = re.replace_all(info.title.trim(), "");

    let lang = match Language::from_name(info.lang.as_str()) {
        None => return Err(NovelError::BadRequest("Invalid language".to_string())),
        Some(l) => l.to_639_1(),
    };

    let tags = generate_tags(&info.tags);
    let uuid = CUIDGEN.create_id();
    let keypair =
        generate_actor_keypair().map_err(|e| NovelError::InternalServerError(e.to_string()))?;
    let url = format!("{}://{}/novel/{}", data.protocol, data.domain(), uuid);
    let pool = data.pool.as_ref();

    let id = match query!(
        r#"INSERT INTO novels
           (apub_id, preferred_username, title, summary, genre, tags, language,
             sensitive, inbox, outbox, public_key, private_key)
           VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12)
           RETURNING apub_id"#,
        url,
        uuid,
        title.trim(),
        info.summary.trim(),
        info.genre.to_string(),
        tags.as_slice(),
        lang,
        info.sensitive,
        format!("{}/inbox", url),
        format!("{}/outbox", url),
        keypair.public_key,
        keypair.private_key
    )
    .fetch_one(pool)
    .await
    {
        Ok(row) => row.apub_id,
        Err(e) => return Err(NovelError::InternalServerError(e.to_string())),
    };

    query!(
        "INSERT INTO author_roles VALUES ($1, $2, $3)",
        id,
        apub_id,
        info.role.to_string()
    )
    .execute(pool)
    .await
    .map_err(|e| NovelError::InternalServerError(e.to_string()))?;
    Ok(uuid)
}

pub async fn get_novel(uuid: &str, data: &Data<DataHandle>) -> Result<DbNovel, NovelError> {
    if uuid.ends_with(data.domain()) {
        let id = format!("acct:{}", uuid);
        let id = extract_webfinger_name(&id, data).map_err(|_| NovelError::NotFound)?;
        return Err(NovelError::PermanentRedirect(format!("/novel/{id}")));
    }
    let novel = if uuid.contains('@') {
        webfinger_resolve_actor(uuid, data)
            .await
            .map_err(|_| NovelError::NotFound)?
    } else {
        match DbNovel::from_novel_id(uuid, data).await {
            Ok(Some(v)) => v,
            Err(e) => return Err(NovelError::InternalServerError(e.to_string())),
            Ok(None) => return Err(NovelError::NotFound),
        }
    };

    Ok(novel)
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct EditNovel {
    pub title: Option<String>,
    pub summary: Option<String>,
    pub tags: Option<String>,
    pub genre: Option<Genres>,
    pub lang: Option<Language>,
    pub sensitive: Option<bool>,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct NovelUpdate {
    pub title: String,
    pub summary: String,
    pub tags: Vec<String>,
    pub genre: String,
    pub language: String,
    pub sensitive: bool,
}

pub async fn edit_novel(
    data: &Data<DataHandle>,
    user: ObjectId<User>,
    novel: ObjectId<DbNovel>,
    info: EditNovel,
) -> Result<NovelUpdate, NovelError> {
    if query!(
        r#"
        SELECT
            author
        FROM
            author_roles
        WHERE
            id = $1 AND
            author = $2
    "#,
        novel.to_string(),
        user.to_string()
    )
    .fetch_optional(data.pool.as_ref())
    .await
    .map_err(|e| NovelError::InternalServerError(e.to_string()))?
    .is_none()
    {
        return Err(NovelError::Unauthorized);
    }

    let tags = info.tags.as_ref().map(|t| generate_tags(t));

    let (is_remote, res) = query!(
        r#"
        UPDATE
            novels
        SET
            title = COALESCE($2, title),
            summary = COALESCE($3, summary),
            tags = COALESCE($4, tags),
            genre = COALESCE($5, genre),
            language = COALESCE($6, language),
            sensitive = COALESCE($7, sensitive)
        WHERE
            apub_id = $1
        RETURNING
            title, summary, tags, genre, language, sensitive, private_key
        "#,
        novel.to_string(),
        info.title,
        info.summary,
        tags.as_deref(),
        info.genre.as_ref().map(|g| g.to_string()),
        info.lang.and_then(|l| l.to_639_1()),
        info.sensitive,
    )
    .fetch_one(data.pool.as_ref())
    .await
    .map(|r| {
        (
            r.private_key.is_none(),
            NovelUpdate {
                title: r.title,
                summary: r.summary,
                tags: r.tags,
                genre: r.genre,
                language: r.language,
                sensitive: r.sensitive,
            },
        )
    })
    .map_err(|e| NovelError::InternalServerError(e.to_string()))?;

    if is_remote {
        let payload = UpdateKind::Novel {
            target: novel,
            object: NovelUpdateKind::Metadata(info),
        };
        Update::send(user, payload, data)
            .await
            .map_err(|e| NovelError::InternalServerError(e.to_string()))?;
        todo!("Implement tracking activities")
    }

    if let Err(e) = regenerate_feed(&novel, data).await {
        log::error!("Failed to generate feed for {}: {}", novel, e);
    }

    Ok(res)
}

pub async fn reorder_chapters(
    data: &Data<DataHandle>,
    author: ObjectId<User>,
    novel: ObjectId<DbNovel>,
    order: &[String],
) -> Result<(), NovelError> {
    if query!(
        r#"
        select
            author
        from
            author_roles
        where
            id = $1 and
            author = $2
    "#,
        novel.inner().as_str(),
        author.inner().as_str()
    )
    .fetch_optional(data.pool.as_ref())
    .await
    .map_err(|e| NovelError::InternalServerError(e.to_string()))?
    .is_none()
    {
        return Err(NovelError::Unauthorized);
    }

    let is_remote = query!(
        r"select private_key from novels where apub_id = $1",
        novel.inner().as_str()
    )
    .fetch_optional(data.pool.as_ref())
    .await
    .map_err(|e| NovelError::InternalServerError(e.to_string()))?
    .is_none();

    query!(
        r#"
        update
            chapters
        set
            sequence = data.seq
        from (
            select
                unnest($1::text[]) as apub_id,
                generate_subscripts($1::text[], 1) as seq
            ) as data
        where
            chapters.apub_id = data.apub_id and
            chapters.audience = $2;
    "#,
        order,
        novel.inner().as_str()
    )
    .execute(data.pool.as_ref())
    .await
    .map_err(|e| NovelError::InternalServerError(e.to_string()))?;

    if is_remote {
        let payload = UpdateKind::Novel {
            target: novel,
            object: NovelUpdateKind::ChapterOrder(order.into()),
        };
        Update::send(author, payload, data)
            .await
            .map_err(|e| NovelError::InternalServerError(e.to_string()))?;
        todo!("Implement tracking activities")
    }

    if let Err(e) = regenerate_feed(&novel, data).await {
        log::error!("Failed to generate feed for {}: {}", novel, e);
    }

    Ok(())
}

pub async fn regenerate_feed(
    novel: &ObjectId<DbNovel>,
    data: &Data<DataHandle>,
) -> Result<(), NovelError> {
    let novel = novel
        .dereference(data)
        .await
        .map_err(|e| NovelError::InternalServerError(e.to_string()))?;
    novel
        .write_feed(data)
        .await
        .map_err(|e| NovelError::InternalServerError(e.to_string()))?;

    Ok(())
}

pub async fn delete_novel(
    data: &Data<DataHandle>,
    apub_id: ObjectId<DbNovel>,
    author: ObjectId<User>,
) -> Result<(DateTime<Utc>, Vec<ChapterError>), (NovelError, Vec<ChapterError>)> {
    let novel = match apub_id.dereference(data).await {
        Ok(n) => n,
        Err(e) => return Err((NovelError::InternalServerError(e.to_string()), vec![])),
    };

    if let Some(deleted) = novel.deleted {
        return Ok((deleted, vec![]));
    }

    if !novel
        .data
        .as_ref()
        .unwrap()
        .authors
        .par_iter()
        .any(|a| a.apub_id == author.to_string())
    {
        return Err((NovelError::Unauthorized, vec![]));
    }

    let mut chapter_errors = vec![];

    if let Ok(chapters) = get_chapters_from_novel(&novel, data)
        .await
        .map_err(|e| log::error!("Failed to get chapters for {}: {}", novel.apub_id, e))
    {
        for chapter_id in chapters.into_iter() {
            let err = delete_chapter(data, chapter_id.href, author.clone())
                .await
                .err();
            if let Some(err) = err {
                chapter_errors.push(err);
            }
        }
    }

    let deleted = if novel.data.is_some() {
        match query!(
            r#"
            update
                novels
            set
                deleted = now(),
                preferred_username = '',
                title = '',
                summary = '',
                sensitive = false,
                tags = '{}',
                private_key = null
            where
                apub_id = $1
            returning
                deleted
        "#,
            apub_id.to_string()
        )
        .fetch_one(data.pool.as_ref())
        .await
        .map(|r| r.deleted.unwrap())
        {
            Ok(v) => {
                if let Err(e) = query!(
                    r"delete from author_roles where id = $1",
                    apub_id.to_string()
                )
                .execute(data.pool.as_ref())
                .await
                {
                    log::error!("Failed to delete authors for {}: {}", apub_id, e)
                }
                v
            }
            Err(e) => {
                return Err((
                    NovelError::InternalServerError(e.to_string()),
                    chapter_errors,
                ))
            }
        }
    } else {
        Delete::send(author.clone(), DeleteTarget::Novel(apub_id), data)
            .await
            .map_err(|e| {
                (
                    NovelError::InternalServerError(e.to_string()),
                    chapter_errors,
                )
            })?;
        todo!("API to track activity responses")
    };

    if let Err(e) = fs::remove_file(
        data.file_store
            .join("feeds")
            .join(novel.webfinger_id(data).as_ref())
            .with_extension("atom"),
    ) {
        log::error!(
            "Failed to delete feed {}.atom: {}",
            novel.webfinger_id(data),
            e
        );
    }

    Ok((deleted, chapter_errors))
}
