use log::error;
use pandoc::{InputFormat, InputKind, OutputFormat, OutputKind, PandocOption};
use pandoc_types::definition::{Block, Inline, IterBlocks, Pandoc};
use rayon::prelude::*;
use serde_json::from_str;
use std::{fs, path::PathBuf};
use url::Url;

pub fn proxy_image_urls(json: String, media_addr: Url) -> String {
    let mut ast: Pandoc = match from_str(&json) {
        Ok(v) => v,
        Err(e) => {
            error!("Failed to parse AST: {}", e);
            return json;
        }
    };

    for block in ast.iter_blocks_mut() {
        if let Block::Figure(_, _, fig_blocks) = block {
            for fig in fig_blocks.iter_mut() {
                if let Block::Plain(inlines) = fig {
                    for inline in inlines.iter_mut() {
                        if let Inline::Image(_, _, target) = inline {
                            if let Ok(url) = Url::parse(&target.url) {
                                if let Some(host) = url.host_str() {
                                    if media_addr.host_str().unwrap() != host {
                                        let path = format!("/image/original?proxy={}", target.url);
                                        target.url = match media_addr.join(&path) {
                                            Ok(proxy) => proxy.to_string(),
                                            Err(err) => {
                                                error!(
                                                    "Failed to build image proxy url ({}, {}): {}",
                                                    media_addr.to_string(),
                                                    target.url,
                                                    err
                                                );
                                                return json;
                                            }
                                        };
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    serde_json::to_string(&ast).unwrap_or_else(|e| {
        error!("Failed to serialize proxy filtered AST: {}", e);
        json
    })
}

pub fn notes_to_fenced_divs(md_content: &str) -> String {
    let mut proc_content = vec![];
    let mut comment_state = false;

    for (n_line, line) in md_content.lines().enumerate() {
        comment_state = if line.starts_with("//") {
            let line = line.split_once("//").unwrap_or_default().1;
            if comment_state {
                proc_content.push(line.to_string());
                true
            } else {
                let open_comment = format!("::::: {{ type=\"note\" line={} }}", n_line + 1);
                proc_content.push(open_comment);
                proc_content.push(line.to_string());
                true
            }
        } else if comment_state {
            proc_content.push(":::::".to_string());
            proc_content.push(line.to_string());
            false
        } else {
            proc_content.push(line.to_string());
            false
        }
    }

    proc_content.join("\n")
}

pub fn extract_notes(json: String, chapter_path: Option<PathBuf>) -> String {
    let mut ast: Pandoc = match from_str(&json) {
        Ok(v) => v,
        Err(e) => {
            error!("Failed to parse AST: {}", e);
            return json;
        }
    };

    let mut notes = vec![];

    ast.blocks.retain(|block| {
        if let Block::Div(attr, blocks) = block {
            if !attr
                .attributes
                .iter()
                .any(|(key, value)| key == "type" && value == "note")
            {
                return true;
            }

            let Some((_, line)) = attr
                .attributes
                .par_iter()
                .find_any(|(key, _)| key == "line")
            else {
                return true;
            };

            let Ok(line) = line.parse::<u32>() else {
                return true;
            };

            let blocks = Pandoc {
                meta: Default::default(),
                blocks: blocks.clone(),
            };

            notes.push((line, blocks));
            return false;
        }
        true
    });

    if let Some(chapter_path) = chapter_path {
        let notes_dir = chapter_path.with_extension("notes");

        let _ =
            fs::create_dir_all(&notes_dir).map_err(|e| error!("Failed to generate notes: {}", e));

        for (line, note) in notes.into_iter() {
            let Ok(note) = serde_json::to_string(&note).map_err(|e| {
                error!("Failed to serialize note AST: {}", e);
            }) else {
                continue;
            };

            let output_path = notes_dir.join(line.to_string()).with_extension("html");

            let mut p = pandoc::new();
            p.set_input(InputKind::Pipe(note));
            p.set_input_format(InputFormat::Json, vec![]);
            p.set_output_format(OutputFormat::Html5, Default::default());
            p.set_output(OutputKind::File(output_path));
            p.add_option(PandocOption::MathJax(None));

            if let Err(err) = p.execute() {
                error!("Failed to render note as html: {}", err);
            };
        }
    }

    serde_json::to_string(&ast).unwrap_or_else(|e| {
        error!("Failed to serialize note filtered AST: {}", e);
        json
    })
}
