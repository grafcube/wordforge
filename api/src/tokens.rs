use crate::account::hash_password;
use chrono::{DateTime, Utc};
use rand::{distributions::Alphanumeric, prelude::*};
use serde::{Deserialize, Serialize};
use sqlx::{query, PgPool};
use std::num::NonZeroU32;
use strum::{Display, EnumIter, EnumString};
use thiserror::Error;

#[derive(Clone, Debug, Display, EnumString, EnumIter, Serialize, Deserialize, PartialEq)]
pub enum Claims {
    UserEdit,
    NovelEdit,
    NovelWrite,
    ChapterEdit,
    ChapterWrite,
}

#[derive(Debug, Error)]
pub enum TokenGenError {
    #[error("Failed to hash token: {0}")]
    HashError(String),
    #[error("Failed to write token to database: {0}")]
    DbError(String),
}

pub struct Permission {
    pub label: String,
    pub owner: String,
    pub expiry: Option<DateTime<Utc>>,
    pub permissions: Vec<Claims>,
}

pub fn generate_token() -> String {
    let token: String = rand::thread_rng()
        .sample_iter(&Alphanumeric)
        .take(48)
        .map(char::from)
        .collect();
    format!("knsu_{}", token)
}

pub async fn store_api_token(
    token: &str,
    info: Permission,
    pool: &PgPool,
) -> Result<(), TokenGenError> {
    let token_hash = hash_password(token).map_err(|e| TokenGenError::HashError(e.to_string()))?;

    let permissions = info
        .permissions
        .into_iter()
        .map(|c| c.to_string())
        .collect::<Vec<String>>();

    let label = info.label.as_str();

    query!(
        r#"
            INSERT INTO
                api_tokens (token, label, owner, expiry, permissions)
            VALUES
                ($1, $2, $3, $4, $5)
        "#,
        token_hash,
        label,
        info.owner,
        info.expiry,
        permissions.as_slice()
    )
    .execute(pool)
    .await
    .map_err(|e| TokenGenError::DbError(e.to_string()))?;

    Ok(())
}

pub async fn store_reg_token(
    token: &str,
    max_uses: Option<NonZeroU32>,
    expiry: Option<DateTime<Utc>>,
    pool: &PgPool,
) -> Result<DateTime<Utc>, TokenGenError> {
    let created = query!(
        r#"
            INSERT INTO
                reg_tokens (token, max_uses, expiry)
            VALUES
                ($1, $2, $3)
            RETURNING
                created
        "#,
        token,
        max_uses.map(|m| m.get() as i32),
        expiry
    )
    .fetch_one(pool)
    .await
    .map_err(|e| TokenGenError::DbError(e.to_string()))?
    .created;

    Ok(created)
}

pub async fn is_reg_token_valid(token: &str, pool: &PgPool) -> Result<bool, TokenGenError> {
    let Some(expired) = query!(
        r#"
        SELECT
            now() >= expiry AS expired
        FROM
            reg_tokens
        WHERE
            token = $1
    "#,
        token
    )
    .fetch_optional(pool)
    .await
    .map(|r| r.map(|r| r.expired))
    .map_err(|e| TokenGenError::DbError(e.to_string()))?
    else {
        return Ok(false);
    };

    if let Some(expired) = expired {
        if expired {
            query!("DELETE FROM reg_tokens WHERE token = $1", token)
                .execute(pool)
                .await
                .map_err(|e| TokenGenError::DbError(e.to_string()))?;
            return Ok(false);
        }
    }

    query!(
        "UPDATE reg_tokens SET used = used + 1 WHERE token = $1",
        token
    )
    .execute(pool)
    .await
    .map_err(|e| TokenGenError::DbError(e.to_string()))?;

    Ok(true)
}
