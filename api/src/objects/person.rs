use crate::DataHandle;
use activitypub_federation::{
    config::Data,
    fetch::object_id::ObjectId,
    kinds::actor::PersonType,
    protocol::public_key::PublicKey,
    traits::{Actor, Object},
};
use axum::async_trait;
use chrono::prelude::*;
use serde::{Deserialize, Serialize};
use sqlx::{query, query_as, PgPool};
use url::Url;

#[derive(Debug, Serialize, Deserialize)]
pub struct User {
    pub apub_id: String,
    pub preferred_username: String,
    pub name: Option<String>,
    pub summary: String,
    pub inbox: String,
    pub outbox: String,
    pub public_key: String,
    #[serde(skip_serializing)]
    private_key: Option<String>,
    pub published: Option<DateTime<Utc>>,
    pub last_refresh: DateTime<Utc>,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Person {
    pub id: ObjectId<User>,
    #[serde(rename = "type")]
    kind: PersonType,
    pub preferred_username: String,
    pub name: Option<String>,
    pub summary: Option<String>,
    inbox: Url,
    outbox: Url,
    public_key: PublicKey,
    pub published: Option<String>,
}

impl User {
    pub async fn read_from_username(
        username: &str,
        data: &PgPool,
    ) -> Result<Option<Self>, sqlx::Error> {
        query_as!(
            Self,
            r#"
            SELECT
                apub_id, preferred_username, name, summary, inbox, outbox,
                public_key, null as private_key, published, last_refresh
            FROM
                users
            WHERE
                lower(preferred_username) = $1 AND
                private_key IS NOT NULL
            "#,
            username.to_lowercase()
        )
        .fetch_optional(data)
        .await
    }

    pub fn webfinger_id(&self, data: &Data<DataHandle>) -> Box<str> {
        let apub_id: Url = self.apub_id.parse().unwrap();
        let domain = format!(
            "{}{}",
            apub_id.host_str().unwrap(),
            apub_id
                .port()
                .map(|p| format!(":{}", p))
                .unwrap_or_default()
        );

        if domain == data.domain() {
            return self.preferred_username.as_str().into();
        }

        let id = format!("/user/{}@{}", self.preferred_username, domain);

        id.as_str().into()
    }
}

#[async_trait]
impl Object for User {
    type DataType = DataHandle;
    type Kind = Person;
    type Error = anyhow::Error;

    fn last_refreshed_at(&self) -> Option<DateTime<Utc>> {
        Some(self.last_refresh)
    }

    async fn read_from_id(
        object_id: Url,
        data: &Data<Self::DataType>,
    ) -> Result<Option<Self>, Self::Error> {
        let pool = data.pool.as_ref();

        query_as!(
            Self,
            r#"
            SELECT
                apub_id, preferred_username, name, summary, inbox, outbox,
                public_key, private_key, published, last_refresh
            FROM
                users
            WHERE
                apub_id = $1
            "#,
            object_id.to_string().to_lowercase()
        )
        .fetch_optional(pool)
        .await
        .map_err(Self::Error::new)
    }

    async fn into_json(self, _data: &Data<Self::DataType>) -> Result<Self::Kind, Self::Error> {
        Ok(Self::Kind {
            id: self.apub_id.parse()?,
            kind: Default::default(),
            preferred_username: self.preferred_username.clone(),
            name: self.name.clone(),
            summary: Some(self.summary.clone()),
            inbox: self.inbox.parse()?,
            outbox: self.outbox.parse()?,
            public_key: self.public_key(),
            published: self
                .published
                .map(|p| p.to_rfc3339_opts(SecondsFormat::Millis, true)),
        })
    }

    async fn verify(
        _json: &Self::Kind,
        _expected_domain: &Url,
        _data: &Data<Self::DataType>,
    ) -> Result<(), Self::Error> {
        Ok(())
    }

    async fn from_json(json: Self::Kind, data: &Data<Self::DataType>) -> Result<Self, Self::Error> {
        // TODO: Why don't responses from Sharkey contain `published`?
        let published = if let Some(p) = json.published {
            Some(p.parse()?)
        } else {
            None
        };

        let user = Self {
            apub_id: json.id.into_inner().into(),
            preferred_username: json.preferred_username,
            name: json.name,
            summary: json.summary.unwrap_or_default(),
            inbox: json.inbox.into(),
            outbox: json.outbox.into(),
            public_key: json.public_key.public_key_pem,
            private_key: None,
            published,
            last_refresh: Utc::now(),
        };

        // TODO: Change database null constraints
        if let Err(err) = query!(
            r#"
            INSERT INTO
                users
                (apub_id, preferred_username, name, summary, inbox, outbox,
                public_key, published, last_refresh)
            VALUES
                ($1, $2, $3, $4, $5, $6, $7, $8, $9)
        "#,
            user.apub_id,
            user.preferred_username,
            user.name,
            user.summary,
            user.inbox,
            user.outbox,
            user.public_key,
            user.published,
            user.last_refresh,
        )
        .execute(data.pool.as_ref())
        .await
        {
            log::warn!("Failed to add remote object `{}`: {}", user.apub_id, err);
        }

        Ok(user)
    }
}

impl Actor for User {
    fn id(&self) -> Url {
        self.apub_id.parse().unwrap()
    }

    fn inbox(&self) -> Url {
        self.inbox.parse().unwrap()
    }

    fn public_key_pem(&self) -> &str {
        &self.public_key
    }

    fn private_key_pem(&self) -> Option<String> {
        self.private_key.clone()
    }
}
