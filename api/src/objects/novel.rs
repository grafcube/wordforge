use super::{chapter::Chapter, Deleted};
use crate::{
    activities,
    enums::{Genres, Roles},
    DataHandle,
};
use activitypub_federation::{
    config::Data,
    fetch::{object_id::ObjectId, webfinger::webfinger_resolve_actor},
    kinds::actor::GroupType,
    protocol::public_key::PublicKey,
    traits::{ActivityHandler, Actor, Object},
};
use anyhow::anyhow;
use axum::async_trait;
use chrono::{DateTime, SecondsFormat, Utc};
use isolang::Language;
use serde::{Deserialize, Serialize};
use sqlx::{query, PgPool};
use std::str::FromStr;
use url::Url;

#[derive(Deserialize, Serialize)]
#[serde(untagged)]
#[enum_delegate::implement(ActivityHandler)]
pub enum NovelAcceptedActivities {
    Add(activities::add::Add),
    Update(activities::update::Update),
    Delete(activities::delete::Delete),
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Author {
    pub apub_id: String,
    pub role: Roles,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct DbNovel {
    pub apub_id: String,
    pub deleted: Option<DateTime<Utc>>,
    #[serde(flatten)]
    pub data: Option<NovelData>,
    pub public_key: String,
    pub last_refresh: DateTime<Utc>,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct NovelData {
    pub preferred_username: String,
    pub title: String,
    pub summary: String,
    pub authors: Vec<Author>,
    pub genre: Genres,
    pub tags: Vec<String>,
    pub language: Language,
    pub sensitive: bool,
    pub inbox: String,
    pub outbox: String,
    #[serde(skip_serializing)]
    private_key: Option<String>,
    pub published: DateTime<Utc>,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Novel {
    id: ObjectId<DbNovel>,
    #[serde(rename = "type")]
    kind: GroupType,
    pub preferred_username: String,
    pub name: String,
    pub summary: String,
    pub authors: Vec<Author>,
    attributed_to: Vec<Url>,
    pub genre: Genres,
    pub tags: Vec<String>,
    pub language: String,
    pub sensitive: bool,
    inbox: Url,
    outbox: Url,
    public_key: PublicKey,
    pub published: String,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
#[serde(untagged)]
pub enum NovelObject {
    Deleted(Box<Deleted<DbNovel, GroupType>>),
    Novel(Box<Novel>),
}

impl NovelObject {
    pub fn id(&self) -> &ObjectId<DbNovel> {
        match &self {
            NovelObject::Novel(box Novel { id, .. }) => id,
            NovelObject::Deleted(box Deleted { id, .. }) => id,
        }
    }
}

impl DbNovel {
    pub async fn from_novel_id(
        novel_id: &str,
        data: &Data<DataHandle>,
    ) -> Result<Option<Self>, anyhow::Error> {
        let pool = data.pool.as_ref();

        let apub_id = match query!(
            "SELECT apub_id FROM novels WHERE preferred_username=$1",
            novel_id
        )
        .fetch_optional(pool)
        .await?
        {
            None => return Ok(None),
            Some(v) => v.apub_id,
        };

        Self::read_from_id(Url::parse(apub_id.as_str()).unwrap(), data).await
    }

    pub async fn from_webfinger_id(
        uuid: &str,
        data: &Data<DataHandle>,
    ) -> Result<Option<Self>, anyhow::Error> {
        let novel = if uuid.contains('@') {
            Some(webfinger_resolve_actor(uuid, data).await?)
        } else {
            Self::from_novel_id(uuid, data).await?
        };

        Ok(novel)
    }

    pub fn webfinger_id(&self, data: &Data<DataHandle>) -> Box<str> {
        let apub_id: Url = self.apub_id.parse().unwrap();
        let domain = format!(
            "{}{}",
            apub_id.host_str().unwrap(),
            apub_id
                .port()
                .map(|p| format!(":{}", p))
                .unwrap_or_default()
        );

        let preferred_username = self.data.as_ref().unwrap().preferred_username.as_str();

        if domain == data.domain() {
            return preferred_username.into();
        }

        let id = format!("/novel/{}@{}", preferred_username, domain);

        id.as_str().into()
    }

    pub async fn resolve_chapter(
        &self,
        seq: i32,
        pool: &PgPool,
    ) -> anyhow::Result<Option<ObjectId<Chapter>>> {
        let apub_id = query!(
            r#"
        SELECT
            apub_id
        FROM
            chapters
        WHERE
            audience = $1 AND
            sequence = $2
        "#,
            self.apub_id,
            seq
        )
        .fetch_optional(pool)
        .await?;

        if let Some(apub_id) = apub_id {
            Ok(Some(apub_id.apub_id.parse()?))
        } else {
            Ok(None)
        }
    }
}

#[async_trait]
impl Object for DbNovel {
    type DataType = DataHandle;
    type Kind = NovelObject;
    type Error = anyhow::Error;

    fn last_refreshed_at(&self) -> Option<DateTime<Utc>> {
        Some(self.last_refresh)
    }

    async fn read_from_id(
        object_id: Url,
        data: &Data<Self::DataType>,
    ) -> Result<Option<Self>, Self::Error> {
        let pool = data.pool.as_ref();

        let authors: Vec<Author> = query!(
            "SELECT author as apub_id, role FROM author_roles WHERE lower(id)=$1",
            object_id.to_string().to_lowercase(),
        )
        .fetch_all(pool)
        .await?
        .into_iter()
        .map(|author| Author {
            apub_id: author.apub_id,
            role: Roles::from_str(author.role.as_str()).unwrap(),
        })
        .collect();

        let novel = query!(
            r#"
            SELECT
                apub_id, preferred_username, title, summary, genre, tags,
                language, sensitive, inbox, outbox, public_key, private_key,
                published, last_refresh, deleted
            FROM
                novels
            WHERE
                lower(apub_id) = $1
            "#,
            object_id.to_string().to_lowercase()
        )
        .fetch_optional(pool)
        .await?
        .map(|row| Self {
            apub_id: row.apub_id,
            last_refresh: row.last_refresh,
            deleted: row.deleted,
            public_key: row.public_key,
            data: Some(NovelData {
                preferred_username: row.preferred_username,
                title: row.title,
                summary: row.summary,
                authors,
                genre: Genres::from_str(row.genre.as_str()).unwrap(),
                tags: row.tags,
                language: Language::from_639_1(row.language.as_str()).unwrap(),
                sensitive: row.sensitive,
                inbox: row.inbox,
                outbox: row.outbox,
                private_key: row.private_key,
                published: row.published,
            }),
        });

        Ok(novel)
    }

    async fn into_json(self, _data: &Data<Self::DataType>) -> Result<Self::Kind, Self::Error> {
        let object = if let Some(deleted) = self.deleted {
            Self::Kind::Deleted(Box::new(Deleted {
                id: self.apub_id.parse()?,
                kind: Default::default(),
                former_type: Default::default(),
                deleted,
            }))
        } else {
            let public_key = self.public_key();
            let ndata = self.data.unwrap();
            Self::Kind::Novel(Box::new(Novel {
                id: self.apub_id.parse()?,
                kind: Default::default(),
                preferred_username: ndata.preferred_username,
                name: ndata.title,
                summary: ndata.summary,
                attributed_to: ndata
                    .authors
                    .iter()
                    .map(|a| a.apub_id.parse().unwrap())
                    .collect(),
                authors: ndata.authors,
                genre: ndata.genre,
                tags: ndata.tags,
                language: ndata.language.to_639_1().unwrap().to_string(),
                sensitive: ndata.sensitive,
                inbox: ndata.inbox.parse()?,
                outbox: ndata.outbox.parse()?,
                public_key,
                published: ndata.published.to_rfc3339_opts(SecondsFormat::Millis, true),
            }))
        };

        Ok(object)
    }

    async fn verify(
        _json: &Self::Kind,
        _expected_domain: &Url,
        _data: &Data<Self::DataType>,
    ) -> Result<(), Self::Error> {
        Ok(())
    }

    async fn from_json(json: Self::Kind, data: &Data<Self::DataType>) -> Result<Self, Self::Error> {
        let novel = match json {
            NovelObject::Deleted(box Deleted { id, deleted, .. }) => {
                let apub_id = id.inner();
                if let Err(e) = query!(
                    r#"
                    INSERT INTO
                        novels
                        (apub_id, deleted, title, tags, sensitive, public_key)
                    VALUES
                        ($1, $2, '', '{}', false, '')
                "#,
                    apub_id.to_string(),
                    deleted
                )
                .execute(data.pool.as_ref())
                .await
                {
                    log::warn!("Failed to add remote object {}: {}", apub_id, e);
                }
                todo!()
            }
            NovelObject::Novel(box Novel {
                id,
                preferred_username,
                name,
                summary,
                authors,
                genre,
                tags,
                language,
                sensitive,
                inbox,
                outbox,
                public_key,
                published,
                ..
            }) => {
                let n = Self {
                    apub_id: id.into_inner().into(),
                    last_refresh: Utc::now(),
                    deleted: None,
                    public_key: public_key.public_key_pem,
                    data: Some(NovelData {
                        preferred_username: preferred_username.parse()?,
                        title: name,
                        summary,
                        authors,
                        genre,
                        tags,
                        language: Language::from_639_1(language.as_str())
                            .ok_or_else(|| anyhow!("Unknown language"))?,
                        sensitive,
                        inbox: inbox.into(),
                        outbox: outbox.into(),
                        private_key: None,
                        published: published.parse()?,
                    }),
                };
                let ndata = n.data.as_ref().unwrap();
                if let Err(e) = query!(
                    r#"
                    INSERT INTO
                        novels
                        (apub_id, preferred_username, title, summary,
                        genre, tags, language, sensitive, inbox, outbox,
                        public_key, published, last_refresh)
                    VALUES
                        ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12,
                        $13)
                "#,
                    n.apub_id,
                    ndata.preferred_username,
                    ndata.title,
                    ndata.summary,
                    ndata.genre.to_string(),
                    &ndata.tags,
                    ndata
                        .language
                        .to_639_1()
                        .ok_or(anyhow!("Invalid language"))?
                        .to_string(),
                    ndata.sensitive,
                    ndata.inbox,
                    ndata.outbox,
                    n.public_key,
                    ndata.published,
                    n.last_refresh
                )
                .execute(data.pool.as_ref())
                .await
                {
                    log::warn!("Failed to add remote object {}: {}", n.apub_id, e);
                }
                n
            }
        };
        Ok(novel)
    }
}

impl Actor for DbNovel {
    fn id(&self) -> Url {
        self.apub_id.parse().unwrap()
    }

    fn inbox(&self) -> Url {
        self.data
            .as_ref()
            .and_then(|n| n.inbox.parse().ok())
            .unwrap_or("file:///dev/null".parse().unwrap())
    }

    fn public_key_pem(&self) -> &str {
        &self.public_key
    }

    fn private_key_pem(&self) -> Option<String> {
        self.data.as_ref().and_then(|v| v.private_key.clone())
    }
}
