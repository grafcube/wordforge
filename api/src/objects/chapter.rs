use super::Deleted;
use crate::{
    api::chapter::get_html_content, objects::novel::DbNovel, util::get_remote_chapter_store_path,
    DataHandle,
};
use activitypub_federation::{
    config::Data,
    fetch::object_id::ObjectId,
    kinds::{collection::OrderedCollectionType, object::ArticleType},
    traits::{Collection, Object},
};
use axum::async_trait;
use chrono::prelude::*;
use serde::{Deserialize, Serialize};
use sqlx::{query, query_as};
use std::{fs, io::Write};
use url::Url;

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub struct Chapter {
    pub apub_id: String,
    pub audience: String,
    pub title: String,
    pub summary: String,
    pub sensitive: bool,
    pub sequence: i32,
    pub content: String,
    pub published: DateTime<Utc>,
    pub updated: Option<DateTime<Utc>>,
    pub last_refresh: DateTime<Utc>,
    pub deleted: Option<DateTime<Utc>>,
}

impl Chapter {
    pub async fn from_uuid(uuid: &str, data: &Data<DataHandle>) -> anyhow::Result<Option<Self>> {
        let Some(mut chapter) = query_as!(
            Self,
            r#"
            SELECT
                apub_id, audience, title, summary, sensitive, sequence, published,
                updated, last_refresh, apub_id AS content, deleted
            FROM
                chapters
            WHERE
                lower(local_id) = $1
            "#,
            uuid,
        )
        .fetch_optional(data.pool.as_ref())
        .await?
        else {
            return Ok(None);
        };
        if chapter.deleted.is_none() {
            chapter.content = get_html_content(uuid, data).await?;
        }
        Ok(Some(chapter))
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Article {
    pub id: ObjectId<Chapter>,
    #[serde(rename = "type")]
    pub kind: ArticleType,
    pub name: String,
    pub audience: Url,
    pub summary: String,
    pub sensitive: bool,
    pub sequence: i32,
    pub content: String,
    pub published: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub updated: Option<String>,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
#[serde(untagged)]
pub enum ChapterObject {
    Deleted(Box<Deleted<Chapter, ArticleType>>),
    Article(Box<Article>),
}

impl ChapterObject {
    pub fn id(&self) -> &ObjectId<Chapter> {
        match &self {
            ChapterObject::Article(box Article { id, .. }) => id,
            ChapterObject::Deleted(box Deleted { id, .. }) => id,
        }
    }
}

#[async_trait]
impl Object for Chapter {
    type DataType = DataHandle;
    type Kind = ChapterObject;
    type Error = anyhow::Error;

    fn last_refreshed_at(&self) -> Option<DateTime<Utc>> {
        Some(self.last_refresh)
    }

    async fn read_from_id(
        object_id: Url,
        data: &Data<Self::DataType>,
    ) -> Result<Option<Self>, Self::Error> {
        let pool = data.pool.as_ref();
        let content = get_html_content(object_id.as_str(), data).await?;

        query_as!(
            Self,
            r#"
                SELECT
                    apub_id, audience, title, summary, sensitive, sequence,
                    published, updated, last_refresh, apub_id AS content, deleted
                FROM
                    chapters
                WHERE
                    lower(apub_id) = $1
            "#,
            object_id.to_string().to_lowercase()
        )
        .fetch_optional(pool)
        .await
        .map(|c| {
            c.map(|mut c| {
                c.content = content;
                c
            })
        })
        .map_err(Self::Error::new)
    }

    async fn into_json(self, _data: &Data<Self::DataType>) -> Result<Self::Kind, Self::Error> {
        let object = if let Some(deleted) = self.deleted {
            Self::Kind::Deleted(Box::new(Deleted {
                id: self.apub_id.parse()?,
                kind: Default::default(),
                former_type: Default::default(),
                deleted,
            }))
        } else {
            Self::Kind::Article(Box::new(Article {
                id: self.apub_id.parse()?,
                kind: Default::default(),
                name: self.title,
                audience: self.audience.parse()?,
                summary: self.summary,
                sensitive: self.sensitive,
                sequence: self.sequence,
                content: self.content,
                published: self.published.to_rfc3339_opts(SecondsFormat::Millis, true),
                updated: self
                    .updated
                    .map(|t| t.to_rfc3339_opts(SecondsFormat::Millis, true)),
            }))
        };
        Ok(object)
    }

    async fn verify(
        _json: &Self::Kind,
        _expected_domain: &Url,
        _data: &Data<Self::DataType>,
    ) -> Result<(), Self::Error> {
        Ok(())
    }

    async fn from_json(json: Self::Kind, data: &Data<Self::DataType>) -> Result<Self, Self::Error> {
        let chapter = match json {
            ChapterObject::Deleted(box Deleted { id, deleted, .. }) => {
                let c = Self {
                    apub_id: id.into_inner().into(),
                    deleted: Some(deleted),
                    ..Default::default()
                };
                if let Err(e) = query!(
                    r#"
                    INSERT INTO
                        chapters
                        (apub_id, deleted, audience, sequence, title, sensitive)
                    VALUES
                        ($1, $2, '', 1, '', false)
                "#,
                    c.apub_id,
                    c.deleted
                )
                .execute(data.pool.as_ref())
                .await
                {
                    log::warn!("Failed to add remote object {}: {}", c.apub_id, e);
                }
                c
            }
            ChapterObject::Article(box Article {
                id,
                name,
                audience,
                summary,
                sensitive,
                sequence,
                content,
                published,
                updated,
                ..
            }) => {
                let c = Self {
                    apub_id: id.to_string(),
                    audience: audience.into(),
                    title: name,
                    summary,
                    sensitive,
                    sequence,
                    content,
                    published: published.parse()?,
                    updated: match updated {
                        None => None,
                        Some(u) => Some(u.parse()?),
                    },
                    last_refresh: Utc::now(),
                    deleted: None,
                };
                if let Err(e) = query!(
                    r#"
                    INSERT INTO
                        chapters
                        (apub_id, audience, title, summary, sensitive, sequence,
                        published, updated, last_refresh)
                    VALUES
                        ($1, $2, $3, $4, $5, $6, $7, $8, $9)
                "#,
                    c.apub_id,
                    c.audience,
                    c.title,
                    c.summary,
                    c.sensitive,
                    c.sequence,
                    c.published,
                    c.updated,
                    c.last_refresh
                )
                .execute(data.pool.as_ref())
                .await
                {
                    log::warn!("Failed to add remote object {}: {}", c.apub_id, e);
                }
                let (file, domain_path) =
                    get_remote_chapter_store_path(id.inner(), data.file_store.as_ref());
                fs::create_dir_all(domain_path.as_ref())?;
                fs::File::create(domain_path.join(file.as_ref()).with_extension("html"))?
                    .write_all(c.content.as_bytes())?;
                c
            }
        };
        Ok(chapter)
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct ChapterItem {
    pub href: ObjectId<Chapter>,
    pub title: String,
    pub summary: String,
    pub sensitive: bool,
    pub sequence: i32,
    pub published: DateTime<Utc>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub updated: Option<DateTime<Utc>>,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct ChapterList {
    #[serde(rename = "type")]
    kind: OrderedCollectionType,
    total_items: usize,
    pub ordered_items: Vec<ChapterItem>,
}

#[async_trait]
impl Collection for ChapterList {
    type Owner = DbNovel;
    type DataType = DataHandle;
    type Kind = ChapterList;
    type Error = anyhow::Error;

    async fn read_local(
        owner: &Self::Owner,
        data: &Data<Self::DataType>,
    ) -> Result<Self::Kind, Self::Error> {
        let pool = data.pool.as_ref();

        let chapters: Vec<ChapterItem> = query!(
            r#"
                SELECT
                    apub_id, title, summary, sensitive, sequence, published, updated
                FROM
                    chapters
                WHERE
                    lower(audience)=$1 AND
                    deleted IS null
                ORDER BY
                    sequence DESC
            "#,
            owner.apub_id.to_string().to_lowercase()
        )
        .fetch_all(pool)
        .await?
        .iter()
        .map(|row| ChapterItem {
            href: row.apub_id.parse().unwrap(),
            title: row.title.clone(),
            summary: row.summary.clone(),
            sensitive: row.sensitive,
            sequence: row.sequence,
            published: row.published,
            updated: row.updated,
        })
        .collect();

        Ok(Self::Kind {
            kind: Default::default(),
            total_items: chapters.len(),
            ordered_items: chapters,
        })
    }

    async fn verify(
        _json: &Self::Kind,
        _expected_domain: &Url,
        _data: &Data<Self::DataType>,
    ) -> Result<(), Self::Error> {
        Ok(())
    }

    async fn from_json(
        json: Self::Kind,
        _owner: &Self::Owner,
        _data: &Data<Self::DataType>,
    ) -> Result<Self, Self::Error> {
        Ok(json)
    }
}
