use activitypub_federation::{
    fetch::object_id::ObjectId, kinds::object::TombstoneType, traits::Object,
};
use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};

pub mod chapter;
pub mod novel;
pub mod novel_list;
pub mod person;

#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Deleted<T, U>
where
    T: Object,
    for<'de2> <T as Object>::Kind: Deserialize<'de2>,
{
    id: ObjectId<T>,
    #[serde(rename = "type")]
    kind: TombstoneType,
    former_type: U,
    deleted: DateTime<Utc>,
}
