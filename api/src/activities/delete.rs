use crate::{
    api::chapter::delete_chapter,
    objects::{chapter::Chapter, novel::DbNovel, person::User},
    util::generate_activity_id,
    DataHandle,
};
use activitypub_federation::{
    activity_queue::queue_activity,
    config::Data,
    fetch::object_id::ObjectId,
    kinds::activity::DeleteType,
    traits::{ActivityHandler, Actor, Object},
};
use anyhow::anyhow;
use axum::async_trait;
use itertools::Itertools;
use serde::{Deserialize, Serialize};
use sqlx::query;
use url::Url;

/// `actor` deletes `target`
#[derive(Serialize, Deserialize, Debug)]
pub struct Delete {
    actor: ObjectId<User>,
    target: DeleteTarget,
    #[serde(rename = "type")]
    kind: DeleteType,
    id: Url,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
#[serde(untagged)]
pub enum DeleteTarget {
    Novel(ObjectId<DbNovel>),
    Chapter(ObjectId<Chapter>),
}

impl Delete {
    pub async fn send(
        actor: ObjectId<User>,
        target: DeleteTarget,
        data: &Data<DataHandle>,
    ) -> anyhow::Result<Url> {
        let user = User::read_from_id(actor.clone().into_inner(), data)
            .await?
            .ok_or_else(|| anyhow!("Local user not found"))?;
        let id = generate_activity_id(data.protocol.as_ref(), data.domain());
        let inbox = match &target {
            DeleteTarget::Novel(novel) => novel.dereference(data).await?.inbox(),
            DeleteTarget::Chapter(chapter) => {
                let novel: ObjectId<DbNovel> = chapter.dereference(data).await?.audience.parse()?;
                novel.dereference(data).await?.inbox()
            }
        };
        let delete = Self {
            actor,
            target,
            kind: Default::default(),
            id: id.clone(),
        };
        queue_activity(&delete, &user, vec![inbox], data).await?;
        Ok(id)
    }
}

#[async_trait]
impl ActivityHandler for Delete {
    type DataType = DataHandle;
    type Error = anyhow::Error;

    fn id(&self) -> &Url {
        &self.id
    }

    fn actor(&self) -> &Url {
        self.actor.inner()
    }

    async fn verify(&self, data: &Data<Self::DataType>) -> anyhow::Result<()> {
        let user = self.actor.dereference(data).await?;
        let novel = match &self.target {
            DeleteTarget::Novel(novel) => novel.dereference(data).await?,
            DeleteTarget::Chapter(chapter) => {
                let chapter = chapter.dereference_local(data).await?;
                let novel: ObjectId<DbNovel> = chapter.audience.parse()?;
                let novel = novel.dereference(data).await?;
                if chapter.deleted.is_some() || novel.apub_id != chapter.audience {
                    return Err(anyhow!("Invalid Delete activity"));
                }
                novel
            }
        };

        let pool = data.pool.as_ref();

        query!(
            "SELECT author FROM author_roles WHERE lower(id)=$1",
            novel.apub_id.to_string().to_lowercase()
        )
        .fetch_all(pool)
        .await
        .map_err(|e| anyhow!("{e}"))?
        .into_iter()
        .map(|row| row.author)
        .contains(&user.apub_id)
        .then_some(())
        .ok_or(anyhow!("No write permission"))
    }

    async fn receive(self, data: &Data<Self::DataType>) -> anyhow::Result<()> {
        match self.target {
            DeleteTarget::Novel(_novel) => todo!(),
            DeleteTarget::Chapter(chapter) => {
                delete_chapter(data, chapter.into_inner().into(), self.actor).await?;
            }
        }

        // TODO: Send Announce if accepted. Requires follows to be implemented.

        Ok(())
    }
}
