use crate::{
    api::{
        chapter::{edit_chapter, ChapterEdit},
        novel::{edit_novel, reorder_chapters, EditNovel},
    },
    objects::{chapter::Chapter, novel::DbNovel, person::User},
    util::generate_activity_id,
    DataHandle,
};
use activitypub_federation::{
    activity_queue::queue_activity,
    config::Data,
    fetch::object_id::ObjectId,
    kinds::activity::DeleteType,
    traits::{ActivityHandler, Actor, Object},
};
use anyhow::anyhow;
use axum::async_trait;
use itertools::Itertools;
use serde::{Deserialize, Serialize};
use sqlx::query;
use url::Url;

/// `actor` updates `payload.target` with `payload.object`.
#[derive(Serialize, Deserialize, Debug)]
pub struct Update {
    actor: ObjectId<User>,
    #[serde(flatten)]
    payload: UpdateKind,
    #[serde(rename = "type")]
    kind: DeleteType,
    id: Url,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
#[serde(untagged)]
pub enum UpdateKind {
    Novel {
        target: ObjectId<DbNovel>,
        object: NovelUpdateKind,
    },
    Chapter {
        target: ObjectId<Chapter>,
        object: ChapterEdit,
    },
}

#[derive(Serialize, Deserialize, Debug, Clone)]
#[serde(untagged)]
pub enum NovelUpdateKind {
    Metadata(EditNovel),
    ChapterOrder(Box<[String]>),
}

impl Update {
    pub async fn send(
        actor: ObjectId<User>,
        payload: UpdateKind,
        data: &Data<DataHandle>,
    ) -> anyhow::Result<Url> {
        let user = User::read_from_id(actor.clone().into_inner(), data)
            .await?
            .ok_or_else(|| anyhow!("Local user not found"))?;
        let id = generate_activity_id(data.protocol.as_ref(), data.domain());
        let inbox = match &payload {
            UpdateKind::Novel { target, .. } => target.dereference(data).await?.inbox(),
            UpdateKind::Chapter { target, .. } => {
                target.dereference(data).await?.audience.parse()?
            }
        };
        let update = Self {
            actor,
            payload,
            kind: Default::default(),
            id: id.clone(),
        };
        queue_activity(&update, &user, vec![inbox], data).await?;
        Ok(id)
    }
}

#[async_trait]
impl ActivityHandler for Update {
    type DataType = DataHandle;
    type Error = anyhow::Error;

    fn id(&self) -> &Url {
        &self.id
    }

    fn actor(&self) -> &Url {
        self.actor.inner()
    }

    async fn verify(&self, data: &Data<Self::DataType>) -> anyhow::Result<()> {
        let user = self.actor.dereference(data).await?;
        let novel = match &self.payload {
            UpdateKind::Novel { target, .. } => target.dereference(data).await?,
            UpdateKind::Chapter { target, .. } => {
                let chapter = target.dereference_local(data).await?;
                let novel: ObjectId<DbNovel> = chapter.audience.parse()?;
                let novel = novel.dereference(data).await?;
                if chapter.deleted.is_some() || novel.apub_id != chapter.audience {
                    return Err(anyhow!("Invalid Delete activity"));
                }
                novel
            }
        };

        let pool = data.pool.as_ref();

        query!(
            "SELECT author FROM author_roles WHERE lower(id)=$1",
            novel.apub_id.to_string().to_lowercase()
        )
        .fetch_all(pool)
        .await
        .map_err(|e| anyhow!("{e}"))?
        .into_iter()
        .map(|row| row.author)
        .contains(&user.apub_id)
        .then_some(())
        .ok_or(anyhow!("No write permission"))
    }

    async fn receive(self, data: &Data<Self::DataType>) -> anyhow::Result<()> {
        match self.payload {
            UpdateKind::Novel { target, object } => match object {
                NovelUpdateKind::Metadata(object) => {
                    edit_novel(data, self.actor, target, object).await?;
                }
                NovelUpdateKind::ChapterOrder(order) => {
                    reorder_chapters(data, self.actor, target, order.as_ref()).await?;
                }
            },
            UpdateKind::Chapter { target, object } => {
                edit_chapter(data, self.actor, target, object).await?;
            }
        };

        // TODO: Send Announce if accepted. Requires follows to be implemented.

        Ok(())
    }
}
