use crate::{
    enums::InstanceUserRoles,
    tokens::generate_token,
    util::{ADMIN_TOKEN_PATH, SESSION_EXPIRY, USERNAME_RE},
    DataHandle,
};
use activitypub_federation::{config::Data, http_signatures::generate_actor_keypair};
use argon2::{
    password_hash::{rand_core::OsRng, SaltString},
    Argon2, PasswordHash, PasswordHasher, PasswordVerifier,
};
use serde::{Deserialize, Serialize};
use sqlx::{query, PgPool};
use std::{borrow::Cow, fs::File, io::Write};
use thiserror::Error;
use tower_sessions::{Expiry, Session};
use validator::Validate;

#[derive(Debug, Error)]
pub enum UserValidateError {
    #[error("Local user not found")]
    NotFound,
    #[error("User InternalServerError: {0}")]
    InternalServerError(String),
}

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct UserInfo {
    pub id: String,
    pub name: Option<String>,
    pub username: String,
}

pub async fn admin_exists(pool: &PgPool) -> anyhow::Result<bool> {
    let admin_exists = query!(
        r#"
        SELECT
            apub_id
        FROM
            admins
        WHERE
            role = $1
    "#,
        InstanceUserRoles::Admin.to_string()
    )
    .fetch_optional(pool)
    .await?
    .is_some();

    if !admin_exists {
        let mut token_file = File::create(ADMIN_TOKEN_PATH.as_ref())?;
        let token = generate_token();
        println!("\nADMIN TOKEN: {}\n", token);
        if let Err(err) = token_file.write_all(token.as_bytes()) {
            log::error!("Error while writing admin token: {}", err);
        }
    }

    Ok(admin_exists)
}

pub async fn validate(conn: &PgPool, apub_id: &str) -> Result<UserInfo, UserValidateError> {
    let name = match query!(
        "SELECT apub_id, name, preferred_username FROM users WHERE apub_id=$1",
        apub_id
    )
    .fetch_optional(conn)
    .await
    {
        Ok(Some(v)) => UserInfo {
            id: v.apub_id,
            name: v.name,
            username: v.preferred_username,
        },
        Ok(None) => return Err(UserValidateError::NotFound),
        Err(e) => return Err(UserValidateError::InternalServerError(e.to_string())),
    };
    Ok(name)
}

#[derive(Debug, Error)]
pub enum LoginError {
    #[error("LoginError: BadRequest: {0}")]
    BadRequest(String),
    #[error("LoginError: Unauthorized: {0}")]
    Unauthorized(FormAuthError),
    #[error("LoginError: InternalServerError: {0}")]
    InternalServerError(String),
}

#[derive(Debug, Error)]
pub enum FormAuthError {
    #[error("Username or email not found")]
    LoginId,
    #[error("Wrong password")]
    Password,
    #[error("Email already exists")]
    Email,
    #[error("Username already exists")]
    Username,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub enum LoginId {
    Username(String),
    Email(String),
}

impl TryFrom<String> for LoginId {
    type Error = validator::ValidationErrors;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        if validator::ValidateEmail::validate_email(&value) {
            Ok(LoginId::Email(value))
        } else if USERNAME_RE.is_match(&value) {
            Ok(LoginId::Username(value))
        } else {
            let mut errors = validator::ValidationErrors::new();
            let mut err = ::validator::ValidationError::new("login_id");
            err.add_param(::std::borrow::Cow::from("value"), &&value);
            errors.add("login_id", err);
            Err(errors)
        }
    }
}

impl Validate for LoginId {
    fn validate(&self) -> Result<(), validator::ValidationErrors> {
        let mut errors = validator::ValidationErrors::new();

        match self {
            LoginId::Username(username) => {
                if !USERNAME_RE.is_match(username) {
                    let mut err = ::validator::ValidationError::new("regex");
                    err.message = Some(Cow::from("Invalid username"));
                    err.add_param(Cow::from("value"), &username);
                    errors.add("username", err);
                }
            }
            LoginId::Email(email) => {
                if !validator::ValidateEmail::validate_email(email) {
                    let mut err = ::validator::ValidationError::new("email");
                    err.add_param(Cow::from("value"), &email);
                    errors.add("email", err);
                }
            }
        }

        if errors.is_empty() {
            Ok(())
        } else {
            Err(errors)
        }
    }
}

pub async fn login(
    pool: &PgPool,
    session: Session,
    login_id: LoginId,
    password: String,
    client_app: String,
    client_website: Option<String>,
    remember: bool,
) -> Result<String, LoginError> {
    #[derive(Deserialize, Validate)]
    struct LoginData {
        login_id: LoginId,
        #[validate(length(min = 8))]
        password: String,
        client_app: String,
        #[validate(url)]
        client_website: Option<String>,
    }

    let info = LoginData {
        login_id,
        password,
        client_app,
        client_website,
    };

    info.validate()
        .map_err(|e| LoginError::BadRequest(e.to_string()))?;

    let email = if let LoginId::Email(ref email) = info.login_id {
        Some(email.to_lowercase())
    } else {
        None
    };
    let username = if let LoginId::Username(ref username) = info.login_id {
        Some(username.to_lowercase())
    } else {
        None
    };

    let res = sqlx::query!(
        r#"
        SELECT
            apub_id, password
        FROM
            users
        WHERE
            lower(email) = $1 OR
            lower(preferred_username) = $2
    "#,
        email,
        username
    )
    .fetch_one(pool)
    .await
    .map_err(|_| LoginError::Unauthorized(FormAuthError::LoginId))?;

    let Some(password) = res.password else {
        return Err(LoginError::Unauthorized(FormAuthError::Password));
    };

    let password_hash =
        PasswordHash::new(&password).map_err(|e| LoginError::InternalServerError(e.to_string()))?;

    match PasswordVerifier::verify_password(
        &Argon2::default(),
        info.password.as_bytes(),
        &password_hash,
    ) {
        Ok(_) => {
            session
                .insert("id", &res.apub_id)
                .await
                .map_err(|e| LoginError::InternalServerError(e.to_string()))?;
            session
                .insert("client_app", &info.client_app)
                .await
                .map_err(|e| LoginError::InternalServerError(e.to_string()))?;
            session
                .insert("client_website", &info.client_website)
                .await
                .map_err(|e| LoginError::InternalServerError(e.to_string()))?;

            if remember {
                session.set_expiry(Some(Expiry::OnInactivity(SESSION_EXPIRY)));
            } else {
                session.set_expiry(Some(Expiry::OnSessionEnd));
            }

            Ok(res.apub_id)
        }
        Err(_) => Err(LoginError::Unauthorized(FormAuthError::Password)),
    }
}

pub fn hash_password(password: &str) -> Result<String, argon2::password_hash::Error> {
    let salt = SaltString::generate(&mut OsRng);
    Argon2::default()
        .hash_password(password.as_bytes(), &salt)
        .map(|v| v.to_string())
}

#[derive(Debug, Error)]
pub enum RegistrationError {
    #[error("RegistrationError: Conflict: {0}")]
    Conflict(FormAuthError),
    #[error("RegistrationError: BadRequest: {0}")]
    BadRequest(String),
    #[error("RegistrationError: InternalServerError: {0}")]
    InternalServerError(String),
}

pub async fn register(
    data: &Data<DataHandle>,
    display_name: Option<String>,
    username: String,
    email: Option<String>,
    password: String,
) -> Result<String, RegistrationError> {
    if username.to_lowercase() == "me" {
        return Err(RegistrationError::BadRequest(
            "Reserved username".to_string(),
        ));
    }

    #[derive(Debug, Deserialize, Serialize, Validate)]
    struct NewUser {
        display_name: Option<String>,
        #[validate(regex(path = *USERNAME_RE, message = "Invalid username"))]
        username: String,
        #[validate(email)]
        email: Option<String>,
        #[validate(length(min = 8))]
        password: String,
    }

    let info = NewUser {
        display_name,
        username,
        email,
        password,
    };

    info.validate()
        .map_err(|e| RegistrationError::BadRequest(e.to_string()))?;

    let pool = data.pool.as_ref();

    match query!(
        r#"SELECT
           EXISTS(SELECT 1 FROM users WHERE preferred_username = $1) AS username,
           EXISTS(SELECT 1 FROM users WHERE email = $2) AS email"#,
        info.username.to_lowercase(),
        info.email.as_ref().map(|v| v.to_lowercase())
    )
    .fetch_one(pool)
    .await
    {
        Err(e) => return Err(RegistrationError::InternalServerError(e.to_string())),
        Ok(v) => {
            match v.email {
                None => (),
                Some(e) => {
                    if e {
                        return Err(RegistrationError::Conflict(FormAuthError::Email));
                    }
                }
            };
            match v.username {
                None => (),
                Some(u) => {
                    if u {
                        return Err(RegistrationError::Conflict(FormAuthError::Username));
                    }
                }
            };
        }
    }

    let password = hash_password(&info.password)
        .map_err(|e| RegistrationError::InternalServerError(e.to_string()))?;

    let keypair = generate_actor_keypair()
        .map_err(|e| RegistrationError::InternalServerError(e.to_string()))?;

    let apub_id = query!(r#"
        INSERT INTO
            users
           (apub_id, preferred_username, name, inbox, outbox, public_key, private_key, email, password)
        VALUES
            (lower($1), $2, $3, $4, $5, $6, $7, $8, $9)
        RETURNING
            apub_id
    "#,
        format!("{}://{}/user/{}", data.protocol, data.domain(), info.username.to_lowercase()),
        info.username,
        info.display_name,
        format!("{}://{}/user/{}/inbox", data.protocol, data.domain(), info.username.to_lowercase()),
        format!("{}://{}/user/{}/outbox", data.protocol, data.domain(), info.username.to_lowercase()),
        keypair.public_key,
        keypair.private_key,
        info.email,
        password,
    )
    .fetch_one(pool)
    .await
    .map(|r| r.apub_id)
    .map_err(|e| RegistrationError::InternalServerError(e.to_string()))?;

    Ok(apub_id)
}
