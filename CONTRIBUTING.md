# Contribution Guide

Thanks for your interest in helping improve this project!

Check out the [development guide](HACKING.md) to get started. You can look
through the [open issues](https://codeberg.org/grafcube/wordforge/issues) to
pick something to work on.

## Community

- **Matrix:** [#wordforge:catgirl.cloud](https://matrix.to/#/#wordforge:catgirl.cloud)
