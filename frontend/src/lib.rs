#![allow(unexpected_cfgs)]

use wasm_bindgen::prelude::wasm_bindgen;
use wordforge_ui::app::App;

#[wasm_bindgen]
pub fn hydrate() {
    console_error_panic_hook::set_once();
    leptos::mount_to_body(App);
}
