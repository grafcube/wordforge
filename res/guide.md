This document demonstrates the markdown syntax that you can use to write
novels. A subset of it is supported for notes.

## Text formatting

*italics* $\leftarrow$ `*italics*`

**bold** $\leftarrow$ `**bold**`

***bold and italics*** $\leftarrow$ `***bold and italics***`

~~strikethrough~~ $\leftarrow$ `~~strikethrough~~`

`verbatim` $\leftarrow$ `` `verbatim` ``

```markdown
> Quoted text like this
```

> Quoted text like this

### Escaping characters

You can escape special characters with `\`.

```markdown
\*This is not italicized\*

\`This is not verbatim\`

`` `verbatim works a little differently if you want to use backticks inside a
line. Blocks are separated by the number of backticks used. ``
```

\*This is not italicized\*

\`This is not verbatim\`

`` `verbatim` works a little differently if you want to use backticks inside a
line. Blocks are separated by the number of backticks used. ``

## Headings (This one is a `h2`)

```markdown
## Headings (This one is a `h2`)
```

### h3

```markdown
### h3
```

#### h4

```markdown
#### h4
```

##### h5

```markdown
##### h5
```

###### h6

```markdown
###### h6
```

## Paragraphs

Paragraphs must be separated by an empty line.

```markdown
This is the first paragraph. It continues in the same line.

This is the *second* paragraph. See the blank line above?
We're still in the second paragraph even though this sentence is on a
different line and also abruptly cuts into the third line.

Here we see the third paragraph.
```

This is the first paragraph. It continues in the same line.

This is the *second* paragraph. See the blank line above?
We're still in the second paragraph even though this sentence is on a
different line and also abruptly cuts into the third line.

Here we see the third paragraph.

### Dashes

#### Hyphen

```markdown
anti-static
```

anti-static

#### En dash

```markdown
The event will be held June 1--June 30.
```

The event will be held June 1--June 30.

#### Em dash

```markdown
"Where did that---"

Suddenly, the cat jumped onto the counter.
```

"Where did that---"

Suddenly, the cat jumped onto the counter.

## Lists

### Unordered list

Use hyphens to make lists.

```markdown
- Dinner
- A bath
- Or perhaps...
```

- Dinner
- A bath
- Or perhaps...

### Ordered list

Use numbers to make lists.

```markdown
1. Get new story idea
2. Write it down
3. Forget about it
```

1. Get new story idea
2. Write it down
3. Forget about it

### Multiline list

Leave a blank line between list item and indent continuing lines to make
multiline lists. This works the same way with both ordered and unordered
lists.

```markdown
- This line is very long
  trust me.

- This is another very
  long point.

  With another paragraph.

- And on and on and on and on and on...
```

- This line is very long
  trust me.

- This is another very
  long point.

  With another paragraph.

- And on and on and on and on and on...

### Sublists

Use two spaces to indent points and create sublists. This works the same way
with both ordered and unordered lists.

```markdown
- Things I like
  - Cats
  - Food
  - Anime
    - and also manga
    - and light novels
    - and vtubers
    - ...
    - who's your oshi?
- Things I don't like
  - Mosquitoes
  - Cumin
  - Anime
    - :facepalm:
```

- Things I like
  - Cats
  - Food
  - Anime
    - and also manga
    - and light novels
    - and vtubers
    - ...
    - who's your oshi?
- Things I don't like
  - Mosquitoes
  - Cumin
  - Anime
    - :facepalm:

## Page break

Create a page break with `---`.

```markdown
...and then they all died.

---

But they actually lived!
```

...and then they all died.

---

But they actually lived!

## Tables

```markdown
| Right | Left | Default | Center |
| ----: | :--- | ------- | :----: |
|    12 | 12   | 12      |   12   |
|   123 | 123  | 123     |  123   |
|     1 | 1    | 1       |   1    |
```

| Right | Left | Default | Center |
| ----: | :--- | ------- | :----: |
|    12 | 12   | 12      |   12   |
|   123 | 123  | 123     |  123   |
|     1 | 1    | 1       |   1    |

## Emoji

```markdown
You :clap: can :candle: use :sleepy: emoji :pensive: by :smiling_imp:
putting :raised_hands: them :grinning: between :point_right::point_left: `:`s.
```

You :clap: can :candle: use :sleepy: emoji :pensive: by :smiling_imp:
putting :raised_hands: them :grinning: between :point_right::point_left: `:`s.

## Links

### Relative links

```markdown
[Go to `/user/grafcube`](/user/grafcube)
```

[Go to `/user/grafcube`](/user/grafcube)

### Absolute links

```markdown
[Go to joinwordforge.com](https://joinwordforge.com)
```

[Go to joinwordforge.com](https://joinwordforge.com)

### Bare links

This doesn't work.

```markdown
https://example.com
```

<!-- markdownlint-disable-next-line -->
https://example.com

This works.

```markdown
<https://example.com>
```

<https://example.com>

### Link references

```markdown
[Go to this website][link1]

[Also check this out][link2]

[link1]: https://joinwordforge.com
[link2]: https://example.com "Optional title"
```

[Go to this website][link1]

[Also check this out][link2]

[link1]: https://joinwordforge.com
[link2]: https://example.com "Optional title"

#### Impicit naming

```markdown
[This][] is a link.

[This]: https://example.com
```

[This][] is a link.

[This]: https://example.com

## Images

Images have the same features as links (relative, implicit, reference etc.) The
only difference is that it is prefixed by an exclamation mark `!`.

```markdown
![Optional alt text](https://picsum.photos/200)

![Relative source](/favicon.svg)
```

![Optional alt text](https://picsum.photos/200)

![Relative source](/favicon.svg)

You can adjust the size of your image with attributes. The aspect ratio will be
maintained in this example.

```markdown
![Adjusting width](https://picsum.photos/200){width=400}
```

![Adjusting width](https://picsum.photos/200){width=400}

## Notes

You can write notes by starting a line with `//`. This will only appear in the
editor. It can be useful to have notes that only collaborators can see.

If you want to start a line with `//` without making a note, you can prefix it
with whitespace and it will be ignored.

 // ...like this

// This is a note

// This is another note.
//
// This is a new paragraph in the same note.

// A subset of this *markdown* **works** within notes.

You won't see this in action here, so create a book of your own and give it a
try!

## Mentions

TODO (parsing)

## MathML

You can write maths formulae using LaTeX syntax by enclosing it in `$`
symbols.

Inline: `$y = mx + b$` $\rightarrow$ $y = mx + b$

Block:

```latex
$$
{F}_{\text{D}}=\frac{1}{2}C\rho A{v}^{2}
$$
```

$$
{F}_{\text{D}}=\frac{1}{2}C\rho A{v}^{2}
$$

## Code blocks

You can make multiline fenced code blocks. Surround your code with 3
backticks.

````markdown
```
fn hello() {
    println!("Creating worlds...");
}
```
````

```rust
fn hello() {
    println!("Creating worlds...");
}
```
