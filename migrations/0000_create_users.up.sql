create table users (
  apub_id text primary key,
  preferred_username text not null,
  name text,
  summary text not null default '',
  inbox text not null,
  outbox text not null,
  public_key text not null,
  private_key text,
  published timestamptz default now(),
  last_refresh timestamptz not null default now(),
  email text unique,
  password text
);

create
or replace function set_null_if_empty () returns trigger as $$
begin
    if new.name = '' then
        new.name := null;
    end if;
    return new;
end;
$$ language plpgsql;

create trigger set_null_on_insert_or_update before insert
or
update on users for each row
execute function set_null_if_empty ();
