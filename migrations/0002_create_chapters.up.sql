create table chapters (
  apub_id text primary key,
  audience text not null,
  sequence int not null,
  title text not null,
  summary text not null default '',
  sensitive boolean not null,
  local_id text,
  published timestamptz not null default now(),
  updated timestamptz,
  last_refresh timestamptz not null default now(),
  deleted timestamptz
);
