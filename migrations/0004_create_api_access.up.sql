create table api_tokens (
  token text not null,
  label text not null,
  owner text not null references users (apub_id),
  created timestamptz not null default now(),
  expiry timestamptz,
  last_used timestamptz,
  permissions text[] not null,
  primary key (label, owner)
);

create table reg_tokens (
  token text primary key,
  used int not null default 0,
  max_uses int,
  created timestamptz not null default now(),
  expiry timestamptz
);

create
or replace function check_uses_limit () returns trigger as $$
begin
    if new.used >= new.max_uses then
        delete from reg_tokens where token = new.token;
    end if;
    return new;
end;
$$ language plpgsql;

create trigger check_uses_limit_trigger
after
update on reg_tokens for each row
execute function check_uses_limit ();
