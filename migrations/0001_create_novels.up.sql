create table novels (
  apub_id text primary key,
  preferred_username text not null,
  title text not null,
  summary text not null default '',
  genre text not null,
  tags text[] not null,
  language text not null,
  sensitive boolean not null,
  inbox text not null,
  outbox text not null,
  public_key text not null,
  private_key text,
  published timestamptz not null default now(),
  last_refresh timestamptz not null default now(),
  deleted timestamptz
);

create table author_roles (
  id text not null,
  author text not null,
  role text not null default 'None',
  primary key (id, author)
);
